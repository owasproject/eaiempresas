/**
 * Isban Mexico
 *   Clase: DAOBitacoraAdministrativaImpl.java
 *   Descripción: Implementacion del componente de Acceso a Datos para la
 *   bitacora administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 16, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.dto.VersionRSADTO;
import com.isban.eai.enlace.infra.GenericBdIsbanDA;
import com.isban.eai.enlace.infra.IsbanDataAccessCanal;
import com.isban.eai.enlace.infra.MapeoConsulta;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * Implementacion del componente de Acceso a Datos para la
 * consulta de la bandera de version de RSA.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOVersionRSAImpl extends GenericBdIsbanDA implements
	DAOVersionRSA, MapeoConsulta<VersionRSADTO> {
	
	/**
	 * LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger(DAOVersionRSAImpl.class);
	
    /**
     * Crea el componente de Acceso a Base de Datos.
     **/
    public DAOVersionRSAImpl() {
        super(IsbanDataAccessCanal.DB_EAI);
    }

    /** {@inheritDoc} */
    public VersionRSADTO consulta()
            throws BusinessException {
        try{
        	return mapeoRegistro(consultarSinMapeo(SELECT));
        }catch(BusinessException e){
        	LOGGER.debug("Error: "+ e.getMessage());
        	LOGGER.debug("Error: "+ e.getCause());
        	throw new BusinessException("Error al ejecutar la consulta de la bandera de RSA.");
        }
    }
    
	/** {@inheritDoc} */
	public VersionRSADTO mapeoRegistro(List<HashMap<String, Object>> list)
			throws BusinessException {
			final VersionRSADTO versionDto = new VersionRSADTO();
			String valor = list.get(0).toString();
			final int posicion = valor.indexOf('=');
			valor = valor.substring(posicion+1,posicion+2);
			versionDto.setValor(valor);
		return versionDto;
	}

	/** {@inheritDoc} */
	public VersionRSADTO mapeoRegistro(Map<String, Object> registro)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

}
