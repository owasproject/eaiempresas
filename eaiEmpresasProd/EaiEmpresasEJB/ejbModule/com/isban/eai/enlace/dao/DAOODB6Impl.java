/**
 * Isban Mexico
 *   Clase: DAOODB6Impl.java
 *   Descripción: Implenetacion DAO
 *
 *   Control de Cambios:
 *   1.0 Diciembre 10, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.isban.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import com.isban.eai.enlace.dto.trans390.ODB6Response;
import com.isban.eai.enlace.infra.GenericCicsIsbanDA;
import com.isban.eai.enlace.infra.IsbanDataAccessCanal;
import com.isban.ebe.commons.exception.BusinessException;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOODB6Impl extends GenericCicsIsbanDA implements DAOODB6 {
	/**LOG**/
	private static final Logger LOG = Logger.getLogger(DAOODB6Impl.class);
	
	
	
	/**
	 * Constructor 
	 */
	public DAOODB6Impl() {
		super(IsbanDataAccessCanal.CICS_INTRANET);
		PropertyConfigurator.configure("/proarchivapp/WebSphere8/was8/eai/enlace/log4j.properties");
		
	}

	/** {@inheritDoc} */
	public ODB6Response consultaCorreo(String request)
			throws BusinessException {
		StringBuffer envio= new StringBuffer();
		ODB6Response respuesta = new ODB6Response();

		//Ejecuta trama para obtener respuesta
		ResponseMessageCicsDTO regreso = new ResponseMessageCicsDTO();
		envio.append(generaTramaODB6(request));
		LOG.info("trama------>>>>[" + envio.toString()+"]");
		regreso = ejecutar(envio.toString(), "ODB6");
	    
		if(regreso != null  && regreso.getResponseMessage() != null) {
			if(regreso.getResponseMessage().contains("AVODA0002")) {
				
		    	respuesta.setCodError("EXI000");
				respuesta.setCorreo( regreso.getResponseMessage().
						substring(60, 110).trim());
				
			} else {
				respuesta.setCodError("FAIL00");
			}
		} else {
			respuesta.setCodError("FAIL00");
			LOG.error(">>>>>>>>>>> No hay respuesta MQ");
		}
		
		return respuesta;
	}

	

}
