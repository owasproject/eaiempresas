/**
 * Isban Mexico
 *   Clase: DAOPE68Impl.java
 *   Descripción: Implenetacion DAO
 *
 *   Control de Cambios:
 *   1.0 Diciembre 10, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.dao;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.isban.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import com.isban.eai.enlace.dto.trans390.PE68Response;
import com.isban.eai.enlace.infra.GenericCicsIsbanDA;
import com.isban.eai.enlace.infra.IsbanDataAccessCanal;
import com.isban.ebe.commons.exception.BusinessException;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class DAOPE68Impl extends GenericCicsIsbanDA implements DAOPE68 {
	/**LOG**/
	private static final Logger LOG = Logger.getLogger(DAOPE68Impl.class);
	
	/**
	 * Constructor 
	 */
	public DAOPE68Impl() {
		super(IsbanDataAccessCanal.CICS_INTRANET);
		PropertyConfigurator.configure("/proarchivapp/WebSphere8/was8/eai/enlace/log4j.properties");
		
	}

	@Override
	public PE68Response consultaPersona(String nomPersona)
			throws BusinessException {
		
		StringBuffer envio= new StringBuffer();
		PE68Response dato = new PE68Response();
		
		String[] tokens;
		
		//Ejecuta trama para obtener respuesta
		ResponseMessageCicsDTO regreso = new ResponseMessageCicsDTO();
		envio.append(generaTramaPE68(nomPersona));
		regreso = ejecutar(envio.toString(), "PE68");
		
		if(regreso != null && regreso.getResponseMessage() != null 
				&& regreso.getResponseMessage().contains("DCPEM1400")) {
			LOG.debug("--->Contenido: " + regreso.getResponseMessage());
			tokens = regreso.getResponseMessage().split("@");
			int posicionNom = 0, cont = 0;
			for(String msj : tokens){
				if(msj.contains("DCPEM1400")){
					posicionNom = cont;
				}
				cont++;
			}
			dato.setNombre(tokens[posicionNom].substring(73, 92).trim());
			dato.setPrimerApe(tokens[posicionNom].substring(93, 112).trim());
			dato.setSegundoApe(tokens[posicionNom].substring(113, 153).trim());
			
			dato.setCodError("EXI000");
		} else {
			dato.setCodError("FAIL00");
		}
	    
	  	return dato ;
	}

	

}
