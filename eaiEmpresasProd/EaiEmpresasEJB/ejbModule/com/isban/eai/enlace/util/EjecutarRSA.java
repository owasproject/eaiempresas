package com.isban.eai.enlace.util;

import mx.isban.rsa.aa.ws.ChallengeQuestion;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;
import mx.isban.rsa.bean.ServiciosSTUResponse;
import mx.isban.rsa.conector.servicios.ServiciosAA;
import mx.isban.rsa.conector.servicios.ServiciosSTU;

import org.apache.log4j.Logger;

import com.ibm.jsse2.util.e;
import com.isban.eai.enlace.dto.RSADTO;

public class EjecutarRSA{
	
	/**
	 * LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger(EjecutarRSA.class);
		
	/**
	 * @param bean : bean
	 * @return ServiciosAAResponse
	 */
	public ServiciosAAResponse ejecutaAnalyze(RSADTO bean) {
	    //Llamado de la versi�n 7
		LOGGER.info("Ejecuta Analyze ");	    
	    final ServiciosAA serviciosAA = new ServiciosAA();
	    final RSABean rsaBean = convertirDTO(bean);
	    ServiciosAAResponse respuesta = null;
	    try{
	    	respuesta = serviciosAA.analyzeSiteToUser(rsaBean, bean.getVersionRSA());
	    	LOGGER.info("Fin de Ejecuta Analyze ");
		}catch(NullPointerException e){
	    	LOGGER.info("Error al ejecutar Analyze");
	    }
		LOGGER.info("Envio de respuesta Analyze ");
	    return respuesta;
	}
	
	/**
	 * @param bean : bean
	 * @return ServiciosSTUResponse
	 */
	public ServiciosSTUResponse ejecutaConsultaImagenUsr(RSADTO bean) {
		
		LOGGER.info("Ejecuta ConsultaImagenUsr ");
		final ServiciosSTU servicioSTU = new ServiciosSTU();
		final RSABean rsaBean = convertirDTO(bean);
		ServiciosSTUResponse respuesta = null;
		try{
			respuesta = servicioSTU.getImage(rsaBean, bean.getVersionRSA());
	    }catch(NullPointerException e){
	    	LOGGER.info("Error al ejecutar ejecutaConsultaImagenUsr");
	    }
	    return respuesta;
	}
	
	/**
	 * @param bean : bean
	 * @return ServiciosAAResponse
	 */
	public ServiciosAAResponse ejecutaConsultaPreguntaReto(RSADTO bean) {
		
		LOGGER.info("Ejecuta ConsultaPreguntaReto ");
		final ServiciosAA serviciosAA = new ServiciosAA();
		final RSABean rsaBean = convertirDTO(bean);
		ServiciosAAResponse respuesta = null;
		try{
			respuesta = serviciosAA.getChallQuestion(rsaBean, bean.getVersionRSA());
		}catch(NullPointerException e){
	    	LOGGER.info("Error al ejecutar ejecutaConsultaPreguntaReto");
	    }
	    return respuesta;
	}
	
	/**
	 * @param bean : bean
	 * @param challengeQuestion : challengeQuestion
	 * @return ServiciosAAResponse
	 */
	public ServiciosAAResponse ejecutaRespuestaPreguntaReto(RSADTO bean, ChallengeQuestion[] challengeQuestion) {
		
		LOGGER.info("Ejecuta RespuestaPreguntaReto ");
		final ServiciosAA serviciosAA = new ServiciosAA();
		final RSABean rsaBean = convertirDTO(bean);
		ServiciosAAResponse respuesta = null;
	    try{
	    	respuesta = serviciosAA.authenticateChallQuestion(rsaBean, challengeQuestion, bean.getVersionRSA());
	    }catch(NullPointerException e){
	    	LOGGER.info("Error al ejecutar ejecutaRespuestaPreguntaReto");
	    }
	    return respuesta;
	}
	
	/**
	 * @param bean : bean
	 * @return ServiciosAAResponse
	 */
	public ServiciosAAResponse ejecutaCreateUser(RSADTO bean) {
		
		LOGGER.info("Ejecuta CreateUser ");
		final ServiciosAA serviciosAA = new ServiciosAA();
		final RSABean rsaBean = convertirDTO(bean);
		ServiciosAAResponse respuesta = null;
		try{
			respuesta = serviciosAA.createUser(rsaBean, bean.getVersionRSA());
		}catch(NullPointerException e){
	    	LOGGER.info("Error al ejecutar ejecutaCreateUser");
	    }
	    return respuesta;
	}
	/**
	 * @param bean : bean
	 * @return ServiciosAAResponse
	 */
	public ServiciosAAResponse ejecutaUpdateUser(RSADTO bean) {
		
		LOGGER.info("Ejecuta UpdateUser (enrollUser)");
		final ServiciosAA serviciosAA = new ServiciosAA();
		final RSABean rsaBean = convertirDTO(bean);
		ServiciosAAResponse respuesta = null;
	    try{
	    	respuesta = serviciosAA.enrollUser(rsaBean, bean.getVersionRSA());
	    }catch(NullPointerException e){
	    	LOGGER.info("Error al ejecutar ejecutaUpdateUser (enrollUser)");
	    }
	    
	    return respuesta;
	}
	
	/**
	 * @param bean : bean de tipo RSADTO
	 * @return rsaBean de RSABean
	 */
	public RSABean convertirDTO(RSADTO bean){
		
		final RSABean rsaBean = new RSABean();
		rsaBean.setAuthSessionId(bean.getAuthSessionId());
		rsaBean.setDeviceRequest(bean.getDeviceRequest());
		rsaBean.setIdSession(bean.getIdSession());
		rsaBean.setAuthTransactionId(bean.getAuthTransactionId());
		rsaBean.setOrgName(bean.getOrgName());
		rsaBean.setUserCountry(bean.getUserCountry());
		rsaBean.setUserLanguage(bean.getUserLanguage());
		rsaBean.setUserName(bean.getUserName());
		
		return rsaBean;
		
	}

	
}