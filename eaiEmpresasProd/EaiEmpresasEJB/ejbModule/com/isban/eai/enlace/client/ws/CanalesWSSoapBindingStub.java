/**
 * CanalesWSSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.client.ws;

import java.rmi.RemoteException;

import org.apache.log4j.Logger;


import com.isban.eai.enlace.util.EnlaceConfig;


public class CanalesWSSoapBindingStub extends org.apache.axis.client.Stub implements com.isban.eai.enlace.client.ws.CanalesWS {
	
    /**
     * operations
     */
    static org.apache.axis.description.OperationDesc [] operations;

    static {
    	operations = new org.apache.axis.description.OperationDesc[13];
        initOperationDesc1();
        initOperationDesc2();
    }
	
	/**logger**/
	private static final Logger LOGGER = Logger.getLogger(CanalesWSSoapBindingStub.class);
	
    /**
     * codCliente
     */
    private final static String COD_CLIENTE = "codCliente";
    
    /**
     * codIdAplicacion
     */
    private final static String COD_ID_APLICACION = "codIdAplicacion";
    
    /**
     * string
     */
    private final static String STRING = "string";
    
    /**
     * codOperacion
     */
    private final static String COD_OPERACION = "codOperacion";
    
    /**
     * ArrayOf_xsd_string
     */
    private final static String ARRAY_OF_XSD = "ArrayOf_xsd_string";
	
    /**
     * codLang
     */
    private final static String COD_LANG = "codLang";
    
    /**
     * http://www.w3.org/2001/XMLSchema
     */
    private final static String HTTP = "http://www.w3.org/2001/XMLSchema";
    
    /**
     * http://ws.token.asteci.net
     */
    private final static String HTTP_ASTECI = "http://ws.token.asteci.net";
    
	/**
	 * cachedSerClasses
	 */
	private final transient java.util.List cachedSerClasses = new java.util.Vector();
    
	/**
	 * cachedSerQNames
	 */
	private final transient java.util.List cachedSerQNames = new java.util.Vector();
    
	/**
	 * cachedSerFactories
	 */
	private final transient java.util.List cachedSerFactories = new java.util.Vector();
    
	/**
	 * cachedDeserFactories
	 */
	private final transient java.util.List cachedDeserFactories = new java.util.Vector();
	

    /**
     * CanalesWSSoapBindingStub
     * @throws org.apache.axis.AxisFault manejo de excepcion
     */
    public CanalesWSSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    /**
     * CanalesWSSoapBindingStub
     * @param endpointURL URL de acceso
     * @param service nombre del WS
     * @throws org.apache.axis.AxisFault manejo de excepciones
     */
    public CanalesWSSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         setTimeout(5000);
         super.cachedEndpoint = endpointURL;
    }

    /**
     * CanalesWSSoapBindingStub
     * @param service nombre WS
     * @throws org.apache.axis.AxisFault manejo de excepciones
     */
    public CanalesWSSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            final java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            final java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            final java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            final java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            final java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            final java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            final java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            final java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            final java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            final java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            
            LOGGER.debug("valores: " + beansf +"-"+ beandf+"-"+ enumsf+"-"+enumdf+"-"+arraysf+"-"+
            		arraydf+"-"+simplesf+"-"+simpledf+"-"+simplelistsf+"-"+simplelistdf);
            
            qName = new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD);
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName(HTTP, STRING);
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, "ArrayOfArrayOf_xsd_string");
            cachedSerQNames.add(qName);
            cls = java.lang.String[][].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName(HTTP, STRING);
            qName2 = null;
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }

    /**
     * initOperationDesc1
     */
    private static void initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Estatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_OPERACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "EstatusReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Contrato");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_OPERACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "ContratoReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("consultaListaEstatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "lista"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD), java.lang.String[].class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_OPERACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, "ArrayOfArrayOf_xsd_string"));
        oper.setReturnClass(java.lang.String[][].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "consultaListaEstatusReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Bloqueo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codMotivo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "BloqueoReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("desBloqueo");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "desBloqueoReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Activacion");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "ActivacionReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Solicitud");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_OPERACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codContratoEnlace"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codCuentaCargo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codClienteEmpresa"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "SolicitudReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("consultaEstatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_OPERACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "consultaEstatusReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("consultaContrato");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_OPERACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "consultaContratoReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("activarToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "activarTokenReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[9] = oper;

    }

    /**
     * initOperationDesc2
     */
    private static void initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("bloquearToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codMotivo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "bloquearTokenReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("desBloquearToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "desBloquearTokenReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("solicitudToken");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_CLIENTE), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_ID_APLICACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_LANG), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", COD_OPERACION), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codContratoEnlace"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codCuentaCargo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "codClienteEmpresa"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName(HTTP, STRING), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName(EnlaceConfig.RUTA_WEB_SERVICE_CANALES, ARRAY_OF_XSD));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "solicitudTokenReturn"));
        oper.setStyle(org.apache.axis.constants.Style.RPC);
        oper.setUse(org.apache.axis.constants.Use.ENCODED);
        operations[12] = oper;

    }

    /**
     * createCall
     * @return _call llamada del WS
     * @throws java.rmi.RemoteException manejo de excepcion
     */
    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            final org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            final java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                final java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
                    _call.setEncodingStyle(org.apache.axis.Constants.URI_SOAP11_ENC);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        final java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        final javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        final java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            final java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            final java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            final org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            final org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] estatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "Estatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codOperacion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        	//throw rmEx;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] contrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "Contrato"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codOperacion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[][] consultaListaEstatus(java.lang.String[] lista, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "consultaListaEstatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {lista, codIdAplicacion, codLang, codOperacion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[][]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[][]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[][].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] bloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "Bloqueo"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codMotivo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] desBloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "desBloqueo"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] activacion(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "Activacion"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codLang});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] solicitud(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "Solicitud"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codOperacion, codContratoEnlace, codCuentaCargo, codClienteEmpresa});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] consultaEstatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "consultaEstatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codOperacion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] consultaContrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "consultaContrato"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codOperacion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] activarToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "activarToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codLang});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw axisFaultException;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] bloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "bloquearToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codMotivo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] desBloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "desBloquearToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

    /** {@inheritDoc} */
    public java.lang.String[] solicitudToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        final org.apache.axis.client.Call _call = createCall();
        _call.setOperation(operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName(HTTP_ASTECI, "solicitudToken"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {        final java.lang.Object _resp = _call.invoke(new java.lang.Object[] {codCliente, codIdAplicacion, codLang, codOperacion, codContratoEnlace, codCuentaCargo, codClienteEmpresa});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
        } catch (org.apache.axis.AxisFault axisFaultException) {
        	throw (java.rmi.RemoteException) axisFaultException ;
        }
    }

}
