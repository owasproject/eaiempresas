package com.isban.eai.enlace.client.sts.trivoli;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Implementation class for a response. Note that this is Higgins-aware
 *
 */
public class RequestSecurityTokenResponseImpl implements
		IRequestSecurityTokenResponse {

	/**
	 * _requestedSecurityToken
	 */
	transient Element requestedSecurityToken = null;

	/**
	 * _requestedAttachedReference
	 */
	transient Element requestedAttachedReference = null;

	/**
	 * _higginsResp
	 */
	transient org.eclipse.higgins.sts.api.IRequestSecurityTokenResponse higginsResp = null;

	/**
	 * RequestSecurityTokenResponseImpl
	 */
	protected RequestSecurityTokenResponseImpl() {

	}

	/** {@inheritDoc} */
	public Element getRequestedAttachedReference() {
		if (requestedAttachedReference == null) {
			requestedAttachedReferenceElementFromContainer();
		}
		return requestedAttachedReference;

	}

	/** {@inheritDoc} */
	public Element getRequestedSecurityToken() {
		if (requestedSecurityToken == null) {
			retrieveSecurityTokenElementFromContainer();
		}
		return requestedSecurityToken;
	}

	/**
	 * @param resp : resp
	 * @return result
	 */
	public static RequestSecurityTokenResponseImpl convertHigginsRequestSecurityTokenResponseToLocal(
			org.eclipse.higgins.sts.api.IRequestSecurityTokenResponse resp) {
		final RequestSecurityTokenResponseImpl result = new RequestSecurityTokenResponseImpl();

		result.setHigginsRequestSecurityTokenResponse(resp);

		return result;
	}
		
	/**
	 * retrieveSecurityTokenElementFromContainer
	 */
	private void retrieveSecurityTokenElementFromContainer() {
		if (higginsResp == null) {
			return;
		}

		Element requestedSecurityTokenContainer = null;
		try {
			requestedSecurityTokenContainer = (Element) higginsResp
					.getRequestedSecurityToken().getAs(Element.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		if (requestedSecurityTokenContainer != null) {

			final NodeList nl = requestedSecurityTokenContainer.getChildNodes();

			// Get the first Element Node and break
			for (int i = 0; i < nl.getLength(); i++) {
				final Node n = nl.item(i);
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					requestedSecurityToken = (Element) n;

					break;
				}
			}
		}
	}
	
	
	/**
	 * requestedAttachedReferenceElementFromContainer
	 */
	private void requestedAttachedReferenceElementFromContainer() {
		if (higginsResp == null) {
			return;
		}

		Element requestedAttachedReferenceElementContainer = null;
		try {
			requestedAttachedReferenceElementContainer = (Element) higginsResp
					.getRequestedAttachedReference().getAs(Element.class);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		if (requestedAttachedReferenceElementContainer != null) {

			final NodeList nl = requestedAttachedReferenceElementContainer
					.getChildNodes();

			// Get the first Element Node and break
			for (int i = 0; i < nl.getLength(); i++) {
				final Node n = nl.item(i);
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					requestedAttachedReference = (Element) n;

					break;
				}
			}
		}

	}

	/**
	 * @param resp : resp
	 */
	private void setHigginsRequestSecurityTokenResponse(
			org.eclipse.higgins.sts.api.IRequestSecurityTokenResponse resp) {
		higginsResp = resp;
	}

}
