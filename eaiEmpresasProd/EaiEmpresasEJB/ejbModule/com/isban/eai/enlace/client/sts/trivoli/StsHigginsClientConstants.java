package com.isban.eai.enlace.client.sts.trivoli;

import java.net.URI;

import org.eclipse.higgins.sts.api.IConstants;

/**
 * Defines a set of constants for namespaces and other URI values.
 * 
 */

public class StsHigginsClientConstants implements IConstants {	
	
	/**
	 * SOAP Fault Code for request was invalid or malformed
	 */
	private final static transient String INVALID_REQUEST_FAULT_CODE = "InvalidRequest";

	/**
	 * SOAP Fault Code for Authentication failed
	 */
	private final static transient String FAILED_AUTHENTICATION_FAULT_CODE = "FailedAuthentication";

	/**
	 * SOAP Fault Code for specified request failed
	 */
	private final static transient String REQUEST_FAILED_FAULT_CODE = "RequestFailed";

	/**
	 * SOAP Fault Code for Security token has been revoked
	 */
	private final static transient String INVALID_SECURITY_TOKEN_FAULT_CODE = "InvalidSecurityToken";

	/**
	 * SOAP Fault Code for Insufficient Digest Elements
	 */
	private final static transient String AUTHENTICATION_BAD_ELEMENTS_FAULT_CODE = "AuthenticationBadElements";

	/**
	 * SOAP Fault Code for specified RequestSecurityToken is not understood
	 */
	private final static transient String BAD_REQUEST_FAULT_CODE = "BadRequest";

	/**
	 * SOAP Fault Code for request data is out-of-date
	 */
	private final static transient String EXPIRED_DATA_FAULT_CODE = "ExpiredData";

	/**
	 * SOAP Fault Code for requested time range is invalid or unsupported
	 */
	private final static transient String INVALID_TIME_RANGE_FAULT_CODE = "InvalidTimeRange";

	/**
	 * SOAP Fault Code for request scope is invalid or unsupported
	 */
	private final static transient String INVALID_SCOPE_FAULT_CODE = "InvalidScope";

	/**
	 * SOAP Fault Code for renewable security token has expired
	 */
	private final static transient String RENEW_NEEDED_FAULT_CODE = "RenewNeeded";

	/**
	 * SOAP Fault Code for requested renewal failed
	 */
	private final static transient String UNABLE_TO_RENEW_FAULT_CODE = "UnableToRenew";

	/**
	 * Token Type URI for SAML 1.0 Assertions
	 */
	private final transient java.net.URI uriSAML10TokenType = java.net.URI
			.create("urn:oasis:names:tc:SAML:1.0:assertion");

	/**
	 * Token Type URI for SAML 2.0 Assertions
	 */
	private final transient java.net.URI uriSAML20TokenType = java.net.URI
			.create("urn:oasis:names:tc:SAML:2.0:assertion");

	/**
	 * uriIdentityNamespace
	 */
	private final transient java.net.URI uriIdentityNamespace = java.net.URI
			.create("http://schemas.xmlsoap.org/ws/2005/05/identity");

	/**
	 * Namespace URI used for InfoCard defined claims
	 */
	private final transient java.net.URI uriIdentityClaimsNamespace = java.net.URI
			.create(uriIdentityNamespace + "/claims");

	/**
	 * Namespace URI used for InfoCard extensions to
	 */
	private final transient java.net.URI uriAddressingIdentityNamespace = java.net.URI
			.create("http://schemas.xmlsoap.org/ws/2006/02/addressingidentity");

	/**
	 * Namespace URI for SAML 1.0 Assertions
	 */
	private final transient java.net.URI uriSAML10Namespace = java.net.URI
			.create("urn:oasis:names:tc:SAML:1.0:assertion");

	
	/**
	 * uriSAML10ConfirmationMethodBearer
	 */
	private final transient java.net.URI uriSAML10ConfirmationMethodBearer = java.net.URI
			.create("urn:oasis:names:tc:SAML:1.0:cm:bearer");

	/**
	 * uriSAML10ConfirmationMethodHolderOfKey
	 */
	private final transient java.net.URI uriSAML10ConfirmationMethodHolderOfKey = java.net.URI
			.create("urn:oasis:names:tc:SAML:1.0:cm:holder-of-key");

	/**
	 * Namespace URI for SAML 2.0 Assertions
	 */
	private final transient java.net.URI uriSAML20Namespace = java.net.URI
			.create("urn:oasis:names:tc:SAML:2.0:assertion");

	/**
	 * Namespace URI for Web Services Addressing 1.0 - Core W3C Recommendation 9
	 * May 2006
	 */
	private transient java.net.URI uriWSAddressingNamespace = java.net.URI
			.create("http://www.w3.org/2005/08/addressing");

	/**
	 * Core Namespace URI for Web Services Security: SOAP Message Security 1.0
	 * (WS-Security 2004) OASIS Standard 200401, March 2004
	 */
	private final transient java.net.URI uriWSSecurityNamespace = java.net.URI
			.create("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

	/**
	 * Utility Namespace URI for Web Services Security: SOAP Message Security
	 * 1.0 (WS-Security 2004) OASIS Standard 200401, March 2004
	 */
	private final transient java.net.URI uriWSSecurityUtilityNamespace = java.net.URI
			.create("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

	/**
	 * Namespace URI for Web Services Trust Language (WS-Trust) February 2005
	 */
	private final transient java.net.URI uriWSTrustNamespace = java.net.URI
			.create( /* "http://docs.oasis-open.org/ws-sx/ws-trust/200512" */"http://schemas.xmlsoap.org/ws/2005/02/trust");

	/**
	 * Namespace URI for Web Services Security Policy Language
	 * (WS-SecurityPolicy) July 2005 Version 1.1
	 */
	private final transient java.net.URI uriWSSecurityPolicyNamespace = java.net.URI
			.create("http://schemas.xmlsoap.org/ws/2005/07/securitypolicy");

	/**
	 * Namespace URI for Web Services Policy Language (WS-Policy) September 2004
	 */
	private final transient java.net.URI uriWSPolicyNamespace = java.net.URI
			.create("http://schemas.xmlsoap.org/ws/2004/09/policy");

	/**
	 * Namespace URI for XML-Signature Syntax and Processing W3C Recommendation
	 * 12 February 2002
	 */
	private final transient java.net.URI uriXMLSignatureNamespace = java.net.URI
			.create("http://www.w3.org/2000/09/xmldsig#");

	/**
	 * Namespace URI for XML Encryption Syntax and Processing W3C Recommendation
	 * 10 December 2002
	 */
	private final transient java.net.URI uriXMLEncryptionNamespace = java.net.URI
			.create("http://www.w3.org/2001/04/xmlenc#");

	/**
	 * Token Issuer URI for Self Issued Tokens
	 */
	private final transient java.net.URI uriIssuerSelf = java.net.URI
			.create("http://schemas.xmlsoap.org/ws/2005/05/identity/issuer/self");

	/**
	 * Request Type URI for Issue (RequestSecurityToken)
	 */
	private final transient java.net.URI uriIssueRequestType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/Issue");

	/**
	 * WS-Addressing Action URI for Issue Request
	 */
	private final transient java.net.URI uriIssueRequestAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RST/Issue");

	/**
	 * WS-Addressing Action URI for Issue Response
	 */
	private final transient java.net.URI uriIssueResponseAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RSTR/Issue");

	/**
	 * Request Type URI for Renew (RequestSecurityToken)
	 */
	private final transient java.net.URI uriRenewRequestType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/Renew");

	/**
	 * WS-Addressing Action URI for Renew Request
	 */
	private final transient java.net.URI uriRenewRequestAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RST/Renew");

	/**
	 * WS-Addressing Action URI for Renew Response
	 */
	private final transient java.net.URI uriRenewResponseAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RSTR/Renew");

	/**
	 * Request Type URI for Cancel (RequestSecurityToken)
	 */
	private final transient java.net.URI uriCancelRequestType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/Cancel");

	/**
	 * WS-Addressing Action URI for Cancel Request
	 */
	private final transient java.net.URI uriCancelRequestAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RST/Cancel");

	/**
	 * WS-Addressing Action URI for Cancel Response
	 */
	private final transient java.net.URI uriCancelResponseAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RSTR/Cancel");

	/**
	 * Request Type URI for Validate (RequestSecurityToken)
	 */
	private final transient java.net.URI uriValidateRequestType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/Validate");

	/**
	 * WS-Addressing Action URI for Validate Request
	 */
	private final transient java.net.URI uriValidateRequestAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RST/Validate");

	/**
	 * WS-Addressing Action URI for Validate Response
	 */
	private final transient java.net.URI uriValidateResponseAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RSTR/Validate");

	/**
	 * Request Type URI for Key Exchange (RequestSecurityToken)
	 */
	private final transient java.net.URI uriKeyExchangeRequestType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/KET");

	/**
	 * WS-Addressing Action URI for Key Exchange Request
	 */
	private final transient java.net.URI uriKeyExchangeRequestAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RST/KET");

	/**
	 * WS-Addressing Action URI for Key Exchange Response
	 */
	private final transient java.net.URI uriKeyExchangeResponseAction = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RSTR/KET");

	/**
	 * Token Type URI for Status (Validate)
	 */
	private final transient java.net.URI uriStatusTokenType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/RSTR/Status");

	/**
	 * Status Code URI for Valid
	 */
	private final transient java.net.URI uriValidStatusCode = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/status/valid");

	/**
	 * Status Code URI for Invalid
	 */
	private final transient java.net.URI uriInvalidStatusCode = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/status/invalid");

	/**
	 * Computed Key URI for PSHA1 Algorithm
	 */
	private final transient java.net.URI uriPSHA1ComputedKey = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/CK/PSHA1");

	/**
	 * Computed Key URI for HASH Algorithm
	 */
	private final transient java.net.URI uriHashComputedKey = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/CK/HASH");

	/**
	 * Binary Secret Type URI for Asymmetric Key
	 */
	private final transient java.net.URI uriAsymmetricKeyBinarySecretType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/AsymmetricKey");

	/**
	 * Binary Secret Type URI for Asymmetric Key
	 */
	private final transient java.net.URI uriSymmetricKeyBinarySecretType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/SymmetricKey");

	/**
	 * Binary Secret Type URI for Asymmetric Key
	 */
	private final transient java.net.URI uriNonceBinarySecretType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/Nonce");

	/**
	 * Key Type URI for Public Key
	 */
	private final transient java.net.URI uriAsymmetricKeyType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/PublicKey");

	/**
	 * Key Type URI for Symmetric Key
	 */
	private final transient java.net.URI uriSymmetricKeyType = java.net.URI
			.create(uriWSTrustNamespace.toString() + "/SymmetricKey");

	/**
	 * uriNoProofKey
	 */
	private final transient java.net.URI uriNoProofKey = java.net.URI
			.create(uriIdentityNamespace.toString() + "/NoProofKey");
	
	/**
	 * @return the uriSAML20TokenType
	 */
	public java.net.URI getSAML20TokenType() {
		return uriSAML20TokenType;
	}

	/**
	 * @return the uriSAML20Namespace
	 */
	public java.net.URI getSAML20Namespace() {
		return uriSAML20Namespace;
	}

	/**
	 * @return the uriAddressingIdentityNamespace
	 */
	public java.net.URI getAddressingIdentityNamespace() {
		return uriAddressingIdentityNamespace;
	}

	/**
	 * @return the uriNoProofKey
	 */
	public java.net.URI getNoProofKeyKeyType() {
		return uriNoProofKey;
	}

	/**
	 * @return the uriIdentityClaimsNamespace
	 */
	public java.net.URI getIdentityClaimsNamespace() {
		return uriIdentityClaimsNamespace;
	}

	/**
	 * @return the java.net.URI
	 */
	public java.net.URI getIdentityClaimPrivatePersonalIdentifier() {
		return java.net.URI.create(uriIdentityClaimsNamespace.toString()
				+ "/privatepersonalidentifier");
	}

	/**
	 * @return the uriIdentityNamespace
	 */
	public java.net.URI getIdentityNamespace() {
		return uriIdentityNamespace;
	}

	/**
	 * @return the uriAsymmetricKeyBinarySecretType
	 */
	public java.net.URI getAsymmetricKeyBinarySecretType() {
		return uriAsymmetricKeyBinarySecretType;
	}

	/**
	 * @return the uriAsymmetricKeyType
	 */
	public java.net.URI getAsymmetricKeyType() {
		return uriAsymmetricKeyType;
	}

	/**
	 * @return the AUTHENTICATION_BAD_ELEMENTS_FAULT_CODE
	 */
	public String getAuthenticationBadElementsFaultCode() {
		return AUTHENTICATION_BAD_ELEMENTS_FAULT_CODE;
	}

	/**
	 * @return the BAD_REQUEST_FAULT_CODE
	 */
	public String getBadRequestFaultCode() {
		return BAD_REQUEST_FAULT_CODE;
	}

	/**
	 * @return the uriCancelRequestAction
	 */
	public java.net.URI getCancelRequestAction() {
		return uriCancelRequestAction;
	}

	/**
	 * @return the uriCancelRequestType
	 */
	public java.net.URI getCancelRequestType() {
		return uriCancelRequestType;
	}

	/**
	 * @return the uriCancelResponseAction
	 */
	public java.net.URI getCancelResponseAction() {
		return uriCancelResponseAction;
	}

	/**
	 * @return the EXPIRED_DATA_FAULT_CODE
	 */
	public String getExpiredDataFaultCode() {
		return EXPIRED_DATA_FAULT_CODE;
	}

	/**
	 * @return the FAILED_AUTHENTICATION_FAULT_CODE
	 */
	public String getFailedAuthenticationFaultCode() {
		return FAILED_AUTHENTICATION_FAULT_CODE;
	}

	/**
	 * @return the uriHashComputedKey
	 */
	public java.net.URI getHashComputedKey() {
		return uriHashComputedKey;
	}

	/**
	 * @return the InvalidRequestFaultCode
	 */
	public String getInvalidRequestFaultCode() {
		return INVALID_REQUEST_FAULT_CODE;
	}

	/**
	 * @return the INVALID_SCOPE_FAULT_CODE
	 */ 
	public String getInvalidScopeFaultCode() {
		return INVALID_SCOPE_FAULT_CODE;
	}

	/**
	 * @return the INVALID_SECURITY_TOKEN_FAULT_CODE
	 */
	public String getInvalidSecurityTokenFaultCode() {
		return INVALID_SECURITY_TOKEN_FAULT_CODE;
	}

	/**
	 * @return the uriInvalidStatusCode
	 */
	public java.net.URI getInvalidStatusCode() {
		return uriInvalidStatusCode;
	}

	/**
	 * @return the INVALID_TIME_RANGE_FAULT_CODE
	 */
	public String getInvalidTimeRangeFaultCode() {
		return INVALID_TIME_RANGE_FAULT_CODE;
	}

	/**
	 * @return the uriIssueRequestAction
	 */
	public java.net.URI getIssueRequestAction() {
		return uriIssueRequestAction;
	}

	/**
	 * @return the uriIssueRequestType
	 */
	public java.net.URI getIssueRequestType() {
		return uriIssueRequestType;
	}

	/**
	 * @return the uriIssueResponseAction
	 */
	public java.net.URI getIssueResponseAction() {
		return uriIssueResponseAction;
	}

	/**
	 * @return the uriIssuerSelf
	 */
	public java.net.URI getIssuerSelf() {
		return uriIssuerSelf;
	}

	/**
	 * @return the uriKeyExchangeRequestAction
	 */
	public java.net.URI getKeyExchangeRequestAction() {
		return uriKeyExchangeRequestAction;
	}

	/**
	 * @return the uriKeyExchangeRequestType
	 */
	public java.net.URI getKeyExchangeRequestType() {
		return uriKeyExchangeRequestType;
	}

	/**
	 * @return the uriKeyExchangeResponseAction
	 */
	public java.net.URI getKeyExchangeResponseAction() {
		return uriKeyExchangeResponseAction;
	}

	/**
	 * @return the uriNonceBinarySecretType
	 */
	public java.net.URI getNonceBinarySecretType() {
		return uriNonceBinarySecretType;
	}

	/**
	 * @return the uriPSHA1ComputedKey
	 */
	public java.net.URI getPSHA1ComputedKey() {
		return uriPSHA1ComputedKey;
	}

	/**
	 * @return the RENEW_NEEDED_FAULT_CODE
	 */
	public String getRenewNeededFaultCode() {
		return RENEW_NEEDED_FAULT_CODE;
	}

	/**
	 * @return the uriRenewRequestAction
	 */
	public java.net.URI getRenewRequestAction() {
		return uriRenewRequestAction;
	}

	/**
	 * @return the uriRenewRequestType
	 */
	public java.net.URI getRenewRequestType() {
		return uriRenewRequestType;
	}

	/**
	 * @return the uriRenewResponseAction
	 */
	public java.net.URI getRenewResponseAction() {
		return uriRenewResponseAction;
	}

	/**
	 * @return the REQUEST_FAILED_FAULT_CODE
	 */
	public String getRequestFailedFaultCode() {
		return REQUEST_FAILED_FAULT_CODE;
	}

	/**
	 * @return the uriStatusTokenType
	 */
	public java.net.URI getStatusTokenType() {
		return uriStatusTokenType;
	}

	/**
	 * @return the uriSymmetricKeyBinarySecretType
	 */
	public java.net.URI getSymmetricKeyBinarySecretType() {
		return uriSymmetricKeyBinarySecretType;
	}

	/**
	 * @return the uriSymmetricKeyType
	 */
	public java.net.URI getSymmetricKeyType() {
		return uriSymmetricKeyType;
	}

	/**
	 * @return the UNABLE_TO_RENEW_FAULT_CODE
	 */
	public String getUnableToRenewFaultCode() {
		return UNABLE_TO_RENEW_FAULT_CODE;
	}

	/**
	 * @return the uriValidateRequestAction
	 */
	public java.net.URI getValidateRequestAction() {
		return uriValidateRequestAction;
	}

	/**
	 * @return the uriValidateRequestType
	 */
	public java.net.URI getValidateRequestType() {
		return uriValidateRequestType;
	}

	/**
	 * @return the uriValidateResponseAction
	 */
	public java.net.URI getValidateResponseAction() {
		return uriValidateResponseAction;
	}

	/**
	 * @return the uriValidStatusCode
	 */
	public java.net.URI getValidStatusCode() {
		return uriValidStatusCode;
	}

	/**
	 * @return the uriWSAddressingNamespace
	 */
	public java.net.URI getWSAddressingNamespace() {
		return uriWSAddressingNamespace;
	}

	/**
	 * @return the uriWSSecurityNamespace
	 */
	public java.net.URI getWSSecurityNamespace() {
		return uriWSSecurityNamespace;
	}

	/**
	 * @return the uriWSSecurityPolicyNamespace
	 */
	public java.net.URI getWSSecurityPolicyNamespace() {
		return uriWSSecurityPolicyNamespace;
	}

	/**
	 * @return the uriWSPolicyNamespace
	 */
	public java.net.URI getWSPolicyNamespace() {
		return uriWSPolicyNamespace;
	}

	/**
	 * @return the uriWSSecurityUtilityNamespace
	 */
	public java.net.URI getWSSecurityUtilityNamespace() {
		return uriWSSecurityUtilityNamespace;
	}

	/**
	 * @return the uriWSTrustNamespace
	 */
	public java.net.URI getWSTrustNamespace() {
		return uriWSTrustNamespace;
	}

	/**
	 * @return the uriXMLEncryptionNamespace
	 */
	public java.net.URI getXMLEncryptionNamespace() {
		return uriXMLEncryptionNamespace;
	}

	/**
	 * @return the uriXMLSignatureNamespace
	 */
	public java.net.URI getXMLSignatureNamespace() {
		return uriXMLSignatureNamespace;
	}
	
	/**
	 * @return the uriSAML10Namespace
	 */
	public URI getSAML10Namespace() {
		return uriSAML10Namespace;
	}

	/**
	 * @return the uriSAML10TokenType
	 */
	public URI getSAML10TokenType() {
		return uriSAML10TokenType;
	}

	/**
	 * @return the uriSAML10ConfirmationMethodBearer
	 */
	public java.net.URI getSAML10ConfirmationMethodBearer() {
		return uriSAML10ConfirmationMethodBearer;
	}

	/**
	 * @return the uriSAML10ConfirmationMethodHolderOfKey
	 */
	public java.net.URI getSAML10ConfirmationMethodHolderOfKey() {
		return uriSAML10ConfirmationMethodHolderOfKey;
	}

	/**
	 * @param wsaNs : wsaNs
	 */
	public void setWSAddressingNamespace(URI wsaNs) {
		uriWSAddressingNamespace = wsaNs;
	}
}
