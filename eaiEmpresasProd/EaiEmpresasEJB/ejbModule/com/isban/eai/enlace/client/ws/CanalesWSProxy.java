package com.isban.eai.enlace.client.ws;

public class CanalesWSProxy implements com.isban.eai.enlace.client.ws.CanalesWS {
  
	/**
	 * _endpoint
	 */
	private String endpoint = null;
  
	/**
	 * canalesWS
	 */
	private transient com.isban.eai.enlace.client.ws.CanalesWS canalesWS = null;
  
 
  	/**
  	 * Constructor
  	 */
  	public CanalesWSProxy() {
  		_initCanalesWSProxy();
  	}
  
  	/**
  	 * CanalesWSProxy
  	 * @param endpoint punto final del WS
  	 */
  	public CanalesWSProxy(String endpoint) {
  		endpoint = endpoint;
  		_initCanalesWSProxy();
  	}
  
  	/**
  	 * _initCanalesWSProxy
  	 */
  	private void _initCanalesWSProxy() {
    try {
      canalesWS = (new com.isban.eai.enlace.client.ws.CanalesWSServiceLocator()).getCanalesWS();
      if (canalesWS != null) {
        if (endpoint != null){
          ((javax.xml.rpc.Stub)canalesWS)._setProperty("javax.xml.rpc.service.endpoint.address", endpoint);
        }else{
        	endpoint = (String)((javax.xml.rpc.Stub)canalesWS)._getProperty("javax.xml.rpc.service.endpoint.address");
        }
      }
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  	/**
  	 * getEndpoint
  	 * @return _endpoint final del WS
  	 */
  	public String getEndpoint() {
  		return endpoint;
  	}
  
  	/**
  	 * setEndpoint
  	 * @param endpoint final del WS
  	 */
  	public void setEndpoint(String endpoint) {
  		endpoint = endpoint;
  		if (canalesWS != null){
  			((javax.xml.rpc.Stub)canalesWS)._setProperty("javax.xml.rpc.service.endpoint.address", endpoint);
  		}
    
  	}
  
  	/**
  	 * getCanalesWS
  	 * @return canalesWS canal del WS
  	 */
  	public com.isban.eai.enlace.client.ws.CanalesWS getCanalesWS() {
  		if (canalesWS == null){
  			_initCanalesWSProxy();
  		}
  		return canalesWS;
  	}
  
  	/** {@inheritDoc} */
  	public java.lang.String[] estatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException{
  		if (canalesWS == null){
  			_initCanalesWSProxy();
  		}
  		return canalesWS.estatus(codCliente, codIdAplicacion, codOperacion);
  	}
  
  	/** {@inheritDoc} */
  	public java.lang.String[] contrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException{
  		if (canalesWS == null){
  			_initCanalesWSProxy();
  		}
  		return canalesWS.contrato(codCliente, codIdAplicacion, codLang, codOperacion);
  	}
  
  	/** {@inheritDoc} */
  	public java.lang.String[][] consultaListaEstatus(java.lang.String[] lista, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException{
  		if (canalesWS == null){
  			_initCanalesWSProxy();
  		}
  		return canalesWS.consultaListaEstatus(lista, codIdAplicacion, codLang, codOperacion);
  	}
  
  	/** {@inheritDoc} */
  	public java.lang.String[] bloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException{
  		if (canalesWS == null){
  			_initCanalesWSProxy();
  		}
  		return canalesWS.bloqueo(codCliente, codIdAplicacion, codLang, codMotivo);
  	}
  
  	/** {@inheritDoc} */
  	public java.lang.String[] desBloqueo(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException{
  		if (canalesWS == null){
  			_initCanalesWSProxy();
  		}
  		return canalesWS.desBloqueo(codCliente, codIdAplicacion);
  	}
  
  	/** {@inheritDoc} */
  	public java.lang.String[] activacion(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException{
  		if (canalesWS == null){
  			_initCanalesWSProxy();
  		}
  		return canalesWS.activacion(codCliente, codIdAplicacion, codLang);
  	}
  
  	/** {@inheritDoc} */
public java.lang.String[] solicitud(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException{
    if (canalesWS == null){
      _initCanalesWSProxy();
    }
    return canalesWS.solicitud(codCliente, codIdAplicacion, codLang, codOperacion, codContratoEnlace, codCuentaCargo, codClienteEmpresa);
  }
  
/** {@inheritDoc} */
public java.lang.String[] consultaEstatus(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codOperacion) throws java.rmi.RemoteException{
    if (canalesWS == null){
      _initCanalesWSProxy();
    }
    return canalesWS.consultaEstatus(codCliente, codIdAplicacion, codOperacion);
  }
  
/** {@inheritDoc} */
public java.lang.String[] consultaContrato(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion) throws java.rmi.RemoteException{
    if (canalesWS == null){
      _initCanalesWSProxy();
    }
    return canalesWS.consultaContrato(codCliente, codIdAplicacion, codLang, codOperacion);
  }
  
/** {@inheritDoc} */
public java.lang.String[] activarToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang) throws java.rmi.RemoteException{
    if (canalesWS == null){
      _initCanalesWSProxy();
    }
    return canalesWS.activarToken(codCliente, codIdAplicacion, codLang);
  }
  
/** {@inheritDoc} */
public java.lang.String[] bloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codMotivo) throws java.rmi.RemoteException{
    if (canalesWS == null){
      _initCanalesWSProxy();
    }
    return canalesWS.bloquearToken(codCliente, codIdAplicacion, codLang, codMotivo);
  }
  
/** {@inheritDoc} */
public java.lang.String[] desBloquearToken(java.lang.String codCliente, java.lang.String codIdAplicacion) throws java.rmi.RemoteException{
    if (canalesWS == null){
      _initCanalesWSProxy();
    }
    return canalesWS.desBloquearToken(codCliente, codIdAplicacion);
  }
  
/** {@inheritDoc} */
public java.lang.String[] solicitudToken(java.lang.String codCliente, java.lang.String codIdAplicacion, java.lang.String codLang, java.lang.String codOperacion, java.lang.String codContratoEnlace, java.lang.String codCuentaCargo, java.lang.String codClienteEmpresa) throws java.rmi.RemoteException{
    if (canalesWS == null){
      _initCanalesWSProxy();
    }
    return canalesWS.solicitudToken(codCliente, codIdAplicacion, codLang, codOperacion, codContratoEnlace, codCuentaCargo, codClienteEmpresa);
  }
  
  
}