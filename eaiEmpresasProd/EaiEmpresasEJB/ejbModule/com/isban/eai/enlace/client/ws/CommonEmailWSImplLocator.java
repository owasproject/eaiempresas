/**
 * CommonEmailWSImplLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.client.ws;

import com.isban.eai.enlace.util.EnlaceConfig;

public class CommonEmailWSImplLocator extends org.apache.axis.client.Service implements com.isban.eai.enlace.client.ws.CommonEmailWSImpl {
    
    
    /**
     * CommonEmailServiceImplPort
     */
    private final static String COMMON_EMAIL = "CommonEmailServiceImplPort";
    
    /**
     * The WSDD service name defaults to the port name.
     */
    private java.lang.String commonEmailServiceImplPortWSDDServiceName = COMMON_EMAIL;
    
    /**
     * Use to get a proxy class for CommonEmailServiceImplPort
     */
    private transient java.lang.String commonEmailServiceImplPortAddress = EnlaceConfig.RUTA_WEB_SERVICE_EMAIL;
    
    /**
     * Obtiene ports
     */
    private transient java.util.Set ports = null;
	
    /**
     * Constructor
     */
    public CommonEmailWSImplLocator() {
    }
    
    /**
     * CommonEmailWSImplLocator
     * @param config configuracion del WSEmail
     */
    public CommonEmailWSImplLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    /**
     * CommonEmailWSImplLocator
     * @param wsdlLoc ubicacion del WS
     * @param sName nombre del WS
     * @throws javax.xml.rpc.ServiceException manejo de excepcion
     */
    public CommonEmailWSImplLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    /** {@inheritDoc} */
    public java.lang.String getCommonEmailServiceImplPortAddress() {
        return commonEmailServiceImplPortAddress;
    }

    /**
     * getCommonEmailServiceImplPortWSDDServiceName
     * @return CommonEmailServiceImplPortWSDDServiceName nombre del WS mail
     */
    public java.lang.String getCommonEmailServiceImplPortWSDDServiceName() {
        return commonEmailServiceImplPortWSDDServiceName;
    }

    /**
     * setCommonEmailServiceImplPortWSDDServiceName
     * @param name nombre del WS mail
     */
    public void setCommonEmailServiceImplPortWSDDServiceName(java.lang.String name) {
    	commonEmailServiceImplPortWSDDServiceName = name;
    }

    /** {@inheritDoc} */
    public com.isban.eai.enlace.client.ws.CommonEmailServiceImpl getCommonEmailServiceImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(commonEmailServiceImplPortAddress);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCommonEmailServiceImplPort(endpoint);
    }

    /** {@inheritDoc} */
    public com.isban.eai.enlace.client.ws.CommonEmailServiceImpl getCommonEmailServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            final com.isban.eai.enlace.client.ws.CommonEmailServiceImplPortBindingStub _stub = new com.isban.eai.enlace.client.ws.CommonEmailServiceImplPortBindingStub(portAddress, this);
            _stub.setPortName(getCommonEmailServiceImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    /**
     * setCommonEmailServiceImplPortEndpointAddress
     * @param address direccion del WS mail
     */
    public void setCommonEmailServiceImplPortEndpointAddress(java.lang.String address) {
    	commonEmailServiceImplPortAddress = address;
    }

    /** {@inheritDoc} */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.isban.eai.enlace.client.ws.CommonEmailServiceImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                final com.isban.eai.enlace.client.ws.CommonEmailServiceImplPortBindingStub _stub = new com.isban.eai.enlace.client.ws.CommonEmailServiceImplPortBindingStub(new java.net.URL(commonEmailServiceImplPortAddress), this);
                _stub.setPortName(getCommonEmailServiceImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /** {@inheritDoc} */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        final java.lang.String inputPortName = portName.getLocalPart();
        if (COMMON_EMAIL.equals(inputPortName)) {
            return getCommonEmailServiceImplPort();
        }
        else  {
            final java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    /** {@inheritDoc} */
    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.email.common.santander.com/", "CommonEmailWSImpl");
    }

    /** {@inheritDoc} */
    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.email.common.santander.com/", COMMON_EMAIL));
        }
        return ports.iterator();
    }

    /**
     * Set the endpoint address for the specified port name.
     * @param portName nombre del WS mail
     * @param address direccion del WS mail
     * @throws javax.xml.rpc.ServiceException manejo de excepcion
     */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
    	if (COMMON_EMAIL.equals(portName)) {
            setCommonEmailServiceImplPortEndpointAddress(address);
        }
        else 
        { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
     * Set the endpoint address for the specified port name.
     * @param portName nombre del puero WS mail
     * @param address direccion del puerto WS mail 
     * @throws javax.xml.rpc.ServiceException manejo de excepcion
     */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
