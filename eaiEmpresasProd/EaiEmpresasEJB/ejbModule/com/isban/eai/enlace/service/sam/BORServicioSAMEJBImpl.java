/**
 * Isban Mexico
 *   Clase: BORServicioSAMEJBImpl.java
 *   Descripción: La implementacion del componente de negocio para la
 *   bitacora administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.service.sam;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.CmpRSA.bean.SiteToUserResponse;
import mx.isban.rsa.bean.ServiciosAAResponse;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.dto.RSADTO;
import com.isban.eai.enlace.dto.RequestAuthSAMDTO;
import com.isban.eai.enlace.dto.RespuestaAuthDTO;
import com.isban.eai.enlace.servicio.ConsumoCanalesServiceEJB;
import com.isban.eai.enlace.util.EjecutarRSA;
import com.isban.eai.enlace.util.EnlaceConfig;
import com.isban.ebe.commons.exception.BusinessException;
import com.tivoli.pd.jadmin.PDAdmin;
import com.tivoli.pd.jadmin.PDUser;
import com.tivoli.pd.jutil.PDContext;
import com.tivoli.pd.jutil.PDException;
import com.tivoli.pd.jutil.PDMessage;
import com.tivoli.pd.jutil.PDMessages;
import com.tivoli.pd.nls.pdbjamsg;
import com.tivoli.pd.rgy.RgyAttributes;
import com.tivoli.pd.rgy.RgyException;
import com.tivoli.pd.rgy.RgyRegistry;
import com.tivoli.pd.rgy.RgyUser;
import com.tivoli.pd.rgy.exception.WarningPasswordExpiresSoonRgyException;
import com.tivoli.pd.rgy.ldap.LdapRgyRegistryFactory;

/**
 * La implementacion del componente de negocio para la bitacora administrativa.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORServicioSAMEJBImpl implements
        BORServicioSAMEJB {
	
	/**
	 * configFile
	 */
	static String configFile = EnlaceConfig.URL_PD_PERM;
	
	/**LOG**/
    private static final Logger LOGGER = Logger.getLogger(BORServicioSAMEJBImpl.class);
    
    /**
     * Constante file
     */
    private static final String FILE = "file";
    

    /**deviceTokenCookie**/
    private static final String DEVICETOKENCOOKIE = "deviceTokenCookie";
    
    /**deviceTokenFSO**/
    private static final String DEVICETOKENFSO = "deviceTokenFSO";
    
    /**
     * Contante de mensaje de fallo
     */
    private static final String FAILED = "FAILED: Unable to obtain instance of LdapRegistry";
    
	/**
	 * objeto para ConsumoCanalesServiceEJB 
	 */
    @EJB
	private ConsumoCanalesServiceEJB canalesService;
    
    /**
	 * EjecutarRSA
	 */
    private EjecutarRSA ejecutaRsa = new EjecutarRSA();
    
	@Override
	public Map<String,Object> validarPassword(RequestAuthSAMDTO request, int accion, RSADTO bean) throws BusinessException{
		
		final String usr = request.getUser();
		final String pwd = request.getPassword();
		Map<String,Object> datos = new HashMap<String,Object>();
		
		final BORPDClientEJB clientAuth = BORPDClientEJBImpl.getInstance();
		
		final RespuestaAuthDTO respAuth = clientAuth.validaPwd(usr, pwd);
		LOGGER.info("Autenticado: " + respAuth.isAutenticado());
		LOGGER.info("Mensaje: " + respAuth.getErrorAcceso());
		
		if(respAuth.isAutenticado() && "true".equals(EnlaceConfig.BANDERA_RSA) && accion == 1){
			ServiciosAAResponse resp = ejecutaRsa.ejecutaCreateUser(bean);
			String deviceTokenFSO = "";
			String deviceTokenCookie = "";
			if(resp != null){
    			
	    	    deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO() != null
	    	    ? resp.getDeviceResult().getDeviceData().getDeviceTokenFSO() : null;
	    	    
	    	    deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie() != null
	    	    ? resp.getDeviceResult().getDeviceData().getDeviceTokenCookie() : null;
		    	
		    	datos.put(DEVICETOKENFSO, deviceTokenFSO);
			    datos.put(DEVICETOKENCOOKIE, deviceTokenCookie);
			    			    	
		    }
		}
		if(respAuth.isAutenticado() && "true".equals(EnlaceConfig.BANDERA_RSA) && accion == 2){
			ServiciosAAResponse resp1 = ejecutaRsa.ejecutaUpdateUser(bean);
			String deviceTokenFSO = "";
			String deviceTokenCookie = "";
			if(resp1 != null && resp1.getDeviceResult() != null){
    			
	    	    deviceTokenFSO = resp1.getDeviceResult().getDeviceData().getDeviceTokenFSO() != null
	    	    ? resp1.getDeviceResult().getDeviceData().getDeviceTokenFSO() : null;
	    	    
	    	    deviceTokenCookie = resp1.getDeviceResult().getDeviceData().getDeviceTokenCookie() != null
	    	    ? resp1.getDeviceResult().getDeviceData().getDeviceTokenCookie() : null;
		    	
		    	datos.put(DEVICETOKENFSO, deviceTokenFSO);
			    datos.put(DEVICETOKENCOOKIE, deviceTokenCookie);
			    			    	
		    }
		}
		datos.put("respAuth", respAuth);
		return datos;
	}
	
	@Override
	public boolean validaPassword(String newPassword, String userName) throws BusinessException, MalformedURLException {
		
		boolean resultado = false;
		/*------------------------------------------------------------------
         * Initialize the PDAdmin API.  This initialization is required
         * before the PDAdmin API can be used.
         *------------------------------------------------------------------
         */
		
		final String prog = "PDAdminDemo";
		final PDMessages msgs = new PDMessages();
		 
        LOGGER.info("Initializing PDAdmin...\n");
        try {
			PDAdmin.initialize(prog, msgs);
		} catch (PDException e1) {
			LOGGER.info("try pdadmin " + e1.getMessage()+"************************************"+e1.getCause());
		}
        processMsgs(msgs);
		
		//
		final String name = userName;
		final Locale locale = new Locale("ENGLISH", "US");
		final URL configURL = new URL(FILE, "", configFile);
	   
	    //nombre y constraseña para lo del sam
		//final String adminName = USUARIO_ADMIN;
		//final char[] adminPassword = CONTRASENA_ADMIN.toCharArray();
	    
		PDContext ctxt;
		PDUser pdUser;
		try {
			ctxt = new PDContext(locale,
			        //adminName,
			        //adminPassword,
			        configURL);
			
			
			pdUser = new PDUser(ctxt,
			        name,
			        msgs);
			

	        // Change the user's password
	        final String pwd = newPassword;
	        pdUser.setPassword(ctxt, pwd.toCharArray(), msgs);
	        resultado = pdUser.isPasswordValid();
	        processMsgs(msgs);
			
		} catch (PDException e) {
				LOGGER.error(e, e);
		}
		return resultado;
	}
	
	@Override
	public boolean cambiaPassword(String newPassword, String userName) throws BusinessException, MalformedURLException {
		
		/*------------------------------------------------------------------
         * Initialize the PDAdmin API.  This initialization is required
         * before the PDAdmin API can be used.
         *------------------------------------------------------------------
         */
		
		final String prog = "PDAdminDemo";
		final PDMessages msgs = new PDMessages();
		 
        LOGGER.info("Initializing PDAdmin...\n");
        try {
			PDAdmin.initialize(prog, msgs);
		} catch (PDException e1) {
			LOGGER.error(e1.getStackTrace());
		}
        processMsgs(msgs);
		
		//
        final String name = userName;
        final Locale locale = new Locale("ENGLISH", "US");
        final URL configURL = new URL(FILE, "", configFile);
	   
	    //nombre y constraseña para lo del sam
        //final String adminName = USUARIO_ADMIN;
		//final char[] adminPassword = CONTRASENA_ADMIN.toCharArray();
	    
		PDContext ctxt;
		PDUser pdUser;
		try {
			ctxt = new PDContext(locale,
			        //adminName,
			        //adminPassword,
			        configURL);
			
			
			pdUser = new PDUser(ctxt,
			        name,
			        msgs);
			

	        // Change the user's password
			final String pwd = newPassword;
	        LOGGER.info("Changing the user's password using the instance set method ...\n");
	        pdUser.setPassword(ctxt, pwd.toCharArray(), msgs);
	        processMsgs(msgs);
			
			return true;
			
		} catch (PDException e) {
				LOGGER.error(e.getStackTrace());
				return false;
		}
		
	}
	
	//Martin
//	@Override
//	public boolean bloquearUsuario(String userName) throws BusinessException, MalformedURLException {
//		/*------------------------------------------------------------------
//         * Initialize the PDAdmin API.  This initialization is required
//         * before the PDAdmin API can be used.
//         *------------------------------------------------------------------
//         */
//		
//		String prog = "PDAdminDemo";
//		PDMessages msgs = new PDMessages();
//		 
//        LOGGER.info("Initializing PDAdmin... bloqueo de usuario\n");
//        try {
//			PDAdmin.initialize(prog, msgs);
//		} catch (PDException e1) {
//			LOGGER.error(e1,e1);
//		}
//        processMsgs(msgs);
//		
//		//
//		String name = userName;
//		Locale locale = new Locale("ENGLISH", "US");
//	    URL configURL = new URL(FILE, "", configFile);
//	   
//	    //nombre y constraseña para lo del sam
//	    final String adminName = USUARIO_ADMIN;
//		final char[] adminPassword = CONTRASENA_ADMIN.toCharArray();
//	    
//		PDContext ctxt;
//		PDUser pdUser;
//		try {
//			LOGGER.info("Inicializando contexto...");
//			ctxt = new PDContext(locale,
//			        adminName,
//			        adminPassword,
//			        configURL);
//			
//			LOGGER.info("Inicializando PDUser...");
//			pdUser = new PDUser(ctxt,
//			        name,
//			        msgs);
//			
//			/*------------------------------------------------------------------
//	         * Set the account to valid using an instance method.  All set
//	         * instance methods set both the attributes
//	         * in the local copy and those at the Management Server.
//	         *------------------------------------------------------------------
//	         */
//
//			 LOGGER.info("Setting the user's account to invalid using the static set method ...\n");
//	         PDUser.setAccountValid(ctxt, name, false, msgs);
//	         processMsgs(msgs);
//	         return true;
//	         
//		} catch (PDException e) {
//			LOGGER.error(e,e);
//			return false;
//		}
//	
//	}
	
	/**
	 * processMsgs
	 * @param msgs mensaje
	 */
	static void processMsgs(PDMessages msgs)
	   {

	      /*---------------------------------------------------------------
	       * Most of the Tivoli Access Manager Java Admin API require an
	       * input PDMessages object.  The Admin API caller should always
	       * check this object for warning, informational or error messages
	       * that might have been generated during the operation, regardless
	       * of whether the operation succeeded or threw a PDException.
	       *---------------------------------------------------------------
	       */

	      if (msgs.size() > 0)
	      {
	         LOGGER.info("Msgs are: " + msgs + "\n");
	         msgs.clear();
	      }
	   }

	   /**
	 * @param e excepcion
	 */
	@SuppressWarnings("unchecked")
	static void handlePDException(Exception e)
	   {
		   final PDException pd = (PDException)e;
		   final PDMessages msgs = pd.getMessages();
		   final String MESSAGE = "Message text is: ";
	      if (msgs != null)
	      {
	    	  final Iterator pdi = msgs.iterator();
	         PDMessage msg = null;
	         int msgCode = 0;

	         /*---------------------------------------------------------------
	          * The Tivoli Access Manager Java Admin API will throw PDExceptions
	          * that have a single message code in the member PDMessages.
	          * However, the PDException class is designed so that multiple codes
	          * can be returned.  This way, a caller of the Java Admin API can
	          * "stack" another error code in the PDException, if desired,
	          * and rethrow the exception to its caller.
	          *---------------------------------------------------------------
	          */

	         while (pdi.hasNext())
	         {
	            msg = (PDMessage)pdi.next();
	            msgCode = msg.getMsgCode();
	            /*---------------------------------------------------------------
	             * Here are examples of generic Tivoli Access Manager message codes.
	             * They are available by importing one or more of the
	             * com.tivoli.pd.nls.pd*msg classes. These message codes are
	             * documented in the Error Message Reference.
	             *---------------------------------------------------------------
	             */
	            switch (msgCode)
	            {
	               case pdbjamsg.bja_invalid_msgs:
	                  LOGGER.info ("*** Invalid PDMessage Parameter ***");
	                  LOGGER.info (MESSAGE + msg.getMsgText() + "\n");
	                  break;
	               case pdbjamsg.bja_invalid_ctxt:
	                  LOGGER.info ("*** Invalid Context Parameter ***");
	                  LOGGER.info (MESSAGE + msg.getMsgText() + "\n");
	                  break;
	               case pdbjamsg.bja_cannot_contact_server:
	                  LOGGER.info ("*** The Server cannot be contacted ***");
	                  LOGGER.info (MESSAGE + msg.getMsgText() + "\n");
	                  break;
	               default:
	                  LOGGER.info (MESSAGE + msg.getMsgText() + "\n");
	                  break;
	            }
	         }
	      }
	      else
	      {
	         /*---------------------------------------------------------------
	          * A PDException with no messages typically means that a Java
	          * exception or error was thrown and wrappered in a PDException.
	          * To get the underlying exception or error, use the PDException
	          * getCause() method.
	          *---------------------------------------------------------------
	          */

	    	  final Throwable t = pd.getCause();
	          if (t != null)
	          {
	             LOGGER.info("*** Underlying Java Exception ***");
	             LOGGER.error(t.getStackTrace());
	          }
	      }
	   }

//	@Override
//	public RespuestaAuthDTO validarPasswordMock(RequestAuthSAMDTO request)
//			throws BusinessException {
//		
//		HttpServletRequest req = request.getRequest();
//		HttpServletResponse resp = request.getResponse();
//		String usr = request.getUser();
//		String pwd = request.getPassword();
//		
//		BORPDClientEJB clientAuth = BORPDClientEJBImpl.getInstance();
//		RespuestaAuthDTO respAuth = clientAuth.validaPwdMock(usr, pwd);
//		return respAuth;
//	}
	
	@Override
	/** {@inheritDoc} */
	public int autenticarUsr(String usr, String pwd) {
		
		//Respuesta 1 = exito, 2 = password expiro, 3 = fallo autenticación
		int respuesta = 0;
		
		URL propertiesUrl = null;
		try {
		    propertiesUrl = new URL(FILE, "", configFile);
		}
		catch (MalformedURLException e2) {
		    LOGGER.error(e2.getStackTrace());
		}
		
		RgyRegistry registry = null;
		try {
		    registry = LdapRgyRegistryFactory.getRgyRegistryInstance(
		    		propertiesUrl, null);
		}
		catch (RgyException e2) {
		    LOGGER.error(e2.getStackTrace());
		    LOGGER.info(FAILED);
		}

		RgyUser user = null;
		
		try {
			
			user = registry.getUser(null, usr);
			
		} catch (RgyException e) {
			
			LOGGER.info(">>> No se puedo recuper usuario: "   + e.getLocalizedMessage() + e.getMessage() + e.getStackTrace());
		}
		
		try {
			user.authenticate(pwd.toCharArray());
			LOGGER.info("SUCCESS: Autenticación exitosa.");
			respuesta = 1;
		}  catch (WarningPasswordExpiresSoonRgyException e) {
			// ignore to allow for future enhancement. The password
		    // was successfully authenticated, this is just a warning.
			LOGGER.info("WARNING: password will expire in "+e.getExpireTime()+" seconds."   + e.getLocalizedMessage() + e.getMessage() + e.getStackTrace());
			LOGGER.error(e.getStackTrace());
			respuesta = 2;
		}catch (RgyException e) {
			LOGGER.info("FAILED: authenticate failed" + " "  + e.getLocalizedMessage() + e.getMessage() + e.getStackTrace());
			LOGGER.error(e.getStackTrace());
			respuesta = 3;
		}
		
		LOGGER.info("Respuesta: "+respuesta);
		
		return respuesta;
	}

	//prueba Eduardo
	@Override
	/** {@inheritDoc} */
	public boolean bloquearUsuario(String userName) throws BusinessException,
			MalformedURLException {
		LOGGER.info("Entro a Bloqueando al usuario");
		boolean respuesta = false;
		
		URL propertiesUrl = null;
        try {
            propertiesUrl = new URL(FILE, "", configFile);
        }
        catch (MalformedURLException e2) {
            LOGGER.error(e2, e2);
            
        }
		
		RgyRegistry registry = null;
        try {
            registry = LdapRgyRegistryFactory.getRgyRegistryInstance(
            		propertiesUrl, null);
        }
        catch (RgyException e2) {
            LOGGER.error(e2, e2);
            LOGGER.info(FAILED);
            
        }
        
		RgyUser user = null;
		try {
			user = registry.getUser(null, userName);
		} catch (RgyException e) {
			LOGGER.error(e, e);
		}
		
        try {
            String estaBloq = user.getOneAttributeValue(RgyAttributes.SEC_ACCT_VALID_NAME).toString();
            LOGGER.info("ESTATUS DEL USUARIO: "+estaBloq);
            
            if(estaBloq.contains("TRUE")){
            	LOGGER.info("Bloqueando al usuario");
            	user.attributeReplace(RgyAttributes.SEC_ACCT_VALID_NAME,
                    RgyAttributes.BOOL_FALSE_VALUE);
                respuesta = true;
            }else{
            	LOGGER.info("EL USUARIO ESTABA BLOQUEADO CON ANTERIORIDAD. ");
            }
        }
        catch (RgyException e) {
        	LOGGER.error(e, e);
            LOGGER.info("FAILED: attributeReplace (disable) failed.");
            respuesta = false;
        }
        
		return respuesta;
	}

	@Override
	/** {@inheritDoc} */
	public String cambiaPassword(String antPassword, String newPassword, String userName)
			throws BusinessException, MalformedURLException {
		
		String respuesta = "";
		
		URL propertiesUrl = null;
        try {
            propertiesUrl = new URL(FILE, "", configFile);
        }
        catch (MalformedURLException e2) {
            LOGGER.error(e2,e2);
            
        }
		
		RgyRegistry registry = null;
        try {
            registry = LdapRgyRegistryFactory.getRgyRegistryInstance(
            		propertiesUrl, null);
        }
        catch (RgyException e2) {
        	LOGGER.info(FAILED);
        	LOGGER.error(e2,e2);
        	
        }
        LOGGER.info("$$$$$$$$$ Ruta de donde se toma el jar: "+RgyUser.class.getResource("RgyUser.class"));
		RgyUser user = null;
		try {
			user = registry.getUser(null, userName);
		} catch (RgyException e) {
			LOGGER.info("FAILED: Al obtener el usuario.");
			LOGGER.error(e,e);
		}
		
		LOGGER.info("User self change password.");
        try {
            user.changePassword(antPassword.toCharArray(), newPassword
                .toCharArray());
            
            respuesta = "true";
        }
        catch (RgyException e) {
        	LOGGER.info("FAILED: changePassword failed. ------------> "+ e.getLocalizedMessage());
            respuesta = e.getLocalizedMessage().substring(0, 10);
        }
        
		return respuesta;
	}
	
	 /**
     * @param usuario : codigo de cliente
     * @param aplicacion : identificador de aplicacion
     */
    public void bloqueoToken (String usuario, String aplicacion){
    	
    	//Bloqueo del token
		
		String [] respuestaCanales = canalesService.bloquearTokenUsuario(usuario, aplicacion);
		if(respuestaCanales != null && respuestaCanales.length > 0){
			for(String msj : respuestaCanales){
				LOGGER.info("Respuesta del web services, bloqueo token: " + msj);
			}
		}
    }

	/**
	 * @return canalesService ConsumoCanalesServiceEJB
	 */
	public ConsumoCanalesServiceEJB getCanalesService() {
		return canalesService;
	}

	/**
	 * @param canalesService : ConsumoCanalesServiceEJB
	 */
	public void setCanalesService(ConsumoCanalesServiceEJB canalesService) {
		this.canalesService = canalesService;
	}
	
	/**
	 * EjecutarRSA
	 * @return ejecutaRsa
	 */
	public EjecutarRSA getEjecutaRsa() {
		return ejecutaRsa;
	}

	/**
	 * EjecutarRSA
	 * @param ejecutaRsa : ejecutaRsa
	 */
	public void setEjecutaRsa(EjecutarRSA ejecutaRsa) {
		this.ejecutaRsa = ejecutaRsa;
	}
    
    
}
