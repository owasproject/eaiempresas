/**
 * Isban Mexico
 *   Clase: GenericCicsIsbanDA.java
 *   Descripción: Componente generico para consumir transacciones 390.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.infra;

import org.apache.commons.lang.StringUtils;

import com.isban.dataaccess.channels.cics.dto.RequestMessageCicsDTO;
import com.isban.dataaccess.channels.cics.dto.ResponseMessageCicsDTO;
import com.isban.eai.enlace.dto.ConsultaDTO;
import com.isban.eai.enlace.util.EnlaceConfig;
import com.isban.eai.enlace.util.Utils;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * Componente generico para consumir transacciones 390.
 */
public abstract class GenericCicsIsbanDA extends GenericIsbanDA {

    /**
     * Crea una instancia del acceso a 390 para el canal indicado.
     * @param canal : canal
     */
    public GenericCicsIsbanDA(IsbanDataAccessCanal canal) {
        super(canal);
    }
    
    /**
     * Ejecuta una transaccion de 390.
     * @param mensaje mensaje de la operacion.
     * @param transaccion la transaccion. 
     * @return respuesta de la transaccion. 
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
    protected ResponseMessageCicsDTO ejecutar(
            String mensaje, String transaccion)
            throws BusinessException {
        RequestMessageCicsDTO request = null;

        request = new RequestMessageCicsDTO();
        request.setCodeOperation(transaccion);
        request.setMessage(mensaje);
        request.setTransaction(transaccion);
        request.setUser(getUsuario390());
        request.setTimeout(EnlaceConfig.CICS_TIME_OUT);

        return (ResponseMessageCicsDTO) ejecutar(request);
    }

    /**
     * Obtiene el usuario con el que se conectara a 390.
     * @return EnlaceConfig enlace
     */
    protected static final String getUsuario390() {
        return EnlaceConfig.CICS_USUARIO;
    }
    
    /**
     * Obtiene el usuario con el que se conectara a 390.
     * @param servicio : servicio
     * @param longitud : longitud
     * @return cabeza en String
     */
    protected static final String getCabecera(String servicio,int longitud) {
    	StringBuffer cabeza =new StringBuffer();
		cabeza
            .append("    ")
            .append(getUsuario390())
            .append(servicio.trim())
            .append(StringUtils.leftPad(longitud+32+"",4))
            //.append("1")                 // 1  1=Altamira gestina el commit, 0=Altamira no gestiona el commit
            .append("100001")             // 5  Numero de secuencia
    		//.append("1")                 // 1  1=incorporar datos 2=autorizacion 4=reanudar conversacion 5=continua conversacion 6=autorizacion de transaccion en conversacion
			.append("1O")                 // 1  O=on line F=off line
			.append("00")         		 // 2  00=enter 01..12=Fnn 13..=ShiftF1..F24 99=Clear
			//.append("N")                 // 1  N=no impresora S=si impresora
			.append("N2");                // 1  2=formato @DC 1=mapas @PA y@LI

		return cabeza.toString();
    }

    
    /**
     * indPags
     * @param entrada : entrada
     * @return trama en String
     * @throws BusinessException manejo de excepcion
     */
    protected static final String indPags(ConsultaDTO entrada) throws BusinessException{
    	StringBuilder trama= new StringBuilder();
		
		trama.append(StringUtils.rightPad(Utils.defaultStringIfBlank(entrada.getIndPagina()),1))
			.append(rellenar(Utils.defaultStringIfBlank(entrada.getNumRegsPag()),3,'0','I'));
		
		return trama.toString();
    }
    
    /**
     * fchProcTranEst
     * @param entrada : entrada
     * @return trama en String
     * @throws BusinessException manejo de excepcion
     */
    protected static final String fchProcTranEst(ConsultaDTO entrada) throws BusinessException{
    	StringBuilder trama= new StringBuilder();
		
    	trama.append(StringUtils.rightPad(Utils.defaultStringIfBlank(entrada.getFchProcPaginado()),10))
			.append(StringUtils.rightPad(Utils.defaultStringIfBlank(entrada.getFchTransPaginado()),10))
			.append(StringUtils.rightPad(Utils.defaultStringIfBlank(entrada.getEstatusOper()),1));
		
		return trama.toString();
    }
	
    /**
     * rellenar
     * @param cad : cad
     * @param lon : lon
     * @param caractRellen : caractRellen
     * @param tipo : tipo
     * @return Aux cadena de rellenar
     */
    public static String rellenar(String cad,int lon, char caractRellen, char tipo)
	{
	    if( cad.length()>lon ){
	    	return cad.substring(0,lon);
	    }
	    if( tipo!='I' && tipo!='D' ){
	    	tipo = 'I';
	    }
	    String aux = "";
	    if( tipo=='D' ){
	    	aux = cad;
	    }
	    for (int i=0; i<(lon-cad.length()); i++){
		    if( tipo=='I' ){
		    	aux = aux.concat("");
		    	aux = aux.concat(cad);
		    }
	    }
	    return aux;
	}

    
    
    /**
     * obtenerTotalRegistros
     * @param buffer : buffer
     * @return obtenerTotalRegistros cadena
     */
    public static final String obtenerTotalRegistros(String buffer){
    	if(buffer == null || "".equals(buffer)){
    		return null;
    	}
    		
    	
    	String[] bufferSeparado = buffer.split("@");
    	if(bufferSeparado.length >= 3 && bufferSeparado[2].length() > 41){
    		return bufferSeparado[2].substring(33, bufferSeparado[2].length()-1);
    	}
    	return null;
    }
    
    /**
     * obtenerCodigoDeErrorDeBuffer
     * @param buffer : buffer
     * @return obtenerCodigoDeErrorDeBuffer cadena
     */
    public static final String obtenerCodigoDeErrorDeBuffer(String buffer){
    	if(buffer == null || "".equals(buffer)){
    		return null;
    	}
    	String[] bufferSeparado = buffer.split("@");
    	if(bufferSeparado.length >= 3 && bufferSeparado[2].length() > 9){
    		return bufferSeparado[2].substring(2, 9);
    	}
    	return null;
    }
    
    /**
     * obtenerMensajeDeErrorDeBuffer
     * @param buffer : buffer
     * @return obtenerMensajeDeErrorDeBuffer cadena 
     */
    public static final String obtenerMensajeDeErrorDeBuffer(String buffer){
    	if(buffer == null || "".equals(buffer)){
    		return null;
    	}
    	String[] bufferSeparado = buffer.split("@");
    	if(bufferSeparado.length >= 3 && bufferSeparado[2].length() > 10){
    		return bufferSeparado[2].substring(10, bufferSeparado[2].length() - 2);
    	}
    	return null;
    }
    
    /**
     * Genera la trama de entrada de la PE68 detalle por operacion
     * @param numPersona : numPersona
     * @return trama en cadena
     * @throws BusinessException manejo de la excepcion
     */
    protected static final String generaTramaPE68(String numPersona)throws BusinessException {
    	StringBuffer trama= new StringBuffer();
    	//trama.append(StringUtils.leftPad(Utils.defaultStringIfBlank(entrada.getCodigoRegion()),4));
		trama.append(StringUtils.leftPad(Utils.defaultStringIfBlank(numPersona),8));
		trama.append(StringUtils.leftPad(Utils.defaultStringIfBlank(""),905));
		return trama.toString();
    }
    
    /**
     * @param numPersona : numPersona
     * @return trama en cadena
     * @throws BusinessException manejo de la excepcion
     */
    protected static final String generaTramaODB6(String numPersona)throws BusinessException {
    	StringBuffer trama= new StringBuffer();
    	//trama.append(StringUtils.leftPad(Utils.defaultStringIfBlank(entrada.getCodigoRegion()),4));
    	trama.append("C");
		trama.append(StringUtils.leftPad(Utils.defaultStringIfBlank(numPersona),8));
		trama.append(StringUtils.leftPad(Utils.defaultStringIfBlank("001"),3));
		trama.append(StringUtils.leftPad(Utils.defaultStringIfBlank(""),50));
		trama.append(StringUtils.leftPad(Utils.defaultStringIfBlank(""),26));
		return trama.toString();
    }


}