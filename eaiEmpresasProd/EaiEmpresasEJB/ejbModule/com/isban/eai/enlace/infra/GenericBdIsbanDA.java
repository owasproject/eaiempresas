/**
 * Isban Mexico
 *   Clase: GenericBdIsbanDA.java
 *   Descripción: Componente generico para el Acceso a Datos desde BD con
 *   IsbanDataAccess.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.infra;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.isban.dataaccess.channels.database.dto.RequestMessageDataBaseDTO;
import com.isban.dataaccess.channels.database.dto.ResponseMessageDataBaseDTO;
import com.isban.dataaccess.factories.jdbc.ConfigFactoryJDBC;
import com.isban.eai.enlace.comun.Mensaje;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * Componente generico para el Acceso a Datos desde BD con IsbanDataAccess.
 */
public abstract class GenericBdIsbanDA extends GenericIsbanDA {

    /**
     * Encabezado para el insert multiple.
     **/
    protected static final String INSERT_MULTIPLE_ENCABEZADO =
        "INSERT ALL %n%sSELECT * FROM dual";

    /**
     * El formato usado para obtener el total de resultados de una consulta.
     **/
    protected static final String COUNT_QUERY_FMT = 
            "SELECT COUNT(1) AS TOTAL FROM(%s)";

    /**
     * El formato usado para obtener los resultados paginados de una coonsulta.
     **/
    protected static final String PAGINATED_QUERY_FMT = new StringBuilder(
            "select * from (%n").append(
            "   select x.*, rownum row_num%n").append(
            "   from (%s) x%n").append(
            "   where rownum <= %d%n").append(
            ") where row_num >= %d").toString();

    /**
     * El objeto de escritura en LOG para la clase.
     **/
    private static final Logger LOGGER = Logger.getLogger(
            GenericBdIsbanDA.class);

    /**
     * Constructor del componente que indica el canal al que se
     * @param canal : canal
     */
    protected GenericBdIsbanDA(IsbanDataAccessCanal canal) {
        super(canal);
    }
    
    /**
     * Ejecuta la operacion de Base de Datos (INSERT, UPDATE, DELETE, SELECT)
     * indicada.
     * @param query la operacion que se ha de ejecutar.
     * @param tipoOperacion el tipo de la operacion que se ha de ejecutar, los
     *  tipos de operacion se encuentran definidos en
     * @return la respuesta de la Base de datos.
     * @throws BusinessException si ocurre algun error al ejecutar la operacion.
     */
    protected ResponseMessageDataBaseDTO ejecutar(String query,
            int tipoOperacion)
            throws BusinessException {
        RequestMessageDataBaseDTO request = null;

        if (StringUtils.isBlank(query)) {
            throw new BusinessException("El query es nulo");
        }
        request = new RequestMessageDataBaseDTO();
        request.setQuery(query);
        request.setTypeOperation(tipoOperacion);

        return (ResponseMessageDataBaseDTO) ejecutar(request);
    }
    
    /**
     * Ejecuta la consulta indicada y regresa la respuesta del IsbanDataAccess.
     * @param query el query con la consulta.
     * @return el componente de respuesta del IsbanDataAccess.
     * @throws BusinessException si el query con la consulta es nulo
     *  o vacio.
     */
    protected ResponseMessageDataBaseDTO consultarBase(String query)
            throws BusinessException {
        ResponseMessageDataBaseDTO response = ejecutar(query,
                ConfigFactoryJDBC.OPERATION_TYPE_QUERY);

        if (response == null) {
            LOGGER.error("No se obtuvo respuesta de la base de datos");
            return null;
        }
        return response;
    }

    /**
     * Realiza la consulta indicada y regresa la informacion de la base de datos
     * sin que se realice el mapeo.
     * @param query la consulta.
     * @return los resultados de la consulta, donde el {@code key} del mapa
     * @throws BusinessException manejo de la excepcion
     */
    protected List<HashMap<String, Object>> consultarSinMapeo(String query)
            throws BusinessException {
        ResponseMessageDataBaseDTO response = consultarBase(query);

        if ((response == null)
                || (response.getResultQuery() == null)
                || response.getResultQuery().isEmpty()) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Consulta sin resultados");
            }
            return Collections.emptyList();
        }
        return response.getResultQuery();
    }

    /**
     * @param <T> arreglo
     * @param query la consulta.
     * @param mapeo el componente que realiza el mapeo de los resultados a
     *  instancias de DTO.
     * @return la lista de resultados obtenidos.
     * @throws BusinessException manejo de la excepcion
     */
    protected <T extends Serializable> List<T> consultar(String query,
            MapeoConsulta<T> mapeo) throws BusinessException {
        List<T> resultado = null;
        List<HashMap<String, Object>> response = null;

        if (mapeo == null) {
            throw new IllegalArgumentException(
                    "Indicar el componente de mapeo de resultados");
        }

        response = consultarSinMapeo(query);
        resultado = new ArrayList<T>(response.size());
        for (Map<String, Object> registro : response) {
            resultado.add(mapeo.mapeoRegistro(registro));
        }

        return resultado;
    }

    /**
     * Ejecuta un insert en la base de datos.
     * @param query el query con la sentencia de insert.
     * @throws BusinessException si ocurre algun error al insertar en la base
     *  de datos.
     **/
    protected void insert(String query) throws BusinessException {
        insertUpdateDelete(query, ConfigFactoryJDBC.OPERATION_TYPE_INSERT);
    }

    /**
     * Ejecuta un update en la base de datos.
     * @param query el query con la sentencia de update.
     * @throws BusinessException si ocurre algun error al actualizar en la base
     *  de datos.
     **/
    protected void update(String query) throws BusinessException {
        insertUpdateDelete(query, ConfigFactoryJDBC.OPERATION_TYPE_UPDATE);
    }

    /**
     * Ejecuta un delete en la base de datos.
     * @param query el query con la sentencia de delete.
     * @throws BusinessException si ocurre algun error al eliminar en la base
     *  de datos.
     **/
    protected void delete(String query) throws BusinessException {
        insertUpdateDelete(query, ConfigFactoryJDBC.OPERATION_TYPE_DELETE);
    }

    /**
     * Realiza una operacion de Insert, Update o Delete.
     * @param query query con la operacion.
     * @param tipoOperacion tipo de operacion.
     * @throws BusinessException si ocurre algun error.
     */
    private void insertUpdateDelete(String query, int tipoOperacion)
    throws BusinessException {
        ResponseMessageDataBaseDTO response = ejecutar(query, tipoOperacion);

        if (response == null) {
            LOGGER.error("No se obtuvo respuesta de la BD");
            throw new BusinessException(Mensaje.ERROR_COMUNICACION.
                    getCodigo());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(response.getResultToJSONString());
        }
    }
    
}
