package com.isban.eai.enlace.servicio;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.isban.eai.enlace.dao.DAOPE68;
import com.isban.eai.enlace.dto.trans390.PE68Response;
import com.isban.ebe.commons.exception.BusinessException;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORPE68EJBImpl implements BORPE68EJB {
	
	/**
	 * daoPE68
	 */
	@EJB
	private DAOPE68 daoPE68;
	
	/**
	 * @param nomPersona : nomPersona
	 * @return nomPersona nombre de persona
	 * @throws BusinessException manejo de excepcion
	 */
	@Override
	public PE68Response consultaPersona(String nomPersona)
			throws BusinessException {
		
		return getDaoPE68().consultaPersona(nomPersona);
	}
	
	/**
	 * @return daoPE68 datos
	 */
	public DAOPE68 getDaoPE68() {
		return daoPE68;
	}
	
	/**
	 * @param daoPE68 : daoPE68
	 */
	public void setDaoPE68(DAOPE68 daoPE68) {
		this.daoPE68 = daoPE68;
	}
	
	
	

}
