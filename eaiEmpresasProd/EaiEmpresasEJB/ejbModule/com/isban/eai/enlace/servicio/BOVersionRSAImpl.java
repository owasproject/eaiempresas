/**
 * Isban Mexico
 *   Clase: BOLRegistraBitacoraAdministrativaImpl.java
 *   Descripción: Implementacion del componente de registro de movimientos
 *   administrativos.
 *
 *   Control de Cambios:
 *   1.0 Mar 23, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.servicio;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import com.isban.eai.enlace.dao.DAOVersionRSA;
import com.isban.eai.enlace.dto.VersionRSADTO;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * Implementacion del componente de registro de movimientos administrativos.
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BOVersionRSAImpl implements
		BOVersionRSA {

    /**
     * El componente de acceso a datos para la bitacora administrativa.
     **/
    @EJB
    private DAOVersionRSA daoVersion;

	/** {@inheritDoc} */
    public VersionRSADTO consulta()
            throws BusinessException {
        return consultar();
    }

    /**
     * Realiza el llamado del DAO de consulta.
     * @return valor consultado en BD VersionRSADTO
     * @throws BusinessException manejo de la excepcion
     */
    private VersionRSADTO consultar() 
    		throws BusinessException {
    	return getDaoVersion().consulta();
    }
    
    /**
     * Obtiene el componente de acceso a datos para la consulta de la version de RSA.
     * @return el componente de acceso a datos para la version de RSA.
     */
    public DAOVersionRSA getDaoVersion() {
		return daoVersion;
	}
    
    /**
     * Asigna el componente de acceso a datos para la consulta de la version de RSA.
     * @param daoVersion el componente de acceso a datos para la version de RSA.
     */
	public void setDaoVersion(DAOVersionRSA daoVersion) {
		this.daoVersion = daoVersion;
	}

}
