package com.isban.eai.enlace.servicio;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.isban.rsa.aa.ws.BindingType;
import mx.isban.rsa.aa.ws.ChallengeQuestion;
import mx.isban.rsa.aa.ws.ChallengeQuestionChallenge;
import mx.isban.rsa.aa.ws.DeviceData;
import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosAAResponse;
import mx.isban.rsa.bean.ServiciosSTUResponse;
import mx.isban.rsa.conector.servicios.ServiciosAA;
import mx.isban.rsa.conector.config.ConectorConfig;
import mx.isban.rsa.stu.ws.Image;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.dto.CorreoDTO;
import com.isban.eai.enlace.dto.ImagenDTO;
import com.isban.eai.enlace.dto.RSADTO;
import com.isban.eai.enlace.dto.trans390.ODB6Response;
import com.isban.eai.enlace.dto.trans390.PE68Response;
import com.isban.eai.enlace.service.sam.BORServicioSAMEJB;
import com.isban.eai.enlace.util.ConstantesRSA;
import com.isban.eai.enlace.util.EjecutarRSA;
import com.isban.eai.enlace.util.EnlaceConfig;
import com.isban.ebe.commons.exception.BusinessException;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class RSAServiceEJBImpl implements RSAServiceEJB{
	
	/**
	 * LOGGER
	 */
	private static final Logger LOGGER = Logger.getLogger(RSAServiceEJBImpl.class);
    
    /**codigo de exito**/
    private static final String EXI000 = "EXI000"; 
    
    /**cadena 001**/
    private static final String CADENA001 = "001";
	
    /**deviceTokenCookie**/
    private static final String DEVICETOKENCOOKIE = "deviceTokenCookie";
    
    /**deviceTokenFSO**/
    private static final String DEVICETOKENFSO = "deviceTokenFSO";
    
    /**accion**/
    private static final String ACCION = "accion";
    
    /**msj**/
    private static final String MSJ = "msj";
    
    /**aplicacion**/
    private String aplicacion;
	
    /**
	 * EjecutarRSA
	 */
    private EjecutarRSA ejecutaRsa;
    
    //Prueba version 7
    
    
    /**
     * boODB6
     */
    @EJB
    private BORODB6EJB boODB6;
    
    /**
     * boPE68
     */
    @EJB
    private BORPE68EJB boPE68;
    
    /**
     * conCanales
     */
    @EJB
    private ConsumoCanalesServiceEJB conCanales;
    
    /**
     * consumoSAM
     */
    @EJB
    private BORServicioSAMEJB consumoSAM;
    
    /**
     * notiService
     */
    @EJB
    private NotificacionServiceEJB notiService;
    
    /**
     * bitacoraTamSam
     */
    @EJB
    private BOLRegistraBitacoraTamSam bitacoraTamSam;
    
    /**
     * Contructor de la clase 
     */
    public RSAServiceEJBImpl(){
    	final File file = new File("/proarchivapp/WebSphere8/was8/eai/enlace/RSA.cfg");
    	ConectorConfig.init(file);   
    }
    
	 /** {@inheritDoc} */
	public Map<String,Object> ejecutaAnalyze(RSADTO bean) {
		
		LOGGER.info("Ejecuta Analyze ");
		String estatusUsuario = "", actionCode = "", deviceTokenFSO = "", deviceTokenCookie = "";
	    String sessionID = "";
	    String transactionID = "";
	    ServiciosAAResponse resp = new ServiciosAAResponse(); 
	    ejecutaRsa = new EjecutarRSA();
	    resp = ejecutaRsa.ejecutaAnalyze(bean);
	     
	    if(resp != null && resp.getIdentificationData() != null 
	    		&& resp.getRiskResult() != null
	    		&& resp.getDeviceResult() != null){
    			
			estatusUsuario = resp.getIdentificationData().getUserStatus() != null 
    		? resp.getIdentificationData().getUserStatus().toString() : null;
    		
    	    actionCode = resp.getRiskResult().getTriggeredRule().getActionCode() != null
    	    ? resp.getRiskResult().getTriggeredRule().getActionCode().toString() : null;
    	    
    	    deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO() != null
    	    ? resp.getDeviceResult().getDeviceData().getDeviceTokenFSO() : null;
    	    
    	    deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie() != null
    	    ? resp.getDeviceResult().getDeviceData().getDeviceTokenCookie() : null;
	    	
	    	sessionID = resp.getIdentificationData().getSessionId() != null
	    	? resp.getIdentificationData().getSessionId() : null;
	    	
	    	transactionID = resp.getIdentificationData().getTransactionId() != null
	    	? resp.getIdentificationData().getTransactionId() : null;
	    	
	    	final Map<String,Object> datos = new HashMap<String,Object>();
	    	
	    	datos.put(DEVICETOKENFSO, deviceTokenFSO);
		    datos.put(DEVICETOKENCOOKIE, deviceTokenCookie);
		    
	    	LOGGER.info("Estatus usuario: " + estatusUsuario);
	    	LOGGER.info("Action code: " + actionCode);
	    	
	    	bean.getDeviceRequest().setDeviceTokenFSO(deviceTokenFSO);
	    	bean.getDeviceRequest().setDeviceTokenCookie(deviceTokenCookie);
	    	
	    	try {
				return evaluarRespuesta(estatusUsuario, 
						actionCode, bean.getUserName(), sessionID, transactionID, bean, datos);
			} catch (BusinessException e) {
				LOGGER.error(e.getMessage(), e);
			}
	    	
	    }
	    return null;
	}
	
	 /** {@inheritDoc} */
     public Map<String,Object> evaluarRespuesta(String estatusUsuario, 
            String actionCode, String usuario, String sessionID, String transactionID,
            RSADTO rsaBean, Map<String,Object> datos) throws BusinessException{
    	 
    	if(ConstantesRSA.NOTENROLLED.equals(estatusUsuario) || ConstantesRSA.UNVERIFIED.equals(estatusUsuario) || ConstantesRSA.DELETE.equals(estatusUsuario)) {
			
    		if(ConstantesRSA.NOTENROLLED.equals(estatusUsuario) || ConstantesRSA.DELETE.equals(estatusUsuario)){
    			datos.put("CreateUser", "true");
			}else{
				datos.put("CreateUser", "false");
			}
    		try{

	    		bitacoraTamSam.registrarOperacion(rsaBean.getIpOrigen(), rsaBean.getHostName(), 
	    				rsaBean.getUserName(), "1", rsaBean.getAplicacion().concat(CADENA001),
	    				rsaBean.getIdSesion(), estatusUsuario, rsaBean.getHostName());
	    	}catch(BusinessException e){
	    		LOGGER.error(e.getMessage(), e);
	    	}
    		datos.put(ACCION, 1);
			return datos;
			
		} else if((ConstantesRSA.VERIFIED.equals(estatusUsuario) && ConstantesRSA.ALLOW.equals(actionCode)) || ConstantesRSA.UNLOCKED.equals(estatusUsuario)) {
			
			datos.put(ACCION, 2);
			datos.put("UpdateUser", ConstantesRSA.UNLOCKED.equals(estatusUsuario) ? "true" : null);
			try{
				
	    		bitacoraTamSam.registrarOperacion(rsaBean.getIpOrigen(), rsaBean.getHostName(), 
	    				rsaBean.getUserName(), "1", rsaBean.getAplicacion().concat(CADENA001),
	    				rsaBean.getIdSesion(), estatusUsuario, rsaBean.getHostName());
	    	}catch(BusinessException e){
	    		LOGGER.error(e.getMessage(), e);
	    	}
			return mostrarPaginaImagen(rsaBean, datos);
			
		} else if(ConstantesRSA.VERIFIED.equals(estatusUsuario) && ConstantesRSA.CHALLENGE.equals(actionCode)) {
			
			datos.put(ACCION, 3);
			datos.put("sessionID", sessionID);
			datos.put("transactionID", transactionID);
			
			return direccionaPaginaChallenge(rsaBean, sessionID, transactionID, datos);
			
		}else if((ConstantesRSA.VERIFIED.equals(estatusUsuario) || ConstantesRSA.UNVERIFIED.equals(estatusUsuario)
				|| ConstantesRSA.LOCKOUT.equals(estatusUsuario)) && ConstantesRSA.DENY.equals(actionCode)) {
			
			datos.put(ACCION, 4);
			return eventoDeny(rsaBean, usuario, actionCode, datos);
			
		}else if((ConstantesRSA.VERIFIED.equals(estatusUsuario) && ConstantesRSA.REVIEW.equals(actionCode)) || ConstantesRSA.LOCKOUT.equals(estatusUsuario)) {
			
			datos.put(ACCION, 5);
			
			try{
				setAplicacion(rsaBean.getAplicacion());
				//envio de correo
				envioCorreo(usuario, actionCode);
				
	    	}catch(BusinessException e){
	    		LOGGER.error(e.getMessage(), e);
	    	}
	    	
	    	try{

	    		bitacoraTamSam.registrarOperacion(rsaBean.getIpOrigen(), rsaBean.getHostName(), 
	    				rsaBean.getUserName(), "0", rsaBean.getAplicacion().concat(CADENA001),
	    				rsaBean.getIdSesion(), estatusUsuario, rsaBean.getHostName());
	    	}catch(BusinessException e){
	    		LOGGER.error(e.getMessage(), e);
	    	}
	    	
			datos.put(MSJ, "ERR001");
			return datos;
			
		}
    	
    	return null;
	    
    }
	
     /** {@inheritDoc} */
    public Map<String, Object> mostrarPaginaImagen(RSADTO rsaBean, Map<String, Object> datos) {
           	
    	final ImagenDTO dto = new ImagenDTO();

    	ServiciosSTUResponse res = new ServiciosSTUResponse();
    	final EjecutarRSA ejecutaRsa = new EjecutarRSA();
		res = ejecutaRsa.ejecutaConsultaImagenUsr(rsaBean);
		if(res != null && res.getSiteToUserData() != null){
			final Image imagen = res.getSiteToUserData().getUserImage() != null 
			? res.getSiteToUserData().getUserImage() : null;
			
			String deviceTokenFSO = "";
			String deviceTokenCookie = "";
			
			if(imagen != null){
				
				dto.setImagen(imagen != null ? imagen.getData() : "");
				dto.setIdImagen(imagen != null ? imagen.getPath() : "");
				
				deviceTokenFSO = res.getDeviceResult().getDeviceData().getDeviceTokenFSO() != null
				? res.getDeviceResult().getDeviceData().getDeviceTokenFSO() : null;
			    deviceTokenCookie = res.getDeviceResult().getDeviceData().getDeviceTokenCookie() != null
			    ? res.getDeviceResult().getDeviceData().getDeviceTokenCookie() : null;
			    datos.put(DEVICETOKENFSO, deviceTokenFSO);
			    datos.put(DEVICETOKENCOOKIE, deviceTokenCookie);
				
			    datos.put("imagesVal", dto.getImagen());
			    datos.put("imagen", dto);
				LOGGER.info("Imagen   " + dto.getIdImagen());
			}else{	
				datos.put(ACCION, 6);
			}
		}else{	
			datos.put(ACCION, 6);
		}
		
		return datos;		
    }
    
    /** {@inheritDoc} */
    public Map<String, Object> direccionaPaginaChallenge(RSADTO rsaBean, String sesionId, String trasaccionId, Map<String, Object> datos) throws BusinessException{
    	
    	try{
    		LOGGER.info("Los valores: "+ rsaBean.getIpOrigen()+"  --  "+ rsaBean.getHostName()+"  -  "+ 
    				rsaBean.getUserName()+"  --  "+ "1"+"  -  "+ rsaBean.getAplicacion().concat(CADENA001)+"  --  "+
    				rsaBean.getIdSesion()+"  -  "+ trasaccionId+"  -----  "+ConstantesRSA.REVIEW +"  ---  "+ rsaBean.getHostName());
    		bitacoraTamSam.registrarOperacion(rsaBean.getIpOrigen(), rsaBean.getHostName(), 
    				rsaBean.getUserName(), "1", rsaBean.getAplicacion().concat(CADENA001),
    				rsaBean.getIdSesion(), ConstantesRSA.REVIEW, rsaBean.getHostName());
    	}catch(BusinessException e){
    		LOGGER.error(e.getMessage(), e);
    	}
    	
    	String deviceTokenFSO = "";
	    String deviceTokenCookie = "";
	    rsaBean.setAuthSessionId(sesionId);
	    rsaBean.setAuthTransactionId(trasaccionId);
	    final EjecutarRSA ejecutaRsa = new EjecutarRSA();
	    ServiciosAAResponse resp = new ServiciosAAResponse();
		resp = ejecutaRsa.ejecutaConsultaPreguntaReto(rsaBean);
		
		if(resp != null){
			ChallengeQuestionChallenge preguntaUsr = new ChallengeQuestionChallenge();
			preguntaUsr = resp.getChallengeQuestionChallenge();
			final ChallengeQuestion[] question = preguntaUsr.getPayload().getChallengeQuestions();
			final ChallengeQuestion questionUsr = question[0];
			LOGGER.info("Pregunta   " + questionUsr.getQuestionText());
			
		    deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO() != null
    	    ? resp.getDeviceResult().getDeviceData().getDeviceTokenFSO() : null;
    	    
    	    deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie() != null
    	    ? resp.getDeviceResult().getDeviceData().getDeviceTokenCookie() : null;
		    
			datos.put(DEVICETOKENFSO, deviceTokenFSO);
		    datos.put(DEVICETOKENCOOKIE, deviceTokenCookie);
		    
		    datos.put("sesionId", (sesionId != null ? sesionId : ""));
		    datos.put("trasaccionId", resp.getIdentificationData().getTransactionId().toString());
		    datos.put("pregunta", questionUsr);
			
			LOGGER.info("Valor de SessionId en BO: [" + resp.getIdentificationData().getSessionId().toString() + "]");
			LOGGER.info("Valor de TrasaccionId en BO: [" + trasaccionId + "]");
			datos.put("sessionId", resp.getIdentificationData().getSessionId().toString());
		    
		}else{
			datos.put(ACCION, 6);
		}
		
		return datos;
    }
    
    /** {@inheritDoc} */
    public Map<String, Object> eventoDeny(RSADTO rsaBean, String usuario, String actionCode, Map<String, Object> datos){
    			
    	conCanales.bloquearTokenUsuario(usuario, rsaBean.getAplicacion());
		
		 //bloqueo de usuario
		try {
			final boolean banderaBloq = consumoSAM.bloquearUsuario(usuario);
			LOGGER.info("Bandera de bloqueo de usuario: "+banderaBloq);
		} catch (MalformedURLException e1) {
			LOGGER.error(e1.getMessage(), e1);
		} catch (BusinessException e) {
			LOGGER.error(e.getMessage(), e);
		}
					
		try{
			setAplicacion(rsaBean.getAplicacion());
			//envio de correo
			envioCorreo(usuario, actionCode);
			
    	}catch(BusinessException e){
    		LOGGER.error(e.getMessage(), e);
    	}
    	
    	try{

    		bitacoraTamSam.registrarOperacion(rsaBean.getIpOrigen(), rsaBean.getHostName(), 
    				rsaBean.getUserName(), "0", rsaBean.getAplicacion().concat(CADENA001),
    				rsaBean.getIdSesion(), ConstantesRSA.DENY, rsaBean.getHostName());
    	}catch(BusinessException e){
    		LOGGER.error(e.getMessage(), e);
    	}
    	
    	datos.put(MSJ, "ERR000");
		return datos;
    }
    
    /** {@inheritDoc} */
    public void envioCorreo(String usuario, String evento) throws BusinessException {
    	
    	LOGGER.info("Entro a enviando correo");
    	ODB6Response respODB6 = new ODB6Response();
		PE68Response respPE68 = new PE68Response ();
		
		final CorreoDTO correoDTO = new CorreoDTO();
		correoDTO.setAplicacion(getAplicacion());
		
		respODB6 = getBoODB6().consultaCorreo(usuario);
		respPE68 = getBoPE68().consultaPersona(usuario);
		
		String correo = "";
		String nombleCompleto = "";
		
		if(EXI000.equals(respODB6.getCodError()) && EXI000.equals(respPE68.getCodError())){
			correo = respODB6.getCorreo();
			nombleCompleto = respPE68.getNombre() + " " + respPE68.getPrimerApe() + " " + respPE68.getSegundoApe();
		}
		
		LOGGER.debug("valor: " + correo);
		
		correoDTO.setCodigoCliente(usuario);
		correoDTO.setNombreCliente(nombleCompleto);
		
		String tipoCorreoUsr = "";
		String tipoCorreoAnalista = "";
		
		if(ConstantesRSA.DENY.equals(evento)){
			tipoCorreoUsr = "3";
			tipoCorreoAnalista = "2";
		}else{ 	
			tipoCorreoUsr = "3";
			tipoCorreoAnalista = "4";
		}
		
		if(correo != null && !"".equals(correo)){
			//mensaje a usuario
			correoDTO.setTipoCorreo(tipoCorreoUsr);
			correoDTO.setTo(correo);
			Map<String, String> respuestaNotiU = new HashMap<String, String>();
			respuestaNotiU = notiService.consumoCorreo(correoDTO);
			
			LOGGER.debug("valor usuario: " + respuestaNotiU);
		}
				
		//mensaje a analista
		correoDTO.setTipoCorreo(tipoCorreoAnalista);
		correoDTO.setTo(EnlaceConfig.CORREO_ANALISTA_RIESGOS);
		Map<String, String> respuestaNotiA = new HashMap<String, String>();
		respuestaNotiA = notiService.consumoCorreo(correoDTO);
		
		LOGGER.debug("valor analista: " + respuestaNotiA);
		
    }
    
    /** {@inheritDoc} */
    public Map<String, Object> validarRespuestaCliente(RSADTO rsaBean, Map<String, String> entrada) throws BusinessException {
    	
    	final Map<String, Object> datos = new HashMap<String, Object>();
		String deviceTokenFSO = "";
		String deviceTokenCookie = "";
			
		ServiciosAAResponse resp = new ServiciosAAResponse();
		final EjecutarRSA ejecutaRsa = new EjecutarRSA();
		
		final String idPregunta = entrada.get("idPregunta");
		final String respuesta = entrada.get("respuesta");
		final String txtPregunta = entrada.get("txtPregunta");
		
		final ChallengeQuestion question = new ChallengeQuestion();
		question.setQuestionId(idPregunta);
		question.setUserAnswer(respuesta);
		question.setActualAnswer(respuesta);
		question.setQuestionText(txtPregunta);
		final ChallengeQuestion[] payload = {question};
		
		resp = ejecutaRsa.ejecutaRespuestaPreguntaReto(rsaBean, payload);
		
		if(resp != null){
			if(resp.getStatusHeader() != null && (1304 == resp.getStatusHeader().getReasonCode() || 1203 == resp.getStatusHeader().getReasonCode())){
				resp = ejecutaRsa.ejecutaRespuestaPreguntaReto(rsaBean, payload);
				LOGGER.info("+++++Se ejecuta de nuevo.");
			}
			if(resp.getDeviceResult().getDeviceData() != null){
				deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
			    deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie();
			    
			    datos.put(DEVICETOKENFSO, deviceTokenFSO);
			    datos.put(DEVICETOKENCOOKIE, deviceTokenCookie);
			    rsaBean.getDeviceRequest().setDeviceTokenFSO(deviceTokenFSO);
			    rsaBean.getDeviceRequest().setDeviceTokenCookie(deviceTokenCookie);
				final String estatus = resp.getCredentialAuthResultList().getChallengeQuestionAuthResult().getPayload().
					getAuthenticationResult().getAuthStatusCode();
				final String estatusUsr = resp.getIdentificationData().getUserStatus().toString();
				
				final String idSesion = resp.getIdentificationData().getSessionId();
				final String idTransaccion = resp.getIdentificationData().getTransactionId();
				final String tipoDisp = entrada.get("tipoDisp");
				final String usuario = entrada.get("userName");
			 	
			 		return evaluarRespuestaChallenge (estatus, estatusUsr, 
			 	            idPregunta, txtPregunta, idSesion, idTransaccion,
			 	            tipoDisp, rsaBean, datos, usuario);
			}
		 }
		 
		
		LOGGER.info(">>> El web service de RSA no esta activo");
		datos.put(ACCION, 6);
		return datos;
	
     }
    
    /** {@inheritDoc} */
    public Map<String, Object> evaluarRespuestaChallenge (String estatus, String estatusUsr, 
            String idPregunta, String txtPregunta, String idSesion, String idTransaccion,
            String tipoDisp, RSADTO rsaBean, Map<String, Object> datos, String usuario){
    	
    	if("FAIL".equals(estatus)) {
			if(!"LOCKOUT".equals(estatusUsr)){
				final ChallengeQuestion questionUsr = new ChallengeQuestion();
				questionUsr.setQuestionId(idPregunta);
				questionUsr.setQuestionText(txtPregunta);
				LOGGER.info("Pregunta   " + questionUsr.getQuestionText());
				
				datos.put("pregunta", questionUsr);
				datos.put("Sesion", idSesion);
				LOGGER.info("Transaccion   " + idTransaccion);
				datos.put("Transaccion", idTransaccion);
				datos.put("tipoDisp", tipoDisp);
				datos.put(MSJ, "ERR000");
				datos.put(ACCION, 1);
				return datos;
			}
			if("LOCKOUT".equals(estatusUsr)){
				
				LOGGER.debug("valor: " + usuario);
				//envio de correo
				try{
					//envio de correo
					envioCorreo(usuario, "REVIEW");
					
		    	}catch(BusinessException e){
		    		LOGGER.error(e.getMessage(), e);
		    	}
				datos.put(MSJ, "ERR001");
				datos.put(ACCION, 2);
				
				try{

		    		bitacoraTamSam.registrarOperacion(rsaBean.getIpOrigen(), rsaBean.getHostName(), 
		    				rsaBean.getUserName(), "0", rsaBean.getAplicacion().concat(CADENA001),
		    				rsaBean.getIdSesion(), ConstantesRSA.REVIEW, rsaBean.getHostName());
		    	}catch(BusinessException e){
		    		LOGGER.error(e.getMessage(), e);
		    	}
				
				return datos;				
			}
			
		}
    	
    	if("SUCCESS".equals(estatus)) {
			
    		final ServiciosAA siteToUser = new ServiciosAA();
			
			rsaBean.setAuthSessionId(idSesion);
			rsaBean.setAuthTransactionId(idTransaccion);
	    	ServiciosAAResponse resp = new ServiciosAAResponse();
	    	final EjecutarRSA ejecutaRsa = new EjecutarRSA();
	    	final DeviceData deviceData = new DeviceData();
			if("privado".equals(tipoDisp)){
				deviceData.setBindingType(BindingType.HARD_BIND);
			}
			if("publico".equals(tipoDisp)){
				deviceData.setBindingType(BindingType.NONE);
			}
			final RSABean bean = ejecutaRsa.convertirDTO(rsaBean);
			resp = siteToUser.updateDevice(bean, deviceData, rsaBean.getVersionRSA());
			
			final String deviceTokenFSO = resp.getDeviceResult().getDeviceData().getDeviceTokenFSO();
			final String deviceTokenCookie = resp.getDeviceResult().getDeviceData().getDeviceTokenCookie();
		    
			datos.put(DEVICETOKENFSO, deviceTokenFSO);
		    datos.put(DEVICETOKENCOOKIE, deviceTokenCookie);
		    rsaBean.getDeviceRequest().setDeviceTokenFSO(deviceTokenFSO);
	    	rsaBean.getDeviceRequest().setDeviceTokenCookie(deviceTokenCookie);
		    datos.put("challenge","false");
		    datos.put(ACCION, 3);
		    
		    try{

	    		bitacoraTamSam.registrarOperacion(rsaBean.getIpOrigen(), rsaBean.getHostName(), 
	    				rsaBean.getUserName(), "1", rsaBean.getAplicacion().concat("003"),
	    				rsaBean.getIdSesion(), ConstantesRSA.CHALLENGE, rsaBean.getHostName());
	    	}catch(BusinessException e){
	    		LOGGER.error(e.getMessage(), e);
	    	}
	    	
		    return mostrarPaginaImagen(rsaBean, datos);
			
		}
		return null;
    	
    }
    
    /** {@inheritDoc} */
    public Map<String, Object> crearUsuario(RSADTO rsaBean) throws BusinessException {
    	
    	final Map<String, Object> datos = new HashMap<String, Object>();
			
		ServiciosAAResponse resp = new ServiciosAAResponse();
				
		resp = ejecutaRsa.ejecutaCreateUser(rsaBean);
		
		if(resp != null){
					    
			final String estatusUsuario = resp.getIdentificationData().getUserStatus() != null 
    		? resp.getIdentificationData().getUserStatus().toString() : null;
		 	
    		LOGGER.info("Creación de usuario: "+estatusUsuario);
    		datos.put("estatusUsuario", estatusUsuario);
    		
		}
		
		return datos;	
    }
    
    /** {@inheritDoc} */
    public Map<String, Object> updateUsuario(RSADTO rsaBean) throws BusinessException {
    	
    	final Map<String, Object> datos = new HashMap<String, Object>();
			
		ServiciosAAResponse resp = new ServiciosAAResponse();
				
		resp = ejecutaRsa.ejecutaUpdateUser(rsaBean);
		
		if(resp != null){
					    
			final String estatusUsuario = resp.getIdentificationData().getUserStatus() != null 
    		? resp.getIdentificationData().getUserStatus().toString() : null;
		 	
    		LOGGER.info("Actualización de usuario: "+estatusUsuario);
    		datos.put("estatusUsuario", estatusUsuario);
    		
		}
		
		return datos;	
     }
    
    /**
	 * getBoODB6
	 * @return boODB6 origen
	 */
	public BORODB6EJB getBoODB6() {
		return boODB6;
	}

	/**
	 * setBoODB6
	 * @param boODB6 origen
	 */
	public void setBoODB6(BORODB6EJB boODB6) {
		this.boODB6 = boODB6;
	}

	/**
	 * getBoPE68 
	 * @return boPE68 origen
	 */
	public BORPE68EJB getBoPE68() {
		return boPE68;
	}

	/**
	 * setBoPE68 
	 * @param boPE68 origen
	 */
	public void setBoPE68(BORPE68EJB boPE68) {
		this.boPE68 = boPE68;
	}
	
	/**
	 * conCanales
	 * @return conCanales origen
	 */
	public ConsumoCanalesServiceEJB getConCanales() {
		return conCanales;
	}

	/**
	 * conCanales
	 * @param conCanales origen
	 */
	public void setConCanales(ConsumoCanalesServiceEJB conCanales) {
		this.conCanales = conCanales;
	}

	/**
	 * consumoSAM
	 * @return consumoSAM origen
	 */
	public BORServicioSAMEJB getConsumoSAM() {
		return consumoSAM;
	}

	/**
	 * consumoSAM
	 * @param consumoSAM origen
	 */
	public void setConsumoSAM(BORServicioSAMEJB consumoSAM) {
		this.consumoSAM = consumoSAM;
	}

	/**
	 * notiService
	 * @return notiService origen
	 */
	public NotificacionServiceEJB getNotiService() {
		return notiService;
	}

	/**
	 * notiService
	 * @param notiService origen
	 */
	public void setNotiService(NotificacionServiceEJB notiService) {
		this.notiService = notiService;
	}

	/**
	 * ejecutaRsa
	 * @return ejecutaRsa 
	 */
	public EjecutarRSA getEjecutaRsa() {
		return ejecutaRsa;
	}

	/**
	 * ejecutaRsa
	 * @param ejecutaRsa : ejecutaRsa
	 */
	public void setEjecutaRsa(EjecutarRSA ejecutaRsa) {
		this.ejecutaRsa = ejecutaRsa;
	}

	/**
	 * @return aplicacion con a la que se ingresara
	 */
	public String getAplicacion() {
		return aplicacion;
	}

	/**
	 * @param aplicacion : aplicacion con a la que se ingresara
	 */
	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}
	
	/**
	 * @return bitacoraTamSam objeto para generar bitacora
	 */
	public BOLRegistraBitacoraTamSam getBitacoraTamSam() {
		return bitacoraTamSam;
	}
	
	/**
	 * @param bitacoraTamSam : bitacoraTamSam objeto para generar bitacora
	 */
	public void setBitacoraTamSam(BOLRegistraBitacoraTamSam bitacoraTamSam) {
		this.bitacoraTamSam = bitacoraTamSam;
	}
	
}
