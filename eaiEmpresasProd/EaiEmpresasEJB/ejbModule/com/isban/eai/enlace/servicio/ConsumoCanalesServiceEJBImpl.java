package com.isban.eai.enlace.servicio;

import java.rmi.RemoteException;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.client.ws.CanalesWS;
import com.isban.eai.enlace.client.ws.CanalesWSProxy;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ConsumoCanalesServiceEJBImpl implements ConsumoCanalesServiceEJB{
	
	/**
	 * LOGGER : El objeto de escritura en log.
	 **/
	public final static Logger LOGGER = Logger.getLogger(ConsumoCanalesServiceEJBImpl.class);
	/**proxy**/
	private transient CanalesWS prox;
	
	/**
	 * bloquea token del usuario
	 * @param usuario : usuario
	 * @param aplicacion : aplicacion
	 * @return Lista con la respuesta
	 */
	public String[] bloquearTokenUsuario(String usuario, String aplicacion) {
		prox = new CanalesWSProxy();
		String[] resultado = null;
		String valor = "E".equals(aplicacion) ? aplicacion : "D";
		LOGGER.info("Bloqueo de token en aplicacion: "+valor);
		try {
			resultado = prox.bloquearToken(usuario, valor, "ES", "6");
			for(String res : resultado){
				LOGGER.info("Bloqueo de token: "+res);
			}
		} catch (RemoteException e) {
			LOGGER.info(e.getMessage());
		}
		
		return resultado;
	}
	
	/**
	 * Optiene el estatus del usuario y valida si es valido o invalido
	 * de acuerdo a la validacion regresada por el ws 
	 * donde 0|Estatus|Motivo de bloqueo y 
	 * Estatus: 8 Bloqueado
	 * Motivo de Bloqueo: 5 Intentos fallidos
	 * @param codCliente : codigo de cliente
	 * @return String : V=valido � I=invalido
	 */
	public String getEstatus(String codCliente) {
	
			prox = new CanalesWSProxy();
			String[] resp = null;
  			String resultado0 = "";
  			String resultado1 = "";
  			String resultado2 = "";
  			String respuesta = "";
			try {
				resp = prox.estatus(codCliente, 
						"",
						"");
				
				for(String r : resp) {
					LOGGER.info("------->valor de ws wn:" + r);
				
				}
				resultado0 = resp[0] != null ? resp[0] : "";
  				resultado1 = resp[1] != null ? resp[1] : "";
  				resultado2 = resp[2] != null ? resp[2] : "";
  				
	  			if("0".equals(resultado0) && "8".equals(resultado1)  && "5".equals(resultado2)) {
	  				respuesta = "I";
	  			} else {
	  				respuesta = "V";
	  			}
	  		
	  			LOGGER.info("------->estatus token:" + respuesta);
			
			} catch (RemoteException e) {
				LOGGER.info(e.getMessage());
			}
			
			return respuesta;
		
			
	}


}
