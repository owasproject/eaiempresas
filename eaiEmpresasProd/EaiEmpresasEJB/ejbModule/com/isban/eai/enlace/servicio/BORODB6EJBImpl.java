/**
 * Isban Mexico
 *   Clase: BORODB6EJBImpl.java
 *   Descripción: implementacion de BORODB6EJB
 *
 *   Control de Cambios:
 *   1.0 Diciembre 10, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.servicio;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.dao.DAOODB6;
import com.isban.eai.enlace.dto.trans390.ODB6Response;
import com.isban.ebe.commons.exception.BusinessException;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class BORODB6EJBImpl implements BORODB6EJB {
	
	/**LOG**/
	private static final Logger LOG = Logger.getLogger(BORODB6EJBImpl.class);
	
	/**
	 * DAOODB6
	 */
	@EJB
	private DAOODB6 daoODB6;
	
	/** {@inheritDoc} */
	public ODB6Response consultaCorreo(String nomPersona) throws BusinessException {
		return getDaoODB6().consultaCorreo(nomPersona);
	}
	
	/**
	 * @return DAOODB6 : objeto dao
	 */
	public DAOODB6 getDaoODB6() {
		return daoODB6;
	}
	
	/**
	 * @param daoODB6 : objeto dao
	 */
	public void setDaoODB6(DAOODB6 daoODB6) {
		this.daoODB6 = daoODB6;
	}

	
	

}
