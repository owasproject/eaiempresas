package com.isban.eai.enlace.util;

public class ConstantesRSA {
	
	/**Constante verified**/
    public static final String VERIFIED = "VERIFIED";
    
    /**Constante unverified**/
    public static final String UNVERIFIED = "UNVERIFIED";

    /**Constante allow**/
    public static final String ALLOW = "ALLOW";
    
    /**Constante challenge**/
    public static final String CHALLENGE = "CHALLENGE";
    
    /**Constante review**/
    public static final String REVIEW = "REVIEW";
    
    /**Constante lockout**/
    public static final String LOCKOUT = "LOCKOUT";
    
    /**Constante deny**/
    public static final String DENY = "DENY";
    
    /**Constante notenrolled**/
    public static final String NOTENROLLED = "NOTENROLLED";
    
    /**Constante unlocked**/
    public static final String UNLOCKED = "UNLOCKED";
    
    /**Constante delete**/
    public static final String DELETE = "DELETE";

}
