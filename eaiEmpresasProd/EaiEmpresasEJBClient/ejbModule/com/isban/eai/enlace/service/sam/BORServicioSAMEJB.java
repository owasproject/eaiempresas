package com.isban.eai.enlace.service.sam;

import java.net.MalformedURLException;
import java.util.Map;

import javax.ejb.Remote;

import com.isban.eai.enlace.dto.RSADTO;
import com.isban.eai.enlace.dto.RequestAuthSAMDTO;
import com.isban.ebe.commons.exception.BusinessException;

@Remote
public interface BORServicioSAMEJB {
	
	/**
	 * @param request : request
	 * @param accion : accion
	 * @param bean : bean
	 * @return validarPassword password
	 * @throws BusinessException manejo de la excepcion
	 */
	public Map<String,Object> validarPassword(RequestAuthSAMDTO request, int accion, 
			RSADTO bean) throws BusinessException;
	
	/**
	 * @param antPassword : antPassword
	 * @param newPassword : newPassword
	 * @param userName : userName
	 * @return cambiaPassword cambio
	 * @throws BusinessException manejo de la excepcion
	 * @throws MalformedURLException manejo de la excepcion
	 */
	public String cambiaPassword(String antPassword, String newPassword, String userName) 
	throws BusinessException, MalformedURLException;
	
	/**
	 * @param userName : userName
	 * @return bloquearUsuario bloqueo
	 * @throws BusinessException manejo de la excepcion
	 * @throws MalformedURLException manejo de la excepcion
	 */
	public boolean bloquearUsuario(String userName) throws BusinessException, MalformedURLException;
	
	/**
	 * @param usr : usr
	 * @param pwd : pwd
	 * @return int autenticacion
	 */
	public int autenticarUsr(String usr, String pwd);
	
	/**
     * @param usuario : codigo de cliente
     * @param aplicacion : identificador de aplicacion
     */
	public void bloqueoToken (String usuario, String aplicacion);
	
	/**
	 * @param newPassword : newPassword
	 * @param userName : userName
	 * @return validaPassword validacion
	 * @throws BusinessException manejo de la excepcion
	 * @throws MalformedURLException manejo de la excepcion
	 */
	boolean validaPassword(String newPassword, String userName)
			throws BusinessException, MalformedURLException;
	
	/**
	 * @param newPassword : newPassword
	 * @param userName : userName
	 * @return cambiaPassword cambio
	 * @throws BusinessException manejo de la excepcion
	 * @throws MalformedURLException manejo de la excepcion
	 */
	boolean cambiaPassword(String newPassword, String userName)
			throws BusinessException, MalformedURLException;
	
}