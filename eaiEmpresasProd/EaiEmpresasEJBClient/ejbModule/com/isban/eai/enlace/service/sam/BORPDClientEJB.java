/**
 * Isban Mexico
 *   Clase: BORODB6EJB.java
 *   Descripci�n: BO para la el consumo del servicio de SAM
 *
 *   Control de Cambios:
 *   1.0 Diciembre 10, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.service.sam;

import com.isban.eai.enlace.dto.RespuestaAuthDTO;

public interface BORPDClientEJB {

	
//	/**
//	 * Funci�n para evaluar el usuario y contrase�a.
//	 * @param usr : usuario
//	 * @param pwd : contrase�a
//	 * @return RespuestaAuthDTO 
//	 */
//	public RespuestaAuthDTO validaPwd(String usr, String pwd);
	
	/**
	 * copia de validaPwd para pruebas	
	 * @param usr : usr
	 * @param pwd : pwd
	 * @return validaPwdMock  respuesta
	 */
	public RespuestaAuthDTO validaPwdMock(String usr, String pwd);

	/**
	 * @param usr : usr
	 * @param pwd : pwd 
	 * @return validaPwd respuesta
	 */
	public RespuestaAuthDTO validaPwd(String usr, String pwd);
	
	
}
