package com.isban.eai.enlace.servicio;

import java.util.Map;

import javax.ejb.Remote;

import com.isban.eai.enlace.dto.RSADTO;
import com.isban.ebe.commons.exception.BusinessException;

@Remote
public interface RSAServiceEJB {
	
	
	/**
	 * @param bean : bean
	 * @return Map<String,Object>
	 */
	public Map<String,Object> ejecutaAnalyze(RSADTO bean);
	
	/**
     * @param estatusUsuario : estatus usuario
     * @param actionCode : action code
     * @param usuario : usuario
     * @param sessionID : sesion
     * @param transactionID : transaccion
     * @param rsaBean : rsaBean
     * @param datos : datos
     * @return ModelAndView
     * @throws BusinessException exception
     */
    public Map<String,Object> evaluarRespuesta(String estatusUsuario, 
            String actionCode, String usuario, String sessionID, String transactionID,
            RSADTO rsaBean, Map<String,Object> datos) throws BusinessException;
	
	/**
     * @param rsaBean : rsaBean
     * @param datos : datos
     * @return Map<String, Object> datos para agregar a la vista
     */
    public Map<String, Object> mostrarPaginaImagen(RSADTO rsaBean, Map<String, Object> datos);
    
    
    /**
     * @param rsaBean : rsaBean
     * @param sesionId : sesionId
     * @param trasaccionId : trasaccionId
     * @param datos : datos
     * @return Map<String, Object> datos para agregar a la vista
     * @throws BusinessException excepcion de la aplicacion
     */
    public Map<String, Object> direccionaPaginaChallenge(RSADTO rsaBean, String sesionId, 
    		String trasaccionId, Map<String, Object> datos) throws BusinessException;
    
    /**
     * @param rsaBean : rsaBean
     * @param usuario : usuario
     * @param actionCode : action code
     * @param datos : datos 
     * @return Map<String, Object> datos para agregar a la vista
     */
    public Map<String, Object> eventoDeny(RSADTO rsaBean, String usuario, String actionCode, 
    		Map<String, Object> datos);
    
    /**
     * envioCorreo
     * @param usuario : usuario
     * @param evento : evento
     * @throws BusinessException manejo de la excepcion
     */
    public void envioCorreo(String usuario, String evento) throws BusinessException;
    
     /**
     * @param rsaBean : rsaBean
     * @param entrada : entrada
     * @return Map<String, Object> datos
     * @throws BusinessException manejo de excepcion
     */
    public Map<String, Object> validarRespuestaCliente(RSADTO rsaBean, Map<String, 
    		 String> entrada) throws BusinessException;
     
     /**
      * @param estatus : estatus
      * @param estatusUsr : estatus usuario
      * @param idPregunta : pregunta
      * @param txtPregunta : texto pregunta
      * @param idSesion : sesion
      * @param idTransaccion : transaccion
      * @param tipoDisp : tipo de dispositivo
      * @param rsaBean : rsaBean
      * @param datos : datos
      * @param usuario : usuario
      * @return Map<String, Object> datos
      */
     public Map<String, Object> evaluarRespuestaChallenge (String estatus, String estatusUsr, 
             String idPregunta, String txtPregunta, String idSesion, String idTransaccion,
             String tipoDisp, RSADTO rsaBean, Map<String, Object> datos, String usuario);

}
