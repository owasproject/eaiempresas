/**
 * Isban Mexico
 *   Clase: BOLRegistraBitacoraAdministrativa.java
 *   Descripción: Componente que registra los movimientos en
 *   bitacora administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 23, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.servicio;

import javax.ejb.Remote;

import com.isban.eai.enlace.dto.VersionRSADTO;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * Componente que registra los movimientos en bitacora administrativa.
 */
@Remote
public interface BOVersionRSA {
	
	/**
	 * @return DTO con el valor de la version 
	 * @throws BusinessException exception
	 */
	public VersionRSADTO consulta()
            throws BusinessException;
	
}
