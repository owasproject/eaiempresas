/**
 * Isban Mexico
 *   Clase: BORODB6EJB.java
 *   Descripción: BO para la transaccion ODB&
 *
 *   Control de Cambios:
 *   1.0 Diciembre 10, 2012 asanjuan - Creacion
 */
package com.isban.eai.enlace.servicio;

import javax.ejb.Remote;

import com.isban.eai.enlace.dto.trans390.ODB6Response;
import com.isban.ebe.commons.exception.BusinessException;


@Remote
public interface BORODB6EJB {

	
	/**
	 * @param nomPersona : numero de persona a consultar
	 * @return String : Correo electronico 
	 * @throws BusinessException : excpetion
	 */
	public ODB6Response consultaCorreo( String nomPersona)
		throws BusinessException;
	
	
}
