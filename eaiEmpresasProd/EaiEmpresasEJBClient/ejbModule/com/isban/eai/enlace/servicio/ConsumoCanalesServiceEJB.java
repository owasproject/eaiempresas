package com.isban.eai.enlace.servicio;

import javax.ejb.Remote;

@Remote
public interface ConsumoCanalesServiceEJB {
	
	
	/**
	 * bloquea token del usuario
	 * @param usuario : usuario
	 * @param aplicacion : aplicacion
	 * @return Lista con la respuesta
	 */
	public String[] bloquearTokenUsuario(String usuario, String aplicacion);
	
	/**
	 * Optiene el estatus del usuario y valida si es valido o invalido
	 * de acuerdo a la validacion regresada por el ws 
	 * donde 0|Estatus|Motivo de bloqueo y 
	 * Estatus: 8 Bloqueado
	 * Motivo de Bloqueo: 5 Intentos fallidos
	 * @param codCliente : codigo de cliente
	 * @return String : V=valido � I=invalido
	 */
	public String getEstatus(String codCliente);


}
