package com.isban.eai.enlace.dto;

import java.io.Serializable;

/**
 * @author bverduzco
 *
 */
public class CorreoDTO implements Serializable{
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -678654657687L;
	/**
	 * tipoCorreo
	 */
	private String tipoCorreo;
	/**
	 * codigoCliente
	 */
	private String codigoCliente;
	/**
	 * codigoCliente
	 */
	private String nombreCliente;
	
	/**
	 * aplicacion
	 */
	private String aplicacion;
	/**
	 * to
	 */
	private String to;
	/**
	 * @return tipoCorreo
	 */
	public String getTipoCorreo() {
		return tipoCorreo;
	}
	/**
	 * @param tipoCorreo : tipoCorreo
	 */
	public void setTipoCorreo(String tipoCorreo) {
		this.tipoCorreo = tipoCorreo;
	}
	/**
	 * @return codigoCliente
	 */
	public String getCodigoCliente() {
		return codigoCliente;
	}
	/**
	 * @param codigoCliente : codigoCliente
	 */
	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	/**
	 * @return nombreCliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}
	/**
	 * @param nombreCliente : nombreCliente
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	/**
	 * @return to
	 */
	public String getTo() {
		return to;
	}
	/**
	 * @param to : to
	 */
	public void setTo(String to) {
		this.to = to;
	}
	
	/**
	 * @return aplicacion para definir el temple del correo
	 */
	public String getAplicacion() {
		return aplicacion;
	}
	
	/**
	 * @param aplicacion : aplicacion para el manejo del template
	 */
	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}
	
	

}
