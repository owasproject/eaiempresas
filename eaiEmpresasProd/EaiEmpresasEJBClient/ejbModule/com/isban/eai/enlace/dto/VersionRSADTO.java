/**
 * Isban Mexico
 *   Clase: BitacoraTamSamDTO.java
 *   Descripción: Componente de transporte de datos para la
 *   bitacora TAMSAM.
 *
 *   Control de Cambios:
 *   1.0 Mar 11, 2013 Stefanini - Creacion
 */
package com.isban.eai.enlace.dto;

import java.io.Serializable;

/**
 * Componente de transporte de datos para la consulta de la bandera de la version de RSA
 */
public class VersionRSADTO implements Serializable {

    /**
     * La version de la clase.
     */
    private static final long serialVersionUID = 8999083725278537578L;


    /**
     * nombre de la propiedad
     */
    private String propiedad;
   
    /**
     * valor que contiene la propiedad
     */
    private String valor;

	/**
	 * @param propiedad nombre de la propiedad
	 */
	public void setPropiedad(String propiedad) {
		this.propiedad = propiedad;
	}
	
	/**
	 * @return String propiedad
	 */
	public String getPropiedad() {
		return propiedad;
	}
	
	/**
	 * @param valor valor de la propiedad
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	/**
	 * @return String valor
	 */
	public String getValor() {
		return valor;
	}
		
}
