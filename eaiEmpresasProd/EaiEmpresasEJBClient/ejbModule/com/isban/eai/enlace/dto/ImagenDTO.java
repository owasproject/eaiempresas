package com.isban.eai.enlace.dto;

import java.io.Serializable;

public class ImagenDTO implements Serializable{
	
	/**
	 * numero de serial serialVersionUID
	 */
	private static final long serialVersionUID = 6379702278566743148L;
	/**
	 * imagen
	 */
	private String imagen;
	/**
	 * idImagen
	 */
	private String idImagen;
	
	
	/**
	 * @return imagen
	 */
	public String getImagen() {
		return imagen;
	}
	/**
	 * @param imagen : imagen
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	/**
	 * @return idImagen
	 */
	public String getIdImagen() {
		return idImagen;
	}
	/**
	 * @param idImagen : idImagen
	 */
	public void setIdImagen(String idImagen) {
		this.idImagen = idImagen;
	}
	
	

}
