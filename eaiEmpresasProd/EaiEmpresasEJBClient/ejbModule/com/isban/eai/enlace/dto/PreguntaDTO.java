package com.isban.eai.enlace.dto;

import java.io.Serializable;

public class PreguntaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5552146589398138893L;

	/**
	 * preguntaTexto
	 */
	private String preguntaTexto;
	/**
	 * preguntaID
	 */
	private String preguntaID;
	
	/**
	 * @return preguntaTexto
	 */
	public String getPreguntaTexto() {
		return preguntaTexto;
	}
	/**
	 * @param preguntaTexto : preguntaTexto
	 */
	public void setPreguntaTexto(String preguntaTexto) {
		this.preguntaTexto = preguntaTexto;
	}
	/**
	 * @return preguntaID
	 */
	public String getPreguntaID() {
		return preguntaID;
	}
	/**
	 * @param preguntaID : preguntaID
	 */
	public void setPreguntaID(String preguntaID) {
		this.preguntaID = preguntaID;
	}
	
	
}
