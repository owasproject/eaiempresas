package com.isban.eai.enlace.dto;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestAuthSAMDTO implements Serializable {

	/** Lla ve genenrada **/
	private static final long serialVersionUID = 2045763040779166519L;
	/** request **/
	// private HttpServletRequest request;
	/** response **/
	// private HttpServletResponse response;
	/** user **/
	private String user;
	/** password **/
	private String password;

	// /**
	// * @return request : request
	// */
	// public HttpServletRequest getRequest() {
	// return request;
	// }
	//	
	// /**
	// * @param request : request
	// */
	// public void setRequest(HttpServletRequest request) {
	// this.request = request;
	// }
	//	
	// /**
	// * @return response : response
	// */
	// public HttpServletResponse getResponse() {
	// return response;
	// }
	//	
	// /**
	// * @param response : response
	// */
	// public void setResponse(HttpServletResponse response) {
	// this.response = response;
	// }

	/**
	 * @return user : user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user
	 *            : user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return String : password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            : password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
