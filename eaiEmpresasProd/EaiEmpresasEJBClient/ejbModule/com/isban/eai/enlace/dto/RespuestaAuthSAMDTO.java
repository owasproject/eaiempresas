package com.isban.eai.enlace.dto;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RespuestaAuthSAMDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1882277049500173542L;
	
	/**
	 * autenticado
	 */
	private boolean autenticado;
	/**
	 * errorAcceso
	 */
	private String errorAcceso;
	/**
	 * autorizado
	 */
	private boolean autorizado;
	/**
	 * expirado
	 */
	private boolean expirado;
	/**
	 * request
	 */
	private HttpServletRequest request;
	/**
	 *  response
	 */
	private HttpServletResponse response;
	
	/**
	 * @return autenticado
	 */
	public boolean isAutenticado() {
		return autenticado;
	}
	/** 
	 * @param autenticado : autenticado
	 */
	public void setAutenticado(boolean autenticado) {
		this.autenticado = autenticado;
	}
	/**
	 * @return errorAcceso
	 */
	public String getErrorAcceso() {
		return errorAcceso;
	}
	/**
	 * @param errorAcceso : errorAcceso
	 */
	public void setErrorAcceso(String errorAcceso) {
		this.errorAcceso = errorAcceso;
	}
	/**
	 * @return autorizado
	 */
	public boolean isAutorizado() {
		return autorizado;
	}
	/**
	 * @param autorizado :  autorizado
	 */
	public void setAutorizado(boolean autorizado) {
		this.autorizado = autorizado;
	}
	/**
	 * @return expirado
	 */
	public boolean isExpirado() {
		return expirado;
	}
	/**
	 * @param expirado : expirado 
	 */
	public void setExpirado(boolean expirado) {
		this.expirado = expirado;
	}
	/**
	 * @return request
	 */
	public HttpServletRequest getRequest() {
		return request;
	}
	/**
	 * @param request : request
	 */
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	/**
	 * @return response
	 */
	public HttpServletResponse getResponse() {
		return response;
	}
	/**
	 * @param response : response
	 */
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	
	
	
}
