/**
 * 
 * @author asanjuan
 * @version 1.0
 * Descripcion: dto para la transaccion PE68
 * Creador: Arturo Sanjuan
 */
package com.isban.eai.enlace.dto.trans390;

import java.io.Serializable;

public class ODB6Response implements Serializable {

	/**
	 * version de la clase
	 */
	private static final long serialVersionUID = 695666345053484199L;
	
	/**codigo de error**/
	private String codError;
	/**correo**/
	private String correo;
	
	/**
	 * @return String cod error
	 */
	public String getCodError() {
		return codError;
	}
	
	/**
	 * @param codError : codigo error
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}
	
	/**
	 * @return String : codigo error
	 */
	public String getCorreo() {
		return correo;
	}
	
	/**
	 * @param correo :codigo error
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	
}
