package com.isban.eai.enlace.dto;

import java.io.Serializable;

import mx.isban.rsa.aa.ws.DeviceRequest;


public class RSADTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6943086027628467284L;
	
	/**	Caracteristicas del dispositivo usado */
	private DeviceRequest deviceRequest;	
	/**	Nombre del usuario (codigo de cliente) */
	private String userName;
	/**	Pais del usuario */
	private String userCountry;
	/**	Nombre de la organizacion a la que pertenece el usuario */
	private String orgName;
	/**	Identificador de sesion */
	private String idSession;
	/**	Lenguaje de preferencia */
	private String userLanguage;
	/**	Identificador de la sesion de autenticacion */
	private String authSessionId;
	/**	Identificador de la transaccion de autenticacion */
	private String authTransactionId;
	/** ipOrigen*/
	private String ipOrigen;
	/** hostName*/
	private String hostName;
	/** idSesion id de la sesion del navegador*/
	private String idSesion;
	/** aplicacion nombre de la aplicacion a la que se quiere ingresar para generar bitacoras*/
	private String aplicacion = "E";
	/** versionRSA valor que indica la version de RSA que se utilizara*/
	private String versionRSA;
	
	/**
	 * Obtener AuthSessionId
	 * @return Identificador de la sesion de autenticacion
	 */
	public String getAuthSessionId() {
		return authSessionId;
	}
	/**
	 * Asignar AuthSessionId
	 * @param authSessionId Identificador de la sesion de autenticacion
	 */
	public void setAuthSessionId(String authSessionId) {
		this.authSessionId = authSessionId;
	}
	 /** 
	  * Obtener AuthTransactionId
	 * @return Identificador de la transaccion de autenticacion
	 */
	public String getAuthTransactionId() {
		return authTransactionId;
	}
	/**
	 * Asignar AuthTransactionId
	 * @param authTransactionId Identificador de la transaccion de autenticacion
	 */
	public void setAuthTransactionId(String authTransactionId) {
		this.authTransactionId = authTransactionId;
	}
	/**
	/**
	 * Obtener DeviceRequest
	 * @return Caracteristicas del dispositivo usado
	 */
	public DeviceRequest getDeviceRequest() {
		return deviceRequest;
	}
	/**
	 * Asignar DeviceRequest
	 * @param deviceRequest Caracteristicas del dispositivo usado
	 */
	public void setDeviceRequest(DeviceRequest deviceRequest) {
		this.deviceRequest = deviceRequest;
	}
	/**
	 * Obtener UserLanguage
	 * @return Lenguaje de preferencia
	 */
	public String getUserLanguage() {
		return userLanguage;
	}
	/**
	 * Asignar UserLanguage
	 * @param userLanguage Lenguaje de preferencia
	 */
	public void setUserLanguage(String userLanguage) {
		this.userLanguage = userLanguage;
	}
	/**
	 * Obtener IdSession
	 * @return Identificador de sesion
	 */
	public String getIdSession() {
		return idSession;
	}
	/**
	 * Asignar IdSession
	 * @param idSession Identificador de sesion
	 */
	public void setIdSession(String idSession) {
		this.idSession = idSession;
	}
	/**
	 * Obtener OrgName
	 * @return Nombre de la organizacion a la que pertenece el usuario
	 */
	public String getOrgName() {
		return orgName;
	}
	/**
	 * Asignar OrgName
	 * @param orgName Nombre de la organizacion a la que pertenece el usuario
	 */
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	/**
	 * Obtener UserCountry
	 * @return Pais del usuario
	 */
	public String getUserCountry() {
		return userCountry;
	}
	/**
	 * Asignar UserCountry
	 * @param userCountry Pais del usuario
	 */
	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}
	/**
	 * Obtener UserName
	 * @return Nombre del usuario (codigo de cliente)
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * Asignar UserName
	 * @param userName Nombre del usuario (codigo de cliente)
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * 
	 * @return ipOrigen
	 */
	public String getIpOrigen() {
		return ipOrigen;
	}
	
	/**
	 * @param ipOrigen : ipOrigen
	 */
	public void setIpOrigen(String ipOrigen) {
		this.ipOrigen = ipOrigen;
	}
	
	/**
	 * @return hostName
	 */
	public String getHostName() {
		return hostName;
	}
	
	/**
	 * @param hostName : hostName
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	/**
	 * @return idSesion id de la sesion del navegador
	 */
	public String getIdSesion() {
		return idSesion;
	}
	
	/**
	 * @param idSesion : idSesion id de la sesion del navegador
	 */
	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}
	
	/**
	 * @return String aplicacion
	 */
	public String getAplicacion() {
		return aplicacion;
	}
	
	/**
	 * @param aplicacion : indica la aplicacion a la que se esta ingresando 
	 */
	public void setAplicacion(String aplicacion) {
		this.aplicacion = aplicacion;
	}	
	
	/**
	 * @return String versionRSA
	 */
	public String getVersionRSA() {
		return versionRSA;
	}
	
	/**
	 * @param versionRSA : indica la version de RSA que se esta utilizando
	 */
	public void setVersionRSA(String versionRSA) {
		this.versionRSA = versionRSA;
	}	
		
}
