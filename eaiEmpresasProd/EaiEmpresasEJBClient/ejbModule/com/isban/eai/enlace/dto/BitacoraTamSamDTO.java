/**
 * Isban Mexico
 *   Clase: BitacoraTamSamDTO.java
 *   Descripci�n: Componente de transporte de datos para la
 *   bitacora TAMSAM.
 *
 *   Control de Cambios:
 *   1.0 Mar 11, 2013 Stefanini - Creacion
 */
package com.isban.eai.enlace.dto;

import java.io.Serializable;

/**
 * Componente de transporte de datos para la bitacora de operaciones TAMSAM
 */
public class BitacoraTamSamDTO implements Serializable {

    /**
     * La version de la clase.
     */
    private static final long serialVersionUID = 8999083725278537578L;


    /**
     * idOperacion identificador
     */
    private String idOperacion;
   
    /**
     * fechaHora fecha y hora de la operaci�n
     */
    private String fechaHora;
    
    /**
     * ipOrigen ip de origen desde donde se realiza la operaci�n
     */
    private String ipOrigen;
   
    /**
     * host name
     */
    private String hostName;
   
    /**
     * usuarioOperante
     */
    private String usuarioOperante;
    
    /**
     * idUsuario identificador de usuario
     */
    private String idUsuario;
    
    /**
     * tipoOperacion tipo de operaci�n que se realiza
     */
    private String tipoOperacion;
    
    /**
     * resOperacion resultado operaci�n
     */
    private String resOperacion;
    
    /**
     * digIntegridad 
     */
    private String digIntegridad;
    
    /**
     * rsaEstatus
     */
    private String rsaEstatus;
    
    /**
     * idInstanciaWeb
     */
    private String idInstanciaWeb;
        
	/**
	 * @return String identificador Operaci�n
	 */
	public String getIdOperacion() {
		return idOperacion;
	}
	
	/**
	 * @param idOperacion identificador de operaci�n
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}

	/**
	 * @return String fecha y hora de la aplicaci�n
	 */
	public String getFechaHora() {
		return fechaHora;
	}

	/**
	 * @param fechaHora fecha y hora de la operaci�n
	 */
	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	/**
	 * @return String ip de origen
	 */
	public String getIpOrigen() {
		return ipOrigen;
	}

	/**
	 * @param ipOrigen ip de origen
	 */
	public void setIpOrigen(String ipOrigen) {
		this.ipOrigen = ipOrigen;
	}

	/**
	 * @return String host name
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * @param hostName host name
	 */
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	/**
	 * @return String usuario operante
	 */
	public String getUsuarioOperante() {
		return usuarioOperante;
	}

	/**
	 * @param usuarioOperante usuario operante
	 */
	public void setUsuarioOperante(String usuarioOperante) {
		this.usuarioOperante = usuarioOperante;
	}

	/**
	 * @return String identificador de usuario
	 */
	public String getIdUsuario() {
		return idUsuario;
	}

	/**
	 * @param idUsuario identificador de usuario
	 */
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	/**
	 * @return String tipo de operaci�n
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion tipo de operaci�n
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	/**
	 * @return String resultado operaci�n
	 */
	public String getResOperacion() {
		return resOperacion;
	}

	/**
	 * @param resOperacion resultado operaci�n
	 */
	public void setResOperacion(String resOperacion) {
		this.resOperacion = resOperacion;
	}

	/**
	 * @return String digIntegridad
	 */
	public String getDigIntegridad() {
		return digIntegridad;
	}

	/**
	 * @param digIntegridad valor de digIntegridad
	 */
	public void setDigIntegridad(String digIntegridad) {
		this.digIntegridad = digIntegridad;
	}

	/**
	 * @return String rsaEstatus
	 */
	public String getRsaEstatus() {
		return rsaEstatus;
	}
	
	/**
	 * @param rsaEstatus valor de rsaEstatus
	 */
	public void setRsaEstatus(String rsaEstatus) {
		this.rsaEstatus = rsaEstatus;
	}

	/**
	 * @return String idInstanciaWeb
	 */
	public String getIdInstanciaWeb() {
		return idInstanciaWeb;
	}

	/**
	 * @param idInstanciaWeb valor de idInstanciaWeb
	 */
	public void setIdInstanciaWeb(String idInstanciaWeb) {
		this.idInstanciaWeb = idInstanciaWeb;
	}
	
	
}
