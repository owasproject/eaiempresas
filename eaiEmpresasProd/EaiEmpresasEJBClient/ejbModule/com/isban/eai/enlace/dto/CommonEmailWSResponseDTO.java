/**
 * CommonEmailWSResponseDTO.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.dto;

public class CommonEmailWSResponseDTO  implements java.io.Serializable {
	
    
    /**
	 * version
	 */
	private static final long serialVersionUID = 2421720526331556248L;

	/**
     * Type metadata
     */
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CommonEmailWSResponseDTO.class, true);
    
    /**
     * __equalsCalc
     */
    private transient java.lang.Object equalsCalc = null;
    
    /**
     * __hashCodeCalc
     */
    private transient boolean hashCodeCalc = false;
    
	/**
	 * codigoError
	 */
	private java.lang.String codigoError;

    /**
     * idMensaje
     */
    private java.lang.String idMensaje;

    /**
     * msgError
     */
    private java.lang.String msgError;

    /**
     * Constructor
     */
    public CommonEmailWSResponseDTO() {
    }

    /**
     * CommonEmailWSResponseDTO
     * @param codigoError codigo de errror
     * @param idMensaje identificador del mensaje
     * @param msgError mensaje de error
     */
    public CommonEmailWSResponseDTO(
           java.lang.String codigoError,
           java.lang.String idMensaje,
           java.lang.String msgError) {
           this.codigoError = codigoError;
           this.idMensaje = idMensaje;
           this.msgError = msgError;
    }


    /**
     * Gets the codigoError value for this CommonEmailWSResponseDTO.
     * 
     * @return codigoError
     */
    public java.lang.String getCodigoError() {
        return codigoError;
    }
    
    /**
     * Sets the codigoError value for this CommonEmailWSResponseDTO.
     * @param codigoError codigo de error
     */
    public void setCodigoError(java.lang.String codigoError) {
        this.codigoError = codigoError;
    }

    /**
     * Gets the idMensaje value for this CommonEmailWSResponseDTO.
     * @return idMensaje identificador del mensaje
     */
    public java.lang.String getIdMensaje() {
        return idMensaje;
    }

    /**
     * Sets the idMensaje value for this CommonEmailWSResponseDTO.
     * @param idMensaje identificador del mensaje
     */
    public void setIdMensaje(java.lang.String idMensaje) {
        this.idMensaje = idMensaje;
    }

    /**
     * Gets the msgError value for this CommonEmailWSResponseDTO.
     * @return msgError mensaje de error
     */
    public java.lang.String getMsgError() {
        return msgError;
    }


    /**
     * Sets the msgError value for this CommonEmailWSResponseDTO.
     * @param msgError mensaje de error
     */
    public void setMsgError(java.lang.String msgError) {
        this.msgError = msgError;
    }
    
    /** {@inheritDoc} */
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CommonEmailWSResponseDTO)){ 
        	return false;
        }
        final CommonEmailWSResponseDTO other = (CommonEmailWSResponseDTO) obj;
        if (obj == null){
        	return false;
        }
        if (this == obj){
        	return true;
        }
        if (equalsCalc != null) {
            return (equalsCalc == obj);
        }
        equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codigoError==null && other.getCodigoError()==null) || 
             (this.codigoError!=null &&
              this.codigoError.equals(other.getCodigoError()))) &&
            ((this.idMensaje==null && other.getIdMensaje()==null) || 
             (this.idMensaje!=null &&
              this.idMensaje.equals(other.getIdMensaje()))) &&
            ((this.msgError==null && other.getMsgError()==null) || 
             (this.msgError!=null &&
              this.msgError.equals(other.getMsgError())));
        equalsCalc = null;
        return _equals;
    }
    
    /** {@inheritDoc} */
    public synchronized int hashCode() {
        if (hashCodeCalc) {
            return 0;
        }
        hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodigoError() != null) {
            _hashCode += getCodigoError().hashCode();
        }
        if (getIdMensaje() != null) {
            _hashCode += getIdMensaje().hashCode();
        }
        if (getMsgError() != null) {
            _hashCode += getMsgError().hashCode();
        }
        hashCodeCalc = false;
        return _hashCode;
    }

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws.email.common.santander.com/", "commonEmailWSResponseDTO"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codigoError");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codigoError"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idMensaje");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idMensaje"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgError");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgError"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * @return typeDesc type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     * @param mechType tipo de mech
     * @param _javaType tipo java
     * @param _xmlType tipo xml
     * @return typeDesc tipo desc
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     * @param mechType tipo de mech
     * @param _javaType tipo java
     * @param _xmlType tipo xml
     * @return typeDesc tipo desc
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
