package com.isban.eai.enlace.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ConsultaDTO implements Serializable {

	/**
	 * serial
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * tipoConsulta
	 */
	private String tipoConsulta=""; //para definir si es una consulta por fecha = 1, corresponsalia = 2, corresponsal = 3, 
							//	                                  edo = 4, sucursal = 5
	/**
	 * indPagina
	 */
	private String indPagina="";
	/**
	 * numRegsPag
	 */
	private String numRegsPag="";
	/**
	 * fchProcPaginado
	 */
	private String fchProcPaginado="";
	/**
	 * fchTransPaginado
	 */
	private String fchTransPaginado="";
	/**
	 * estatusOper
	 */
	private String estatusOper="";
	
	/**
	 * estatus
	 */
	private String estatus;
	
	/**
	 * @return tipoConsulta
	 */
	public String getTipoConsulta() {
		return tipoConsulta;
	}
	/**
	 * @param tipoConsulta : tipoConsulta
	 */
	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	/**
	 * @return indPagina
	 */
	public String getIndPagina() {
		return indPagina;
	}
	/**
	 * @param indPagina : indPagina
	 */
	public void setIndPagina(String indPagina) {
		this.indPagina = indPagina;
	}
	/**
	 * @return numRegsPag
	 */
	public String getNumRegsPag() {
		return numRegsPag;
	}
	/**
	 * @param numRegsPag : numRegsPag
	 */
	public void setNumRegsPag(String numRegsPag) {
		this.numRegsPag = numRegsPag;
	}
	/**
	 * @return fchProcPaginado
	 */
	public String getFchProcPaginado() {
		return fchProcPaginado;
	}
	/**
	 * @param fchProcPaginado : fchProcPaginado
	 */
	public void setFchProcPaginado(String fchProcPaginado) {
		this.fchProcPaginado = fchProcPaginado;
	}
	/**
	 * @return fchTransPaginado
	 */
	public String getFchTransPaginado() {
		return fchTransPaginado;
	}
	/**
	 * @param fchTransPaginado : fchTransPaginado
	 */
	public void setFchTransPaginado(String fchTransPaginado) {
		this.fchTransPaginado = fchTransPaginado;
	}
	/**
	 * @return estatusOper
	 */
	public String getEstatusOper() {
		return estatusOper;
	}
	/**
	 * @param estatusOper : estatusOper
	 */
	public void setEstatusOper(String estatusOper) {
		this.estatusOper = estatusOper;
	}
	
	/**
	 * @return estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus : estatus
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	
}
