/**
 * Isban Mexico
 *   Clase: FiltroConsultaBitaAdmin.java
 *   Descripción: El componente con los datos del filtro de consulta de la
 *   bitacora administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.filtro;

import java.io.Serializable;

/**
 * El componente con los datos del filtro de consulta de la bitacora
 * administrativa.
 */
public class FiltroConsultaBitaAdmin implements Serializable {

    /**
     * La version de la clase.
     */
    private static final long serialVersionUID = -1428062491637302219L;

    /**
     * fechaInicio
     */
    private String fechaInicio;
    
    /**
     * fechaFin
     */
    private String fechaFin;
    
    /**
     * usuario
     */
    private String usuario;

    /** {@inheritDoc} */
    public String toString() {

        return String.format("fechaIni:%s|fechaFin:%s|usuario:%s",
                getFechaInicio(), getFechaFin(), getUsuario());
    }

    /**
     * Obtiene la fecha de inicio.
     * @return la fecha de inicio.
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Asigna la fecha de inicio.
     * @param fechaInicio la fecha de inicio.
     */
    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * Obtiene la fecha final.
     * @return la fecha final.
     */
    public String getFechaFin() {
        return fechaFin;
    }

    /**
     * Asigna la fecha final.
     * @param fechaFin la fecha final.
     */
    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * Obtiene el usuario.
     * @return el usuario.
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Asigna el usuario.
     * @param usuario el usuario.
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

}
