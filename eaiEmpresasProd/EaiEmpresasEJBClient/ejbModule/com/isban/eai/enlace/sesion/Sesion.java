/**
 * Isban Mexico
 *   Clase: Sesion.java
 *   Descripción: Componente con la informacion de sesion del usuario.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.sesion;

import com.isban.eai.enlace.dto.CambioSAMDTO;

/**
 * Componente con la informacion de sesion del usuario.
 */
public interface Sesion  {

    /**
     * Obtiene la clave del usuario en sesion.
     * @return la clave del usuario en sesion.
     **/
    public String getClaveUsuario();

    /**
     * Asigna la clave del usuario en sesion.
     * @param claveUsuario la clave del usuario en sesion.
     **/
    public void setClaveUsuario(String claveUsuario);

    /**
     * Obtiene la direccion IP del usuario en sesion.
     * @return la direccion IP del usuario en sesion.
     */
    public String getDireccionIp();

    /**
     * Asigna la direccion IP del usuario en sesion.
     * @param direccionIp la direccion IP del usuario en sesion.
     */
    public void setDireccionIp(String direccionIp);

    /**
     * Obtiene el ID de la sesion del usuario.
     * @return el ID de la sesion del usuario.
     */
    public String getIdSesion();

    /**
     * Asigna el ID de la sesion del usuario.
     * @param idSesion el ID de la sesion del usuario.
     */
    public void setIdSesion(String idSesion);

    /**
     * Obtiene el ID de la instancia Web del usuario.
     * @return el ID de la instancia Web del usuario.
     */
    public String getIdInstancia();

    /**
     * Asigna el ID de la instancia Web del usuario.
     * @param idInstancia el ID de la instancia Web del usuario.
     */
    public void setIdInstancia(String idInstancia);

    /**
     * Obtiene el nombre del host Web del usuario.
     * @return el nombre del host Web del usuario.
     */
    public String getNombreHost();

    /**
     * Asigna el nombre del host Web del usuario.
     * @param nombreHost el nombre del host Web del usuario.
     */
    public void setNombreHost(String nombreHost);

    /**
     * El componente con la informacion de usuario de SAM.
     * @return el componente con la informacion de usuario de SAM.
     **/
    public CambioSAMDTO getUsuarioSam();

    /**
     * Asigna el componente con la informacion de usuario de SAM.
     * @param usuarioSam el componente con la informacion de usuario de SAM.
     **/
    public void setUsuarioSam(CambioSAMDTO usuarioSam);
}
