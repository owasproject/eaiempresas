/**
 * Isban Mexico
 *   Clase: SesionImpl.java
 *   Descripción:  Componente que almacena la informacion de sesion del usuario.
 *
 *   Control de Cambios:
 *   1.0 Mar 14, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.sesion;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;

import com.isban.eai.enlace.dto.CambioSAMDTO;
import com.isban.eai.enlace.servicio.BORCambioSAMEJB;
import com.isban.ebe.commons.exception.BusinessException;


/**
 * Componente que almacena la informacion de sesion del usuario.
 */
public class SesionImpl implements Sesion, Serializable {

	/**
     * El objeto de escritura en LOG.
     **/
    private static final Logger LOGGER = Logger.getLogger(SesionImpl.class);

    /**
     * El componente de negocio para registrar los cambios del usuario en
     * sesion.
     **/
    private BORCambioSAMEJB boCambioSam;

    /**
     * La version de la clase.
     */
    private static final long serialVersionUID = 2444746185586178082L;

    /**
     * La clave del usuario en sesion.
     **/
    private String claveUsuario;

    /**
     * La direccion IP del usuario en sesion.
     **/
    private String direccionIp;

    /**
     * El ID de la sesion del usuario.
     **/
    private String idSesion;

    /**
     * El ID de la instancia del usuario.
     **/
    private String idInstancia;

    /**
     * El nombre del host.
     **/
    private String nombreHost;

    /**
     * Los datos del usuario en Sam.
     **/
    private CambioSAMDTO usuarioSam;

    /** {@inheritDoc} */
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((claveUsuario == null) ? 0 : claveUsuario.hashCode());
        result = prime * result
                + ((idSesion == null) ? 0 : idSesion.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SesionImpl other = (SesionImpl) obj;
        if (claveUsuario == null) {
            if (other.claveUsuario != null) {
                return false;
            }
        } else if (!claveUsuario.equals(other.claveUsuario)) {
            return false;
        }
        if (idSesion == null) {
            if (other.idSesion != null) {
                return false;
            }
        } else if (!idSesion.equals(other.idSesion)) {
            return false;
        }
        return true;
    }

    /** {@inheritDoc} */
    public String toString() {

        return String.format("Usuario:%s|IdSesion:%s", getClaveUsuario(),
                getIdSesion());
    }

    /**
     * Obtiene la direccion IP del usuario en sesion.
     * @return la direccion IP del usuario en sesion.
     */
    public String getDireccionIp() {
        return direccionIp;
    }

    /**
     * Asigna la direccion IP del usuario en sesion.
     * @param direccionIp la direccion IP del usuario en sesion.
     */
    public void setDireccionIp(String direccionIp) {
        this.direccionIp = direccionIp;
    }

    /**
     * Obtiene el ID de la sesion del usuario.
     * @return el ID de la sesion del usuario.
     */
    public String getIdSesion() {
        return idSesion;
    }

    /**
     * Asigna el ID de la sesion del usuario.
     * @param idSesion el ID de la sesion del usuario.
     */
    public void setIdSesion(String idSesion) {
        this.idSesion = idSesion;
    }

    /**
     * Obtiene el ID de la instancia Web del usuario.
     * @return el ID de la instancia Web del usuario.
     */
    public String getIdInstancia() {
        return idInstancia;
    }

    /**
     * Asigna el ID de la instancia Web del usuario.
     * @param idInstancia el ID de la instancia Web del usuario.
     */
    public void setIdInstancia(String idInstancia) {
        this.idInstancia = idInstancia;
    }

    /**
     * Obtiene el nombre del host Web del usuario.
     * @return el nombre del host Web del usuario.
     */
    public String getNombreHost() {
        return nombreHost;
    }

    /**
     * Asigna el nombre del host Web del usuario.
     * @param nombreHost el nombre del host Web del usuario.
     */
    public void setNombreHost(String nombreHost) {
        this.nombreHost = nombreHost;
    }

    /**
     * Obtiene la clave del usuario en sesion.
     * @return la clave del usuario en sesion.
     */
    public String getClaveUsuario() {
        return claveUsuario;
    }

    /**
     * Asigna la clave del usuario en sesion.
     * @param claveUsuario la clave del usuario en sesion.
     */
    public void setClaveUsuario(String claveUsuario) {
        this.claveUsuario = claveUsuario;
    }

    /** {@inheritDoc} */
    public CambioSAMDTO getUsuarioSam() {

        return this.usuarioSam;
    }

    /** {@inheritDoc} */
    public void setUsuarioSam(CambioSAMDTO usuarioSam) {
        this.usuarioSam = usuarioSam;
    }

    /**
     * {@code destroy-method} para las sesiones de la aplicacion.
     **/
    public void registraBajaSesion() {
    	if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Se ha terminado la session");
        }

        // Se invalida la sesion.
        getUsuarioSam().setValida(false);
        
        try {
            getBoCambioSam().updateSession(getSesion());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(String.format(
                        "La session fue actualizada en la DB - %s",
                        getClaveUsuario()));
            }
        } catch (BusinessException e) {
            LOGGER.error(e);
        }
    }

    /**
     * Metodo que realiza la clonacion del pojo de sesion.
     * @return clone datos
     */
    private Sesion getSesion() {

        SesionImpl clone = new SesionImpl();
        try {
            BeanUtils.copyProperties(clone, this);
        } catch (IllegalAccessException e) {
            LOGGER.error(e);
        } catch (InvocationTargetException e) {
            LOGGER.error(e);
        }
        return clone;
    }

    /**
     * Obtiene el componente de negocio para registrar los cambios del
     * usuario en sesion.
     * @return el componente de negocio para registrar los cambios del
     *  usuario en sesion.
     */
    public BORCambioSAMEJB getBoCambioSam() {
        return boCambioSam;
    }

    /**
     * Asigna el componente de negocio para registrar los cambios del
     * usuario en sesion.
     * @param boCambioSam el componente de negocio para registrar los
     *  cambios del usuario en sesion.
     */
    public void setBoCambioSam(BORCambioSAMEJB boCambioSam) {
        this.boCambioSam = boCambioSam;
    }

}
