/**
 * Isban Mexico
 *   Clase: DAOBitacoraTamSam.java
 *   Descripci�n: El componente de acceso a datos para la
 *   bitacora TAMSAM.
 *
 *   Control de Cambios:
 *   1.0 Mar 11, 2013 Stefanini - Creacion
 */
package com.isban.eai.enlace.dao;

import javax.ejb.Local;

import com.isban.eai.enlace.dto.VersionRSADTO;
import com.isban.ebe.commons.exception.BusinessException;

/**
 * El componente de acceso a datos para la consulta de la versi�n a usar para RSA.
 */
@Local
public interface DAOVersionRSA {
    	
    /**
     * La instruccion Insert para el registro en bitacora.
     **/
    public static final String SELECT = new StringBuilder().append(
	    "SELECT VAL_TXT FROM EAI_MX_MAE_PARAMETROS WHERE COD_PAR = 'rsaFlag'").toString();
	
    /**
     * @return VersionRSADTO objeto de las propiedades de la version de RSA
     * @throws BusinessException : excepci�n de aplicaci�n
     */
    public VersionRSADTO consulta()
    throws BusinessException ;
}
