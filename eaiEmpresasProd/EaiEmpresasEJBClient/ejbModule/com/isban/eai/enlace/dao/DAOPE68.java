package com.isban.eai.enlace.dao;

import javax.ejb.Local;

import com.isban.eai.enlace.dto.trans390.PE68Response;
import com.isban.ebe.commons.exception.BusinessException;

@Local
public interface DAOPE68 {
	
	/**
	 * si regresa codigo EXI000 la operacion es correacta de lo contrario hubo fallo
	 * @param nomPersona : numero de persona a consultar
	 * @return PE68Response : DTO de datos 
	 * @throws BusinessException : excpetion
	 */
	public PE68Response consultaPersona( String nomPersona)
		throws BusinessException;
	

}
