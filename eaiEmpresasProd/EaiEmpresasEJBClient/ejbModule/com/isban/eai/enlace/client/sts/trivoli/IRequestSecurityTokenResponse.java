package com.isban.eai.enlace.client.sts.trivoli;

import org.w3c.dom.Element;

/**
 * This interface is used to parse pieces of interest from responses that come
 * from the STS.
 */
public interface IRequestSecurityTokenResponse {

	/**
	 * @return getRequestedSecurityToken
	 */
	public Element getRequestedSecurityToken();

	/**
	 * @return getRequestedAttachedReference
	 */
	public Element getRequestedAttachedReference();
}
