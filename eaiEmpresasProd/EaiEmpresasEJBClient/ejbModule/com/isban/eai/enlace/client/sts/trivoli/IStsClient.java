package com.isban.eai.enlace.client.sts.trivoli;

/**
 * A WS-Trust client interface doesn't get much more primitive than this - pass
 * in a request, get back a response.
 * 
 */
public interface IStsClient {

	/**
	 * Exchanges an RST for an RSTR. Any errors are returned via a subclass of
	 * RuntimeException, and are not explicitly declared.
	 * 
	 * @param request : request
	 * @return an RSTR
	 */
	public IRequestSecurityTokenResponse sendRequest(
			IRequestSecurityToken request);
}
