/**
 * CommonEmailWSImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.isban.eai.enlace.client.ws;

public interface CommonEmailWSImpl extends javax.xml.rpc.Service {
    
	
	/**
	 * getCommonEmailServiceImplPortAddress
	 * @return getCommonEmailServiceImplPortAddress Direccion Puerto
	 */
	public java.lang.String getCommonEmailServiceImplPortAddress();

    /**
     * getCommonEmailServiceImplPort
     * @return getCommonEmailServiceImplPort Email WS
     * @throws javax.xml.rpc.ServiceException manejo de excepcio
     */
    public com.isban.eai.enlace.client.ws.CommonEmailServiceImpl getCommonEmailServiceImplPort() throws javax.xml.rpc.ServiceException;

    /**
     * getCommonEmailServiceImplPort
     * @param portAddress direccion del puerto
     * @return getCommonEmailServiceImplPort Implementacion service
     * @throws javax.xml.rpc.ServiceException manejo de excepcion
     */
    public com.isban.eai.enlace.client.ws.CommonEmailServiceImpl getCommonEmailServiceImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
