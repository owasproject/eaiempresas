<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<html><!-- #BeginTemplate "/Templates/ayuda.dwt" -->
<head>
<!-- #BeginEditable "doctitle" --> 
<title>Ayuda</title>
<!-- #EndEditable -->
<!-- #BeginEditable "metas" --> 
<meta http-equiv="Content-Type" content="text/html;">
<meta name="Codigo de Pantalla" content="s25050h">
<meta name="Proyecto" content="Portal Santander">
<meta name="Version" content="1.0">
<meta name="Ultima version" content="28/06/2001 19:00">
<meta name="Desarrollo de codigo HTML" content="Grey Interactive Mexico">
<!-- #EndEditable -->

<link href="${pageContext.servletContext.contextPath}/css/ayuda.css" rel="stylesheet" type="text/css" id = "avisoLink">

<SCRIPT LANGUAGE="JavaScript">
<!--
function windowCloser(){
        navWindow = self.parent.close();
}

//-->
</SCRIPT>

<script language="javascript" src="${pageContext.servletContext.contextPath}/js/scrImpresion.js"></script>
</head>

<body bgcolor="#FFFFFF" text="#000000"> 
  <img src = "${pageContext.servletContext.contextPath}/pics/glo25030.gif" id = "glo25030"></img>
  <div align="center"><!-- #BeginEditable "contenido" -->
	<h3 class = "titencayu2">Ayuda</h3>
	<hr id = "arriba"/>
	<hr id = "abajo"/>
          <table border="0" cellspacing="5" cellpadding="0">
            <tr> 
              <td nowrap valign="top" class="titencayu">Pregunta Reto</td>
            </tr>
            <tr> 
              <td class="textabcon" valign="top"> 
                <p>En esta pantalla debera contestar la pregunta reto e indicar si su dispositivo es privado o p&uacute;blico.</p>
                <p class="textabconbol">Para contestar la pregunta reto:</p>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25070.gif" width="25" height="20"></td>
                    <td class="textabcon" valign="top"> 
                      <p>Ingrese su repuesta en el cuadro de texto.</p>
                    </td>
                  </tr>
                </table>         
                <p class="textabconbol">Indique si su dispositivo es privado o p�blico:</p>
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr> 
                    <td valign="top"><img src="${pageContext.servletContext.contextPath}/pics/gic25070.gif" width="25" height="20"></td>
                    <td class="textabcon" valign="top"> 
                      <p>Seleccione una opci&oacute;n dando clic sobre el radio.</p>
                    </td>
                  </tr>
                </table>
                
                
                <p class="textabcon">La opci&oacute;n de acceso directo, le 
                  permitir&aacute; llegar de inmediato a la opci&oacute;n en la 
                  que desea operar, s&oacute;lo tiene que seleccionarla antes 
                  de Iniciar la sesi&oacute;n.</p>
                <p><span class="textabcon"> Si desea eliminar los datos capturados, utilice el bot&oacute;n </span>
                  <span class="textabconbol">Limpiar.</span></p>
              </td>
            </tr>
          </table>
          <!-- #EndEditable --><br>
    <table width="150" border="0" cellspacing="0" cellpadding="0" height="22">
      <tr> 
        <td align="right" width="83" valign="top"> 
          <a href="javascript:scrImpresion()"><img src="${pageContext.servletContext.contextPath}/pics/gbo25240.gif" width="83" height="22" alt="Imprimir" border="0"></a>
        </td>
        <td align="left" width="71" valign="top"> <a href="javascript:windowCloser()"><img src="${pageContext.servletContext.contextPath}/pics/gbo25200.gif?GXHC_GX_jst=e4df5507662d6164&amp;GXHC_gx_session_id_=7e5eb849e43f9a02&amp;" width="71" height="22" alt="Cerrar" border="0"></a></td>
      </tr>
    </table>
    
  </div>

</body>
<!-- #EndTemplate --></html>
