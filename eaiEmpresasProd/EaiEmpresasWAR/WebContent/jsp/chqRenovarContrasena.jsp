<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<%@ page import="com.isban.eai.enlace.util.EnlaceConfig" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="comun.regresar" var="regresar"/>
<spring:message code="comun.enviar" var="enviar"/>
<spring:message code="comun.bienvenido" var="bienvenido"/>
<spring:message code="comun.renovar.anterior" var="anterior"/>
<spring:message code="comun.renovar.nueva" var="nueva"/>
<spring:message code="comun.renovar.confirmar" var="confirmar"/>
<spring:message code="comun.renovar.titulo" var="titulo"/>
<spring:message code="renovar.ERR000.mensaje"  var = "ERR000"/>
<spring:message code="renovar.ERR001.mensaje"      var = "ERR001"/>
<spring:message code="renovar.ERR002.mensaje"  var = "ERR002"/>
<spring:message code="renovar.ERR003.mensaje"      var = "ERR003"/>
<spring:message code="renovar.ERR004.mensaje"      var = "ERR004"/>
<spring:message code="renovar.ERR005.mensaje"      var = "ERR005"/>
<spring:message code="renovar.ERR006.mensaje"      var = "ERR006"/>
<spring:message code="renovar.ERR007.mensaje"      var = "ERR007"/>
<spring:message code="renovar.ERR008.mensaje"      var = "ERR008"/>
<spring:message code="renovar.ERR009.mensaje"      var = "ERR009"/>
<spring:message code="renovar.ERR010.mensaje"      var = "ERR010"/>
<spring:message code="renovar.INF001.mensaje"      var = "INF001"/>
<spring:message code="contrasena.regla.titulo"      var = "CONTREGLATITULO"/>
<spring:message code="contrasena.regla.regla1"      var = "CONTREGLA1"/>
<spring:message code="contrasena.regla.regla2"      var = "CONTREGLA2"/>
<spring:message code="contrasena.regla.regla3"      var = "CONTREGLA3"/>
<spring:message code="contrasena.regla.regla4"      var = "CONTREGLA4"/>


<html>
	<body>
		<div>
			<jsp:include page="headerChqDig.jsp" flush="true" />
			<script type="text/javascript">
  				InitializeTimer(1200);
  			</script>
			<script src="${pageContext.servletContext.contextPath}/js/private/login/chqRenovarContrasena.js" type="text/javascript"></script>
		</div>
		<div id = "cuerpo">
		   	<img id = "e1gif" src = "${pageContext.servletContext.contextPath}/pics/e1.gif" /><img id = "e2gif" src = "${pageContext.servletContext.contextPath}/pics/e2.gif"/><img id = "e3gif" src = "${pageContext.servletContext.contextPath}/pics/e3.gif" />
			<img id = "e8gif" src = "${pageContext.servletContext.contextPath}/pics/e8.gif" />
		        <form id = "formaPrincipal" action="" method="POST" name="formaPrincipal" onload="ocultar();">
		        <input type="hidden" name="pagID" id="pagID" value="renovarCon">
		        <input type="hidden" name="msj" id="msj" value="${msj }">
		        <input type="hidden" name="urlRegreso" id="urlRegreso" value="<%=EnlaceConfig.URL_REGRESO_CHQDIG %>">
		        <input type="hidden" name="chqDig" id="chqDig" value="${chqDig }">
					<fieldset>
						<legend id = "identificador">
							${titulo }
						</legend>
						<table>
							<tr>
								<td> 
									<label for = "password" id = "IdPassword">
										${anterior }
									</label>
								</td>
								<td> 
									<input type = "password" name = "txtPasswordAnt"  maxlength =  "20" size = "20" autocomplete = "off" id = "txtPasswordAnt" />
								</td>
							</tr>
							<tr>
								<td> 
									<label for = "password" id = "IdPassword">
										${nueva }
									</label>
								</td>
								<td> 
									<input type = "password" name = "txtPasswordNvo"  maxlength =  "20" size = "20" autocomplete = "off" id = "txtPasswordNvo" />
								</td>
							</tr>
							<tr>
								<td> 
									<label for = "password" id = "IdPassword">
										${confirmar }
									</label>
								</td>
								<td> 
									<input type = "password" name = "txtPasswordConf"  maxlength =  "20" size = "20" autocomplete = "off" id = "txtPasswordConf" />
								</td>
							</tr>
							<tr>
								<td colspan="2"> 
									<B>${ CONTREGLATITULO}<br>
									<FONT FACE="arial" SIZE=1>
										${CONTREGLA1 } <br>
										${CONTREGLA2 } <br>
										${CONTREGLA3 }<br>
										${CONTREGLA4 }
									</FONT>
									</B>
								</td>
								<td> 
									<br><br>	
								</td>
							</tr>
						</table>
						<input type = "button" name = "IdSubmit" value = "${enviar }" id = "IdSubmit" ondblclick="javascript:this.disabled=true;"/>
						<input type = "button"  value = "${regresar }" name = "IdRegresar" id = "IdRegresar"/>
						<input type = "hidden" name = "password" />
		            </fieldset>
		        </form>
			<img src = "${pageContext.servletContext.contextPath}/pics/e8.gif" alt = "Borde Lado Derecho" id = "bordeDerecho" /> 
		</div>
		<div id = "pie">
		    <img src = "${pageContext.servletContext.contextPath}/pics/e7.gif" id = "e7gif"/><img src = "${pageContext.servletContext.contextPath}/pics/e2.gif" alt = "Barra de Fondo." id = "barraFondo"/><img src = "${pageContext.servletContext.contextPath}/pics/e5.gif" id = "e5gif" />
		</div>
		<script type="text/javascript" defer="defer">
	        var mensajes = [];
	        
	        mensajes['ERR000']  = '${ERR000}';
	        mensajes['ERR001']	  = '${ERR001}';
	        mensajes['ERR002']      = '${ERR002}';
	        mensajes['ERR003'] = '${ERR003}';
	        mensajes['ERR004']      = '${ERR004}';
	        mensajes['ERR005'] = '${ERR005}';
	        mensajes['ERR006'] = '${ERR006}';
	        mensajes['ERR007'] = '${ERR007}';
	        mensajes['ERR008'] = '${ERR008}';
	        mensajes['ERR009'] = '${ERR009}';
	        mensajes['ERR010'] = '${ERR010}';
	        mensajes['INF001'] = '${INF001}';
	        ocultar();
	</script>   
	</body>
</html>