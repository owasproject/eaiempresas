<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<%@ page import="com.isban.eai.enlace.controller.comun.GenericController" %>
<%@ page import="java.util.UUID" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<spring:message code="comun.tituloApp" var="tituloApp"/>
<% 
	String contexto = "";
	try{
		 contexto = request.getSession().getAttribute("finalContext") == null ? GenericController.contexto : 
		 request.getSession().getAttribute("finalContext").toString();
	}catch(NullPointerException e){
		System.out.println("El contexto es nulo, se perdio de sesi�n.");
	} 	
%>
<html>
	<head>
		<title>${tituloApp}</title>			
		<c:if test="${!empty param.trusteer}">		
		<c:if test="${!empty param.login}">
		<script type="text/javascript"> (function() { document.cookie = "___tk114523=" + encodeURIComponent(Math.random()) + ";path=/;domain=santander-serfin.com"; })(); </script>
		<script type="text/javascript"> (function(){var f=document,e=window,i=e.location.protocol,b=[["src",[i=="https:"?"https:/":"http:/","images.santander-serfin.com/114523/buscar.js?r=" + Math.random()].join("/")],["type","text/javascript"],["async",true]],g="XMLHttpRequest",a=null,j=e[g]&&(a=new e[g]()).withCredentials!==undefined,c=f.createElement("script"),h=f.getElementsByTagName("head")[0];if(j){a.open("GET",b[0][1],b[2][1]);a.withCredentials=true;a.onreadystatechange=function(d){if(a.readyState==4&&a.status==200){c.type="script/meta";c.src=b[0][1];h.appendChild(c);new Function(a.responseText)()}};a.send()}else{setTimeout(function(){for(var d=0,k=b.length;d<k;d++){c.setAttribute(b[d][0],b[d][1])}h.appendChild(c)},0)}})(); </script>
		</c:if>
		<c:if test="${empty sessionScope.tsid}">
			<c:set var="tsid" scope="session"><%=UUID.randomUUID().toString()%></c:set>
		</c:if>
		<script type="text/javascript">
			function data1(){return '${sessionScope.tsid}';}
			<c:if test="${!empty sessionScope.userName}">
			function data2(){return JSON.parse('{"p":["${sessionScope.userName}"]}');}
			</c:if>
		</script>		
		<c:choose>
		<c:when test="${!empty param.login}">
		<script type="text/javascript"> (function(d,f){var b={src:(d.location.protocol=="https:"?"https:":"http:")+"//fie.santander-serfin.com/114523/indice.js?r=" + Math.random(),async:true,type:"text/javascript"},g="XMLHttpRequest",c=f.createElement("script"),h=f.getElementsByTagName("head")[0],a;if(d[g]&&(a=new d[g]()).withCredentials!==undefined){a.open("GET",b.src,b.async);a.withCredentials=true;a.onreadystatechange=function(e){if(a.readyState==4&&a.status==200){c.type="script/meta";c.src=b.src;h.appendChild(c);new Function(a.responseText)()}};a.send()}else{setTimeout(function(){for(var e in b){c.setAttribute(e,b[e])}h.appendChild(c)},0)}})(window,document); </script>
		</c:when>
		<c:when test="${(!empty param.contrasena) || (!empty param.imagen)}">
		<script type="text/javascript"> (function(d,f){var b={src:(d.location.protocol=="https:"?"https:":"http:")+"//fie.santander-serfin.com/114523/erario.js?r=" + Math.random(),async:true,type:"text/javascript"},g="XMLHttpRequest",c=f.createElement("script"),h=f.getElementsByTagName("head")[0],a;if(d[g]&&(a=new d[g]()).withCredentials!==undefined){a.open("GET",b.src,b.async);a.withCredentials=true;a.onreadystatechange=function(e){if(a.readyState==4&&a.status==200){c.type="script/meta";c.src=b.src;h.appendChild(c);new Function(a.responseText)()}};a.send()}else{setTimeout(function(){for(var e in b){c.setAttribute(e,b[e])}h.appendChild(c)},0)}})(window,document); </script>
		</c:when>
		</c:choose>		
		<script src="${pageContext.servletContext.contextPath}/js/common.js" type="text/javascript"></script>
		</c:if>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />		
		<link href="${pageContext.servletContext.contextPath}/css/enlace.css" rel="stylesheet" type="text/css" id = "estilos">
		<link href="${pageContext.servletContext.contextPath}/css/enlaceEAI.css" rel="stylesheet" type="text/css" id = "estilos">
		<link href="${pageContext.servletContext.contextPath}/css/AvisoSeguridad.css" rel="stylesheet" type="text/css" id = "avisoLink">
		<link href="${pageContext.servletContext.contextPath}/css/dialogBox/jquery.alerts.css" rel="stylesheet" type="text/css"/>
		
		<script src="${pageContext.servletContext.contextPath}/js/Santander/jquery-1.12.0.min.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/Santander/jquery-migrate-1.3.0.min.js" type="text/javascript"></script>
        <script src="${pageContext.servletContext.contextPath}/js/dialogBox/jquery.ui.draggable.js" type="text/javascript"></script>                
        <script src="${pageContext.servletContext.contextPath}/js/dialogBox/jquery.alerts.js" type="text/javascript"></script>
		
		<script src="${pageContext.servletContext.contextPath}/js/cargar_imagenes.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/enlace.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/errores_login.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/cuadroDialogo.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/mkd25x.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/ObjetoSantander.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/AvisoInstalacion.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/AdmHuella.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/Mensaje.js" type="text/javascript"></script>
		
		<script src="${pageContext.servletContext.contextPath}/js/global.js" type="text/javascript"></script>
		<script src="${pageContext.servletContext.contextPath}/js/sessionTimeout.js" type="text/javascript"></script>
		
		<script type="text/javascript">
                       var jqueryAlerta                 = 'Alerta';
                       var jqueryError                 = 'Error';
                       var jqueryInfo                         = 'Info';
                       var jqueryAyuda                 = 'Ayuda';
                       var jqueryConfirmar         = 'Confirmar';
                       var jqueryAviso                 = 'Aviso';
                       var jquerySelect                 = 'Seleccionar';
                       var contexto 					= '<%= contexto%>';
        </script>
		<script type = "text/javascript">
			var contextPath = "${pageContext.servletContext.contextPath}";
             var estilos  = document.getElementById("estilos");
             var aviso = document.getElementById("avisoLink");
             var userAgent = window.navigator.userAgent;
             
            if(userAgent.indexOf("Firefox") != -1 ) {
                 estilos.href = "./css/enlace_fx.css";
                 aviso.href = "./css/AvisoSeguridadFX.css";
             }
      	</script>
      	<script type = "text/javascript">
			var sError = "";
			sError = "%ERROR%";
		    validaError(sError);
	        var galletitas = document.cookie; 
			if(!SANTANDER.navegadorValido()) {
               SANTANDER.creaVentana();
            }
			if(mkd25x_installed()) {
               function loading_check(){
                  if( mkd25x_loaded() == false ){
                        window.setTimeout( loading_check, 200 );
                  }else{
                        mkd25x_set_authprefix( 'mkd25x' );
                        mkd25x_set_authserver( 'www.cddmex.com' );
                        mkd25x_set_protect_level('high');
                        mkd25x_start_async( 'mf/mkd' );
                  }
               }
               mkd25x_ssl_write_object();
               window.setTimeout( loading_check, 200 );
            }
            function anclas() {
             	var anclaToken = document.getElementById("administracionToken");
             	var anclaContra = document.getElementById("administracionContra");
             	var legend = document.getElementById("identificador");
             	var textoContrasena = document.getElementById("IdPassword");
             	var campoContrasena = document.getElementById("IdPasswordTxt");
            	var urlPathName = window.location.pathname;
            	var querystring =  window.location.search.substring(1);
             	if(urlPathName == "/acceso/EnlaceMig/indexToken.html"  || (querystring.indexOf("plantilla=11") != -1)) {
                  	anclaToken.className = "oculto";
                 	anclaContra.className = "oculto";
                 	campoContrasena.className = "visible";
                 	textoContrasena.className = "visible";
                  	legend.innerHTML = "Bienvenido a Administraci&oacute;n de Token";
             	}else {
             	}
            }

			function FrameAyuda(url) {
				var ruta = contexto;
				if(url == 's26050h'){
					var admToken = window.open(ruta+"/jsp/ayuda/rsa/ayuda.jsp", "hlp", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450" );
				}else if (url == 'challege'){
					var admToken = window.open(ruta+"/jsp/ayuda/ayudaChallenge.jsp", "hlp", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=450" );
				}
			}
			function SinFrames() {
			  if (parent.frames.length > 0) {
			    if(parent.LOGUEADO == true){
			      parent.LOGUEADO = false;
			    }
			    parent.location.href = self.document.location;
			  }
			  //Aviso.inicio();
			}

			function limpiaCampos () {
			   forma = document.forms[0];
			   forma.username.value = "";
			}
		</script>
		<script type="text/javascript">

		  	var _gaq = _gaq || [];
		 	_gaq.push(['_setAccount', 'UA-10967080-4']);
		  	_gaq.push(['_trackPageview']);
	
		  	(function() {
		   		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  	})();

		</script>
	</head>
	<body onload = "presentaHoraFecha();SinFrames();">
	<% System.out.println("***********************----------------------------El contexto es: "+ request.getSession().getAttribute("finalContext"));%>
	<input id="contextPath" type="hidden" value="${pageContext.servletContext.contextPath}"/>
		<div id = "encabezado">
		    <div id = "arriba">
		        <img id = "glo25010" src = "${pageContext.servletContext.contextPath}/pics/glo25010.gif" alt = "Logotipo Enlace">
					<a href = "javascript:;" id ="gbo25110a">
						<img src = "${pageContext.servletContext.contextPath}/pics/gbo25110.gif" alt = "Imagen Sobre" id = "gbo25110" 
							onmouseover = "cambio(this.id)" 
							onmouseout = "regresaImagen(this.id)">
					</a>
		        <img id = "gbo25120" src = "${pageContext.servletContext.contextPath}/pics/gbo25120.gif" alt = "Logotipo Contactanos">
					<a href = "javascript:;" id = "gbo25130a">
						<img src = "${pageContext.servletContext.contextPath}/pics/gbo25130.gif" alt = "Imagen Tel&eacute;fono" id = "gbo25130"
							onmouseover = "cambio(this.id)" 
							onmouseout = "regresaImagen(this.id)">
		            </a>
		         <img id = "gbo25140" src = "${pageContext.servletContext.contextPath}/pics/gbo25140.gif" alt = "Atenci&oacute;n Telef&oacute;nica">
					<a href = "javascript:;" id = "gbo25150a">
						<img src = "${pageContext.servletContext.contextPath}/pics/gbo25150.gif" alt = "Imagen Tres Puntos" id = "gbo25150"
							onmouseover = "cambio(this.id)" 
							onmouseout = "regresaImagen(this.id)">
					</a>
		         <img id = "gbo25160" src = "${pageContext.servletContext.contextPath}/pics/gbo25160.gif" alt = "Centro de Mensajes">
		         <img id = "glo25020" src = "${pageContext.servletContext.contextPath}/pics/glo25020.gif" alt = "Barra Roja">
		    </div>
		    <div id = "medio">
		    	<table>
		    		<tr >
		    		<td height="48px" width="355px">
		    			<img id = "gti25020" src = "${pageContext.servletContext.contextPath}/pics/gti25020.gif" alt = "Imagen Animada Bienvenidos" >
		    		</td>
		    		<td>
		    			<span id = "fecha"></span>
						<span id = "hora"></span>
		    		</td>
		    		<td>
		    			<img id = "gba25010" src = "${pageContext.servletContext.contextPath}/pics/gba25010.gif" alt = "Logotipo Sitio Seguro">
		    		</td>
		    		</tr>
		    	</table>
		    </div>
		</div>
	</body>
</html>