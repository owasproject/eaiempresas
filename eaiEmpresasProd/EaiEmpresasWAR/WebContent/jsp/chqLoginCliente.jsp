<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>



<jsp:include page="../js/rsa/setDevicePrint.jsp" flush="true" />

<spring:message code="comun.limpiar" var="limpiar"/>
<spring:message code="comun.enviar" var="enviar"/>
<spring:message code="comun.bienvenido" var="bienvenido"/>
<spring:message code="comun.login.codCliente" var="codCliente"/>
<spring:message code="comun.admon.token" var="admonToken"/>
<spring:message code="comun.admon.contrasena" var="admonContrasena"/>
<spring:message code="login.ERR000.mensaje" var="ERR000"/>
<spring:message code="login.ERR001.mensaje" var="ERR001"/>
<spring:message code="comun.contrasena.contrasena" var="contrasena"/>

<html>
	<body>
		<div>
			<jsp:include page="headerChqDig.jsp" flush="true" />
			<script src="${pageContext.servletContext.contextPath}/js/private/login/chqLoginCliente.js" type="text/javascript"></script>
		</div>
        <div id="cuerpo">
            <img alt="" id="e1gif" src= "${pageContext.servletContext.contextPath}/picsChq/e1.gif"/><img alt="" id="e2gif" src="${pageContext.servletContext.contextPath}/picsChq/e2.gif"/><img alt="" id= "e3gif" src="${pageContext.servletContext.contextPath}/picsChq/e3.gif"/><img alt="" id="e8gif" src="${pageContext.servletContext.contextPath}/picsChq/e8.gif"/>
            <form id = "formaPrincipal" action="" method="POST" name="formaPrincipal" >
		        	<input type="hidden" id="msj" name="msj" value="${msj}">		        	
		        	<input type="hidden" id="usrAut" name="usrAut" value="">
                <fieldset>
                    <label for="username" id= "IdUsername">
                        C&oacute;digo de Cliente
                    </label>
                    <input type="password" name="username" maxlength= "8" size="20" autocomplete="off" id= "IdUsernameTxt" title= "Introduzca su C&oacute;digo de Cliente"/>
                    <a href="#" title= "Administraci&oacute;n Contrase&ntilde;as" id= "administracionContra" name="administracionContra">Administraci&oacute;n de
                        Contrase&ntilde;a</a>
                    <input type="hidden" name="password"/>
                    <input type="button" name="IdSubmit" value="Entrar" id= "IdSubmit"/>
                    <input type="reset" value= "Limpiar" name="cmdCancelar" id="IdCancelar" onclick="limpiaCampos()"/>
                </fieldset>
            </form><img src="${pageContext.servletContext.contextPath}/picsChq/e8.gif" alt= "Borde Lado Derecho" id="bordeDerecho"/>
        </div>
        <div id="pie">
            <img alt="" src="${pageContext.servletContext.contextPath}/picsChq/e7.gif" id= "e7gif"/><img src="${pageContext.servletContext.contextPath}/picsChq/e2.gif" alt= "Barra de Fondo." id="barraFondo"/><img alt="" src= "${pageContext.servletContext.contextPath}/picsChq/e5.gif" id="e5gif"/>
        </div>
	</body>
</html>
<jsp:include page="../js/rsa/pmfso.jsp" flush="true" />
<c:if test="${not empty msj}">
	<c:choose>
		<c:when test="${msj eq 'ERR000'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="login.ERR000.mensaje" />', 1);
			</script>
		</c:when>
		<c:when test="${msj eq 'ERR001'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="login.ERR001.mensaje" />', 1);
			</script>
		</c:when>
		<c:when test="${msj eq 'ERRUSU'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="login.ERRUSU.mensaje" />', 1);
			</script>
		</c:when>
	</c:choose>
</c:if>