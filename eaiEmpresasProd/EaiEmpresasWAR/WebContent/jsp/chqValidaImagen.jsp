<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../js/rsa/pmfso_set.jsp" flush="true" />
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>
<jsp:include page="../js/rsa/setDevicePrint.jsp" flush="true" />
<spring:message code="comun.limpiar" var="limpiar"/>
<spring:message code="comun.enviar" var="enviar"/>
<spring:message code="comun.bienvenido" var="bienvenido"/>
<spring:message code="comun.imagen.verifique" var="verifique"/>
<spring:message code="comun.imagen.imagenC" var="imagenC"/>
<spring:message code="comun.imagen.cancelar" var="cancelar"/>
<html>
	<body>
		<div>
			<jsp:include page="headerChqDig.jsp" flush="true" />
			<script type="text/javascript">
  			InitializeTimer(1200);
  			</script>
			<script src="${pageContext.servletContext.contextPath}/js/private/imagen/chqImagen.js" type="text/javascript"></script>
		</div>
		<div id = "cuerpo">
		   	<img id = "e1gif" src = "${pageContext.servletContext.contextPath}/pics/e1.gif" /><img id = "e2gif" src = "${pageContext.servletContext.contextPath}/pics/e2.gif"/><img id = "e3gif" src = "${pageContext.servletContext.contextPath}/pics/e3.gif" />
			<img id = "e8gif" src = "${pageContext.servletContext.contextPath}/pics/e8.gif" />
		        <form id = "formaPrincipal" action="" method="POST" name="formaPrincipal" >
		        <input type="hidden" id="pasoImg" name="pasoImg" value="">
					<fieldset>
						<legend id = "identificador">
						</legend>
						<br />
						<table width="100%">
							<tr>
								<td width="33%">
									<label id = "IDImagenNota" style="font-size: 11px; font-family: serif; vertical-align: text-top;">
										<b>${verifique }</b>
									</label>
								</td>
							    <td width="33%">
									<div align="center">
										<img id="IDImagen" name="IDImagen" src="${pageContext.servletContext.contextPath}/chqGeneraImagen.do"  id="${imagen.idImagen}" alt="Red dot" align="middle"/>
									</div>
								</td>
								<td width="33%">
								</td>
							</tr>
						</table>
						<input type = "button"" name = "IdImagenCorrecta" value = "${imagenC }" id = "IdImagenCorrecta" style="top: 15px" ondblclick="javascript:this.disabled=true;"/>
						<input type = "button"  value = "${cancelar }" name = "IdCancelarEAI" id = "IdCancelarEAI" style="top: 15px" />
						<input type = "hidden" name = "password" />
		            </fieldset>
		        </form>
			<img src = "${pageContext.servletContext.contextPath}/pics/e8.gif" alt = "Borde Lado Derecho" id = "bordeDerecho" /> 
		</div>
		<div id = "pie">
		    <img src = "${pageContext.servletContext.contextPath}/pics/e7.gif" id = "e7gif"/><img src = "${pageContext.servletContext.contextPath}/pics/e2.gif" alt = "Barra de Fondo." id = "barraFondo"/><img src = "${pageContext.servletContext.contextPath}/pics/e5.gif" id = "e5gif" />
		</div>
	</body>
</html>