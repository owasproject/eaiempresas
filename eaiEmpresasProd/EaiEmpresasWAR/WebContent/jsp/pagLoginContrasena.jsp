<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="../js/rsa/pmfso_set.jsp" flush="true" />

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags"    prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt"%>

<jsp:include page="../js/rsa/setDevicePrint.jsp" flush="true" />

<spring:message code="comun.limpiar" var="limpiar"/>
<spring:message code="comun.enviar" var="enviar"/>
<spring:message code="comun.bienvenido" var="bienvenido"/>
<spring:message code="comun.login.codCliente" var="codCliente"/>
<spring:message code="comun.contrasena.contrasena" var="contrasena"/>
<spring:message code="comun.contrasena.contrasena" var="contrasena"/>
<spring:message code="contrasena.ERR000.mensaje"  var = "CERR000"/>
<spring:message code="contrasena.ERR001.mensaje"      var = "CERR001"/>
<spring:message code="contrasena.ERR002.mensaje"      var = "CERR002"/>
<spring:message code="contrasena.ERR003.mensaje"      var = "CERR003"/>

<html>
	<body>
		<div>
			<jsp:include page="header.jsp" flush="true">
				<jsp:param name="trusteer" value="true"/>
				<jsp:param name="contrasena" value="true"/>
			</jsp:include>
			<script type="text/javascript">
  			InitializeTimer(1200);
  			</script>
			<script src="${pageContext.servletContext.contextPath}/js/private/login/pagContrasena.js" type="text/javascript"></script>
		    
		    <div id = "abajo">
		        <img id = "gti25030" src = "${pageContext.servletContext.contextPath}/pics/gti25030.gif" alt = "Imagen Animada Enlace">
					<a href="javascript:FrameAyuda('s26050h');" name="Ayuda" id="gbo25170img" style="border: none;"> 
						<img id="gbo25170" src="${pageContext.servletContext.contextPath}/pics/gbo25170.gif"
							style="border: none;"></img>
					 </a>
					<a href = "#" alt = "Login Enlace" id = "finalizar" name="finalizar" style="border: none;">
						<img id = "gbo251801" src = "${pageContext.servletContext.contextPath}/pics/gbo25180.gif" style="border: none;"></img>
					</a>
		    </div>
		</div>
		<div id = "cuerpo">
		   	<img id = "e1gif" src = "${pageContext.servletContext.contextPath}/pics/e1.gif" /><img id = "e2gif" src = "${pageContext.servletContext.contextPath}/pics/e2.gif"/><img id = "e3gif" src = "${pageContext.servletContext.contextPath}/pics/e3.gif" />
			<img id = "e8gif" src = "${pageContext.servletContext.contextPath}/pics/e8.gif" />
		        <form id = "formaPrincipal" method="post" name="formaPrincipal" onsubmit="return mandarDatosIE6(); document.IdSubmit.disabled = true; return true;" action="validarContrasena.do">
					<fieldset>
						<legend id = "identificador">
							${bienvenido }
						</legend>
						<label for = "password" id = "IdPassword">
							${contrasena }  
						</label>
						<input type = "hidden" name = "codigoCliente" id = "codigoCliente"  />
						<input type = "password" name = "txtPassword"  maxlength =  "20" size = "20" autocomplete = "off" id = "IdPasswordTxt" />
						<br />
						<input type = "hidden" id = "login-form-type" name = "login-form-type" value = "pwd" />
						
						<br />
						<input type = "button" name = "IdSubmit" value = "${enviar }" id = "IdSubmit" onclick="javascript:this.disabled=true;"/>
						<input type = "button"  value = "${limpiar }" name = "IdCancelar" id = "IdCancelar"/>
						<input type = "hidden" name = "password" />
		            </fieldset>
		        </form>
			<img src = "${pageContext.servletContext.contextPath}/pics/e8.gif" alt = "Borde Lado Derecho" id = "bordeDerecho" /> 
		</div>
		<div id = "pie">
		    <img src = "${pageContext.servletContext.contextPath}/pics/e7.gif" id = "e7gif"/><img src = "${pageContext.servletContext.contextPath}/pics/e2.gif" alt = "Barra de Fondo." id = "barraFondo"/><img src = "${pageContext.servletContext.contextPath}/pics/e5.gif" id = "e5gif" />
		</div>
	</body>
	<script type="text/javascript">
	        var mensajes = [];
	
	        mensajes['CERR000'] = '${CERR000}';
	        mensajes['CERR001'] = '${CERR001}';
	        mensajes['CERR002'] = '${CERR002}';
	        mensajes['CERR003'] = '${CERR003}';
	        
	</script>
</html>
<c:if test="${not empty msj}">
	<c:choose>
		<c:when test="${msj eq 'CERR001'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.ERR001.mensaje" />', 1);
			</script>
		</c:when>
		<c:when test="${msj eq 'CERR002'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.ERR002.mensaje" />', 99);
			</script>
		</c:when>
		<c:when test="${msj eq 'CERR003'}" >
			<script type="text/javascript">
				cuadroDialogo('<spring:message code="contrasena.ERR003.mensaje" />', 99);
			</script>
		</c:when>
	</c:choose>
</c:if>
