CDCONEXION = {
  regexNumerico : /[0-9]{8}/,
  codCliente : "",
  codError : "",
  palabProhi : new Array("santander", "altec", "serfin", "banca", "electronica", "enlace", "banco", "supernet", "empresas"),
  inicio : function () {
    var divCuerpo = document.getElementById("cuerpo");
    var spanError = document.getElementById("spnError");
    try {
       divCuerpo.removeChild(spanError);
    }catch(e) {
    }
    var cuerpo = document.getElementsByTagName("body")[0];
    if(cuerpo.getAttribute("id") == "conexion" ) {
    MENSAJE.setCuerpoStr("conexion");
    var formulario = document.getElementById("formulario");
    formulario.onsubmit = function () {
      return CDCONEXION.validacion();
    };
       if(CDCONEXION.codError != "") {
          MENSAJE.creaMensaje(CDCONEXION.getDescripcionError(CDCONEXION.codError));
       } 
    }else if(cuerpo.getAttribute("id") == "pwdVol") {
      MENSAJE.setCuerpoStr("pwdVol");
      if(CDCONEXION.codCliente != "") {
        var texto = document.createTextNode(CDCONEXION.codCliente);
        var barra = document.createTextNode("| ");
        var spanUsuario = document.getElementById("spnUsuario");
        try {
          spanUsuario.appendChild(barra);
          spanUsuario.appendChild(texto);
        }catch(e) {
        }
      }
      var formulario = document.getElementById("formulario");
      formulario.onsubmit = function () {
      return CDCONEXION.cambioContrasenia();
      };
    }
  },
  validacion : function () {
    var regreso = false;
    var forma = document.forms[0];
    var usuario = forma.username.value;
    var pass = forma.password.value;
    var msgAdv = "";
    if(usuario != "" && pass != "") {
      usuario = CDCONEXION.rellenaCeros(usuario);
      if(CDCONEXION.esNumero(usuario)) {
         regreso = true;
         forma.username.value = usuario;
      }else {
        MENSAJE.creaMensaje("El C\u00f3digo de Cliente de ser Num\u00e9rico",true);
      }
    }else{
        if(usuario == "") {
          if(pass == "") {
            msgAdv = "Debe Introducir Usuario y Contrase\u00f1a.";
          }else {
            msgAdv = "Debe Introducir Usuario.";
          }
        }else { 
          msgAdv = "Debe Introducir Contrase\u00f1a.";
        }
    }
    if(msgAdv != ""){
        MENSAJE.creaMensaje(msgAdv);
    }
    return regreso;
  },
  setCodigoCliente : function (cod) {
    CDCONEXION.codCliente = cod;
  },
  cambioContrasenia : function () {
    var regreso = false;
    var forma = document.forms[0];
    var anterior = forma.old.value;
    var nueva = forma.new1.value;
    var nvRep = forma.new2.value;
    if(nueva.length < 8 ) {
      MENSAJE.creaMensaje("La Contrase\u00f1a no cumple con la longitud m\u00ednima", true);
    }else if(nueva == anterior) {
       MENSAJE.creaMensaje("Su contrase\u00f1a nueva y la anterior no deben ser iguales", true);
    }else if(anterior == nvRep){
      MENSAJE.creaMensaje("Su contrase\u00f1a nueva y la anterior no deben ser iguales", true);
    }else if(!CDCONEXION.reglasCNBV(nueva)) {
      MENSAJE.creaMensaje("La contrase\u00f1a no cumple con la normatividad", true);
    }else if(! this.validarPalabras(nueva)) {
      MENSAJE.creaMensaje("La contrase\u00f1a contiene caracteres no permitidos.", true);
    }else {
      regreso = true;
    }
    
    return regreso;
  },
  reglasCNBV : function (pass) {
    var valida = true;
    if(this.limpia(pass) != ""){
    for(i = 0; i < pass.length; i++){
      var carac0 = pass.substring(i, i+1);
      var carac1 = pass.substring(i+1, i+2);
      var carac2 = pass.substring(i+2, i+3);
      if(carac0 == carac1) {
        if(carac0 == carac2) {
          valida = false;
          break;
        }
      }
      var caraCode0 =  carac0.charCodeAt();
      var caraCode00 =  carac0.charCodeAt();
      var caraCode1 =  carac1.charCodeAt();
      var caraCode11 =  carac1.charCodeAt();
      var caraCode2 =  carac2.charCodeAt();
      var caraCode22 =  carac2.charCodeAt();
      if(++caraCode0  == caraCode1) {
        if(++caraCode0 == caraCode2) {
          valida = false;
          break;
        }
      }
      if(--caraCode00 == caraCode11) {
        if(--caraCode00 == caraCode22) {
          valida = false;
          break;
        }
      }
    }
    if(!CDCONEXION.validaAlfa(pass)) {
      valida  = false;
    }
    }
            return valida;
  },
  validaAlfa : function (campo) {
    var re = /^\w*(?=\w*\d)(?=\w*[a-zA-Z])\w*$/;
    return re.test(campo);
  },
  limpia : function (valor){
	return valor.replace(/^s+|s+$/g, '');
  },
  validarPalabras : function (palabra) {
        var valida = true;
        var passwdMin = palabra.toLowerCase();
        for(i = 0; i < this.palabProhi.length; i++) {
                if(passwdMin.indexOf(this.palabProhi[i]) != -1 || passwdMin.indexOf(this.codCliente) != -1) {
                        valida = false;
                        break;
                }
        }
        return valida;
  }, 
  limpiarContrasenias : function (){
    var forma = document.forms[0];
    for( var j = 0; j < forma.length; j++) {
       if(forma[j].type == "text" || forma[j].type == "password" ) {
          forma[j].value = "";
       }
    }
  },
  setCodigoError : function (err) {
     this.codError = err;
  },
  getDescripcionError : function (err) {
     var descrError = "";
     if(err.indexOf("HPDIA0200W") != -1) {
        descrError = "Usted esta utilizando un C\u00f3digo de Cliente Inv\u00e1lido, contrase\u00f1a o Certificado de Cliente";
     }
     return descrError;
  },
  esNumero : function (num) {
    return this.regexNumerico.test(num);
  },
  rellenaCeros : function (cam) {
     var campoStr = "";
     campoStr = cam;
     for(var i = 0; i < (8 - cam.length); i++) {
         campoStr = 0 + campoStr;
     }
     return campoStr;
  }

}
if(document.getElementById) {
  window.onload = CDCONEXION.inicio;
}
