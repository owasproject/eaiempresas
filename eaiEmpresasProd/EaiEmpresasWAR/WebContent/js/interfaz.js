Interfaz = {
    verificarInstalacion: function(){
	    regreso = "0";
	    try {
		if(mkd25x_installed()) {
			regreso = "1";
		}
	}catch(e) {}
        return regreso;
    },
    verificarInstalacionDos: function(){
        regreso = "0";
        try {
            if(aos_isinstalled('ea') == false) {
		    regreso = "0";
	    }else {
		    regreso = "1";
	    }
        } 
        catch (e) {
            //No se realiza ninguna accion
        }
        return regreso;
    },
    activar: function(){
        try {
            if (aos_is_new()) {
                aos_set_auth_server('original.cddmex.com');
                aos_set_subclsid("40", "59B0298B-A7B5-4045-A34E-377EDF7BCB8E");
                aos_set_submimetype("40", "application/ahnlab/asp/npmkd25aos");
                aos_set_authinfo('aosp_cdd_santander.html');
                aos_set_option("mkd_protect_level", "high");
                aos_set_option("uimode", true);
                aos_set_option("asyncmode", false);
                aos_write_object();
                aos_isrunning('ea');
                //Modificación debido a que se tenia un error.
                //aos_isruning('ea');
            }
            else {
                //alert ("aos no new");
            }
        } 
        catch (e) {
            //alert(e.message);
        }
    },
    copiarForma: function(forma){
        if (mkd25x_installed()) {
            if (mkd25x_loaded()) {
                mkd25x_copy_to_form(forma);
            }
        }
    },
    saltoUno: function(){
        if (document.getElementById("MKD25X")) {
            document.getElementById("MKD25X").SkipVerify(1);
        }
    },
    saltoCero: function(){
        if (document.getElementById("MKD25X")) {
            document.getElementById("MKD25X").SkipVerify(0);
        }
    }
};
