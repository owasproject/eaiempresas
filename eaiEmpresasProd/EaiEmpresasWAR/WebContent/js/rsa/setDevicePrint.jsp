<%
   response.setHeader( "Pragma", "no-cache" );
   response.setHeader( "Cache-Control", "no-cache" );
   response.setDateHeader( "Expires", 0 );
   response.addHeader("X-FRAME-OPTIONS", "SAMEORIGIN");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script language="javascript" src="<%=request.getContextPath()%>/js/rsa/hashtable.js"></script>
<script language="javascript" src="<%=request.getContextPath()%>/js/rsa/rsa.js"></script>
</head>
<body>

<script>
	var devicePrint = "";
	
	try {
		if(typeof(encode_deviceprint) != 'undefined' ){
			devicePrint = encode_deviceprint();
		}
	}catch(e){ }
	
	function mandaDevice()
	{
		if( window.XMLHttpRequest )
			ajax = new XMLHttpRequest(); // No Internet Explorer
		else
			ajax = new ActiveXObject("Microsoft.XMLHTTP"); // Internet Explorer
		ajax.open( "GET", "./js/rsa/getDevicePrint.jsp?devicePrint=" + devicePrint, true );
		ajax.send("");
	}
	mandaDevice();
</script>
</body>
</html>