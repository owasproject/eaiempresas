function presentaHoraFecha(){
    var fechaActual = new Date();
    var fechaSpan = document.getElementById("fecha");
    var horaSpan = document.getElementById("hora");
    var textoHora = "";
    var textoFecha = "";
    if (fechaActual.getMinutes() < 10) {
        textoHora = fechaActual.getHours() + ":0" + fechaActual.getMinutes();
    }
    else {
        textoHora = fechaActual.getHours() + ":" + fechaActual.getMinutes();
    }
    try {
        textoFecha = construirFecha();
        if (document.createTextNode) {
            var txtHora = document.createTextNode(textoHora);
            horaSpan.appendChild(txtHora);
            var txtFecha = document.createTextNode(textoFecha);
            fechaSpan.appendChild(txtFecha);
        }
        else {
            horaSpan.innerHTML = textoHora;
            fechaSpan.innerHTML = textoFecha;
        }
    } 
    catch (e) {
        alert(e.message);
    }
}

function construirFecha(){
    var dias = new Array("Domingo", "Lunes", "Martes", "Mi\u00e9rcoles", "Jueves", "Viernes", "S\u00e1bado");
    var meses = new Array("enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre");
    var fecha = new Date();
    var dia = fecha.getDay();
    var mes = fecha.getMonth();
    var fechaStr = "";
    fechaStr = dias[dia] + " " + fecha.getDate() + " de " + meses[mes] + " del " +
    fecha.getFullYear();
    return fechaStr;
}

function validacionDatos(){
    var forma = document.forms[0];
    var error = false;
    var regreso = false;
    if (limpia(forma.username.value) != "") {
        var usrw = forma.username.value;
        var usuarioStr = "";
        
        if (forma.username.value.length > 8){
        return false;
        }

        if (forma.username.value.length == 8) {
            usuarioStr = regresaUsuario(usrw);
        }
        else {
            forma.username.value = cerosIzquierda(forma.username.value);
            usuarioStr = cerosIzquierda2(usrw);
        }
        
        if (!isInteger(forma.username.value)) {
            cuadroDialogo("El C&oacute;digo de Cliente Debe Ser Num&eacute;rico.", 1);
            error = true;
        }
        else {
            regreso = true;
        }
    }
    else {
    
        cuadroDialogo("Debe introducir su C&oacute;digo de cliente.", 1);
    }
    return regreso;
}

function validacionDatosPass(){
    var forma = document.forms[0];
    var error = false;
    var regreso = false;
    if (limpia(forma.txtPassword.value) != "" && limpia(forma.username.value) != "") {
        var usrw = forma.username.value;
        var usuarioStr = "";
        
        if (forma.username.value.length > 8){
        return false;
        }

        if (forma.username.value.length == 8) {
            usuarioStr = regresaUsuario(usrw);
        }
        else {
            forma.username.value = cerosIzquierda(forma.username.value);
            usuarioStr = cerosIzquierda2(usrw);
        }
        
        if (!isInteger(forma.username.value)) {
            cuadroDialogo("El C&oacute;digo de Cliente Debe Ser Num&eacute;rico.", 1);
            error = true;
        }
        else {
            forma.password.value = usuarioStr + forma.txtPassword.value;
            regreso = true;
        }
    }
    else {
    
        cuadroDialogo("Debe introducir su C&oacute;digo de cliente  y Contrase&ntilde;a ", 1);
    }
    return regreso;
}

function limpia(valor){
    return valor.replace(/^s+|s+$/g, '');
}

function isInteger(s){
    var i
    
    for (i = 0; i < s.length; i++) {
        var c = s.charAt(i)
        if (!isDigit(c)) 
            return false
    }
    return true
}

function isDigit(c){
    return ((c >= "0") && (c <= "9"))
}

function cerosIzquierda(codClient){
    var cad = "";
    if (codClient.length <= 7) {
        limiteSuperior = 8 - codClient.length;
        for (i = 0; i < limiteSuperior; i++) {
            cad += "0";
        }
    }
    return cad + codClient;
}

function cerosIzquierda2(codClient){
    var cad = "";
    if (codClient.length <= 6) {
        limiteSuperior = 7 - codClient.length;
        for (i = 0; i < limiteSuperior; i++) {
            cad += "0";
        }
    }
    return cad + codClient;
}


function regresaUsuario(usuario){
    var longitud = usuario.length;
    var usua = "";
    if (longitud == 8) {
        var identificado = usuario.substring(0, 2);
        var user = usuario.substring(2, 8);
        var entero = parseInt(identificado, 10);

        if(entero >= 30)
                return usuario;

        switch (entero) {
            case 10:
                usuario = 'A' + user;
                break;
            case 11:
                usuario = 'B' + user;
                break;
            case 12:
                usuario = 'G' + user;
                break;
            case 13:
                usuario = 'H' + user;
                break;
            case 14:
                usuario = 'I' + user;
                break;
            case 15:
                usuario = 'J' + user;
                break;
            case 16:
                usuario = 'K' + user;
                break;
            case 17:
                usuario = 'L' + user;
                break;
            case 18:
                usuario = 'M' + user;
                break;
            case 19:
                usuario = 'N' + user;
                break;
            case 20:
                usuario = 'O' + user;
                break;
            case 21:
                usuario = 'P' + user;
                break;
            case 22:
                usuario = 'Q' + user;
                break;
            case 23:
                usuario = 'R' + user;
                break;
            case 24:
                usuario = 'T' + user;
                break;
            case 25:
                usuario = 'U' + user;
                break;
            case 26:
                usuario = 'V' + user;
                break;
            case 27:
                usuario = 'W' + user;
                break;
            case 28:
                usuario = 'X' + user;
                break;
            case 29:
                usuario = 'Y' + user;
                break;
            case 0:
                usuario = usuario.substring(1, 8);
                break;
            default:
                usuario = usuario.substring(1, 8);
                break;
        }
    }
    else {
        usua = usuario;
    }
    return usuario;
}

