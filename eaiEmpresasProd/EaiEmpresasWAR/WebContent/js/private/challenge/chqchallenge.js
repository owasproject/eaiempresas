/**
 * Isban Mexico
 *   Clase: challenge.js
 *   Descripci�n: Archivo JS para la pantalla de Challenge.
 *
 *   Control de Cambios:
 *   1.0 Nov 16, 2012 bvb - Creacion
 */
var ID_FORMA = "formaPrincipal";
var ID_VALIDA_RESPUESTA = "chqvalidarRespuesta.do";
var URL_LOGOUT = "chqUsrLogout.do";

//Funcion ready() de jQuery
$(function() {
	$("#IdSubmit").click(function() {
		$('#IdSubmit').attr('disabled', true);
		validaRespuesta();
	});
	
	$("#radio1").click(function() {
		valorRadio();
	});
	
	$("#radio2").click(function() {
		valorRadio();
	});
	  
	$("#finalizar").click(function(){
		ir_a(ID_FORMA, URL_LOGOUT);
	});
	
	$("form").keypress(function(e) {
		  if (e.which == 13) {
			  $('#IdSubmit').attr('disabled', true);
			  validaRespuesta();	    
			  return false;
		  }
	});
	
	$("#respuesta").keyup(function(){
		cambiarValor();
	});
	
});

/**
 * Funci�n que valida el campo respuesta
 */
function validaRespuesta(){
	
	if( $("#respuesta").val() == "") {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR001'], 1);
	}else{
		if($("#tipoDisp").val() == ""){
			$('#IdSubmit').attr('disabled', false);
			cuadroDialogo(mensajes['ERR002'], 1);
		}else{
			ir_a(ID_FORMA, ID_VALIDA_RESPUESTA);
		}
	}
}

function valorRadio(){
	
	 if($("#radio1").is(':checked')) {
		 var valor = $("#radio1").val();
		 $("#tipoDisp").val(valor);
	 }
	 if($("#radio2").is(':checked')) {
		 var valor = $("#radio2").val();
		 $("#tipoDisp").val(valor);
	 }
	
}

function cambiarValor(){
	
	 eliminarCaracteresInvalidos("#respuesta");
	 var campo = $("#respuesta").val();
	 campo = campo.toUpperCase();
	 $("#respuesta").val(campo);
	 	
}

function salirSesion (){
	
	ir_a(ID_FORMA, URL_LOGOUT);
}