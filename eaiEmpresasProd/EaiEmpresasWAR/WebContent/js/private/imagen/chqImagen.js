/**
 * Isban Mexico
 *   Clase: imagen.js
 *   Descripción: Archivo JS para la pantalla de Imagen.
 *
 *   Control de Cambios:
 *   1.0 Dic 13, 2012 bvb - Creacion
 */
var ID_FORMA = "formaPrincipal";
var URL_VALIDO_IMAGEN = "chqinicioContrasena.do";
var URL_IMAGEN_INCORRECTA = "chqUsrLogout.do";

//Funcion ready() de jQuery
$(function() {
	
	$("#IdImagenCorrecta").click(function() {
		$('#IdImagenCorrecta').attr('disabled', true);
		$("#pasoImg").val("true");
		ir_a(ID_FORMA, URL_VALIDO_IMAGEN);
	});
	
	$("#IdCancelarEAI").click(function() {
		salir();
	});
	
	$("form").keypress(function(e) {
		  if (e.which == 13) {
		    return false;		    
		  }
	});
	
});

function salir(){
	ir_a(ID_FORMA, URL_IMAGEN_INCORRECTA);
} 