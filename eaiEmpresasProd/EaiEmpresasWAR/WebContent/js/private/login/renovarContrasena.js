/**
 * Isban Mexico
 *   Clase: renovarContrasena.js
 *   Descripci�n: Archivo JS para la pantalla de Renovaci�n de contrase�a.
 *
 *   Control de Cambios:
 *   1.0 Nov 16, 2012 bvb - Creacion
 */
var ID_FORMA = "formaPrincipal";
var ID_ACTUALIZAR_CONTRASENA = "actualizarContrasena.do";
var ID_REGRESAR = "usrLogout.do";

//Funcion ready() de jQuery
$(function() {
	$("#IdSubmit").click(function() {
		validaRespuestaRenovacion();
	});
	
	$("#IdRegresar").click(function() {
		var querystring;
		try{
			querystring =  window.location.search.substring(1);
		}catch(e){
			querystring = "";
		}
		var vieneEnlace = $('#enlace').val();
		if((querystring.indexOf("RCSaldos=1") != -1) || (vieneEnlace.indexOf("RCSaldos=1") != -1)) {
			var ID_REGRESAR_ENLACE = $("#urlRegreso").val(); 
			window.location = ID_REGRESAR_ENLACE;
		}else{
			window.location = ID_REGRESAR;
		}
	});
	
	var msj = $("#msj").val(); 
	if(msj != null && msj != ''){
		if(msj == 'ERR004'){
			cuadroDialogo(mensajes['ERR004'], 1);
		}else{//Modificacion RFC100001010145
			if(msj == 'ERR005'){
				cuadroDialogo(mensajes['ERR005'], 1);
			}else if(msj == 'ERR006'){
				cuadroDialogo(mensajes['ERR006'], 1);
			}
			else{
				if(msj == 'INF001'){
					//cuadroDialogo(mensajes['INF001'], 99);
					creaDialogo();
				}else if(msj == 'HPDAA0341E' || msj == 'HPDIA0300W'){
					cuadroDialogo(mensajes['ERR009'], 1);
				}else if(msj == 'HPDAA0329E'){
					cuadroDialogo(mensajes['ERR010'], 1);
				}else{
					
					
					cuadroDialogo(msj, 1);
				}
			}
		}
	}
	
	$("#noinstalado").css("display", "none");
	
	$("form").keypress(function(e) {
		  if (e.which == 13) {
			  $('#IdSubmit').attr('disabled', true);
			  validaRespuestaRenovacion();
			  return false;
		  }
	});
	
});

$(document).ready(function() {
	$("#noinstalado").css("display", "none");
	});

function ocultar(){
	$("#noinstalado").css("display", "none");
}

/**
 * Funci�n que valida el campo respuesta
 */
function validaRespuestaRenovacion(){
	//CSA RFC100001010145 - Validacion de politicas de contrase�a
	
	if( $("#txtPasswordAnt").val() == "") {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR000'], 1);
	}else if( $("#txtPasswordNvo").val() == "") {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR001'], 1);
	}else if( $("#txtPasswordConf").val() == "") {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR002'], 1);
	}else if( $("#txtPasswordConf").val() != $("#txtPasswordNvo").val()) {
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR003'], 1);
	}else 	if($("#txtPasswordNvo").val().length < 8){
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR007'], 1);
		return false;
	}else if($("#txtPasswordNvo").val().length > 20){
		$('#IdSubmit').attr('disabled', false);
		cuadroDialogo(mensajes['ERR008'], 1);
		return false;
	}else 
		{
		var querystring2;
		try{
			querystring2 =  window.location.search.substring(1);
		}catch(e){
			querystring2 = "";
		}
		if((querystring2.indexOf("RCSaldos=1") != -1)) {
			$('#enlace').val("RCSaldos=1");
		}
		ir_a(ID_FORMA, ID_ACTUALIZAR_CONTRASENA);		
	}
}
function validaRespuestaRenovacionIE6(){
	//CSA RFC100001010145 - Validacion de politicas de contrase�a
	if( $("#txtPasswordAnt").val() == "") {
		cuadroDialogo(mensajes['ERR000'], 1);
	}else if( $("#txtPasswordNvo").val() == "") {
		cuadroDialogo(mensajes['ERR001'], 1);
	}else if( $("#txtPasswordConf").val() == "") {
		cuadroDialogo(mensajes['ERR002'], 1);
	}else if( $("#txtPasswordConf").val() != $("#txtPasswordNvo").val()) {
		cuadroDialogo(mensajes['ERR005'], 1);
	}else{
			return true;
		
	}
	return false;
}
var subCliente = "";
var valor = "";

function cerrar(){
	ir_a(ID_FORMA, "usrLogout.do");
}
var timer;
function checar() {
	timer = setInterval("validaPopUP()", 1000); 
	return;
}

function validaPopUP() {
	if(activar) {
		if(vent.closed) {
			clearInterval(timer);
			ir_a(ID_FORMA, ID_REGRESAR);
		}
	}
}

function creaDialogo() {
	activar = true;
	vent = cuadroDialogo(mensajes['INF001'],1);
	checar();
}	