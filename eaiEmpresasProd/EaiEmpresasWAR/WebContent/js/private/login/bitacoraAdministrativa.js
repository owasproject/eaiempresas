/**
 * Isban Mexico
 *   Clase: bitacoraAdministrativa.js
 *   Descripción: Archivo JS para la pantalla de bitcora Administrativa.
 *
 *   Control de Cambios:
 *   1.0 Mar 15, 2012 oacosta - Creacion
 */
var ID_FORMA = "frmBitaAdmin";
var URL_INICIO = "visBitaAdmin.do";
var URL_CONSULTA = "conBitaAdmin.do";
var URL_EXPORTA = "expBitaAdmin.do";

//Funcion ready() de jQuery
$(function() {
	//Se indican los datos del paginador.
	Paginador.registraPaginador(ID_FORMA, URL_CONSULTA, 'pagBitaAdm');

	//Se indican los datos del exportador
	Exportador.registrarExportador('btnExportar', URL_EXPORTA, ID_FORMA,
			function () {
				return validarConsulta();
	});

	//Se agrega el comportmiento a los calendarios.
	createCalendar("txtFechaInicio", "cal1");
	createCalendar("txtFechaFin", "cal2");

	//Se agrega el comportamiento a los botones
	$("#btnLimpiar").click(function() {
		limpiarBitaAdmin();
	});
	$("#btnBuscar").click(function() {
		consultarBitacora();
	});

});

/**
 * Valida que la consulta 
 **/
function validarConsulta() {

	/*if (!validarFechaMayor($("#txtFechaInicio").val(),
			$("#txtFechaFin").val())) {
		jError(mensajes['ERR008sugerencia'],
				mensajes['ERR008titulo'],
				mensajes['ERR008codigo'],
				mensajes['ERR008observacion']);
		return false;
	}*/

	return true;
}

/**
 * Realiza la consulta de la bitacora con los datos capturados.
 **/
function consultarBitacora() {
	if (validarConsulta()) {
		ir_a(ID_FORMA, URL_CONSULTA);
	}
}

/**
 * Limpia los elementos de la forma.
 **/
function limpiarBitaAdmin() {
	limpiar(["txtFechaInicio", "txtFechaFin", "txtUsuario"]);
}
