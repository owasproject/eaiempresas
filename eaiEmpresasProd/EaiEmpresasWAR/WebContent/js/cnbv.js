
function reglasCNBV(pass) {
            var valida = true;
            if(limpia(pass) != ""){
               for(i = 0; i < pass.length; i++){
                  var carac0 = pass.substring(i, i+1);
                  var carac1 = pass.substring(i+1, i+2);
                  var carac2 = pass.substring(i+2, i+3);
                  if(carac0 == carac1) {
                     if(carac0 == carac2) {
                        valida = false;
                        break;
                     }
                  }
                  var caraCode0 =  carac0.charCodeAt();
                  var caraCode00 =  carac0.charCodeAt();
                  var caraCode1 =  carac1.charCodeAt();
                  var caraCode11 =  carac1.charCodeAt();
                  var caraCode2 =  carac2.charCodeAt();
                  var caraCode22 =  carac2.charCodeAt();
                  if(++caraCode0  == caraCode1) {
                     if(++caraCode0 == caraCode2) {
                        valida = false;
                        break;
                     }
                  }
                  if(--caraCode00 == caraCode11) {
                          if(--caraCode00 == caraCode22) {
                                  valida = false;
                                  break;
                          }
                 }
              }
              if(!validaAlfa(pass)) {
                valida  = false;
              }
            }
            return valida;
         }




function  validaAlfa(campo) {
  var valido = false
  var mayus = false;
  var minus = false;
  var num = false;
  var carNPer = false;
  var longitud = campo.length;
  for(var i = 0; i < longitud; i++) {
    var caracter = campo.charAt(i);
    if(('A'<= caracter && caracter <= 'Z') || ('a' <= caracter && caracter <= 'z') ) {
      mayus = true;
    }else if('0' <= caracter && caracter <= '9') {
      num = true;
    }else {
      carNPer = true;
    }
    if(mayus && num && !carNPer) {
      valido = true;
    }
  }
  return valido;
}
