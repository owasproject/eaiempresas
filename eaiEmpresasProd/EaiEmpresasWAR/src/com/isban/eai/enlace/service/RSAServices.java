package com.isban.eai.enlace.service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isban.eai.enlace.cookie.LongLivedCookie;

public final class RSAServices {
	
	/**
	 * constructor
	 */
	private RSAServices(){
		
	}
	
	/**
	 * @param response : response
	 * @param valor : valor
	 */
	public static void crearCookie(HttpServletResponse response, String valor) {

		final Cookie cookieToken = new LongLivedCookie(valor);
		response.addCookie(cookieToken);
	} 
	
	/**
	 * @param request : request
	 * @return null
	 */
	public static String getCookieValue( HttpServletRequest request) {
		final Cookie[] cookies = request.getCookies();
		for (int i = 0; cookies != null && i < cookies.length; i++) {
			final Cookie cookie = cookies[i];
			if ("PMData".equals(cookie.getName())){
				return (cookie.getValue());
			}
		}
		return null;
	}

}
