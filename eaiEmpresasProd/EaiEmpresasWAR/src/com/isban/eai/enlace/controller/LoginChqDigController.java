/**
 * Isban Mexico
 *   Clase: LoginClienteController.java
 *   Descripción: Controlador para la pantalla de login del cliente
 *
 *   Control de Cambios:
 *   1.0 Junio 20, 2013 bvb - Creacion
 */
package com.isban.eai.enlace.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.rsa.aa.ws.ChallengeQuestion;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.isban.eai.enlace.controller.comun.GenericController;
import com.isban.eai.enlace.dto.ImagenDTO;
import com.isban.eai.enlace.dto.RSADTO;
import com.isban.eai.enlace.dto.VersionRSADTO;
import com.isban.eai.enlace.service.RSAServices;
import com.isban.eai.enlace.servicio.BOLRegistraBitacoraTamSam;
import com.isban.eai.enlace.servicio.BOVersionRSA;
import com.isban.eai.enlace.servicio.RSAServiceEJB;
import com.isban.eai.enlace.util.ConstantesRSA;
import com.isban.eai.enlace.util.EnlaceConfig;
import com.isban.eai.enlace.util.UtilidadesRSA;
import com.isban.ebe.commons.exception.BusinessException;
import com.passmarksecurity.PassMarkDeviceSupportLite;

/**
 * @author bverduzco
 *
 */
@Controller
public class LoginChqDigController extends GenericController {
	
	/**LOG**/
    private static final Logger LOGGER = Logger.getLogger(LoginChqDigController.class);
    
    /**usuario**/
    private static final String USUARIO = "userName";
    
    /**usrAut**/
    private static final String USRAUT = "usrAut";
    
    /**bandera true**/
    private static final String BANDERA = "true";
    
    /**imagesVal**/
    private static final String IMAGESVAL = "imagesVal";
    
    /**msj**/
    private static final String MSJ = "msj";
    
    /**pagLoginCliente**/
    private static final String PAG_LOGIN = "chqLoginCliente";
    
    /**model and view**/
    private static ModelAndView modeloView;
    
    /**
     * "consumoRSA"
     */
    private RSAServiceEJB consumoRSA;
    
    /**
     * bitacoraTamSam
     */
    private BOLRegistraBitacoraTamSam bitacoraTamSam; 
    
    /**
     * versionRSA
     */
    private BOVersionRSA versionRSA;
    
	/**
     * @param req : HttpServletRequest
     * 
     * @param res : HttpServletResponse
     * @return ModelAndView 
     * @throws BusinessException : exception
     */
    @RequestMapping("/chqinicio.do")
    public ModelAndView mostrarPaginaLogin(HttpServletRequest req,
            HttpServletResponse res) throws BusinessException {
    	req.getSession().invalidate();
    	req.getSession(true);
    	LOGGER.info("-----------------Context path: "+req.getContextPath());
    	//
    	final String samContext = EnlaceConfig.SAM_CONTEXT;
    	final String appContext = req.getContextPath();
    	final String finalContext = samContext + appContext;
    	req.getSession().setAttribute("tamContext", EnlaceConfig.SAM_CONTEXT);
    	req.getSession().setAttribute("finalContext", finalContext);
    	LOGGER.info("-----------------Context path final: "+finalContext);
    	setContexto(finalContext);
    	//Se indica que se quiere ingresar a Cheque Digital para bitacora
    	req.getSession().setAttribute("aplicacion", "C");
    	//Consulta bandera version RSA
    	VersionRSADTO versionDto = new VersionRSADTO();
    	versionDto = versionRSA.consulta();
    	req.getSession().setAttribute("versionRSA", versionDto.getValor().toString());
    	return new ModelAndView(PAG_LOGIN);
    }
    
    
    /**
     * @param req : HttpServletRequest
     * @param res : HttpServletResponse
     * @param model : ModelMap
     * @return : ModelAndView
     * @throws BusinessException : exception
     */
    @RequestMapping("validaClienteChqinicio.do")
    public ModelAndView  validarCliente(HttpServletRequest req,
            HttpServletResponse res, ModelMap model) throws BusinessException {
    	
    	LOGGER.info(" >>> Entra a validaClienteChqinicio.do");
    	
    	final UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
	    //mapa de datos
	    Map<String, Object> resp = new HashMap<String, Object>();
	    RSADTO  rsaBean = new RSADTO();
	    final String usuario = req.getParameter("username");
	    if(usuario == null || "".equals(usuario.trim())){
	    	model.addAttribute("msj", "ERRUSU");
	    	final ModelAndView modelo = new ModelAndView(PAG_LOGIN);
	    	modelo.addAllObjects(model);
	    	return modelo;
	    }
	    
	    final String usrAut = req.getParameter(USRAUT);
	    req.getSession().setAttribute(USUARIO, usuario);
	    req.getSession().setAttribute(USRAUT, usrAut);
	    
	    rsaBean = utilidadesRSA.generaBean(req, "", "");
	    
	    String deviceTokenFSO = "";
	    String deviceTokenCookie = "";
    	
	    if(BANDERA.equals(EnlaceConfig.BANDERA_RSA)){
	    
	    	resp =  consumoRSA.ejecutaAnalyze(rsaBean);
    		
    		if(resp != null) {
    			
	    	    deviceTokenFSO = resp.get("deviceTokenFSO").toString();
	    	    
	    	    deviceTokenCookie = resp.get("deviceTokenCookie").toString();
	    	    
				    RSAServices.crearCookie(res, deviceTokenCookie);
					req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
				    
		    	model.addAttribute("codigoCliente", usuario);
		    	final int opcion = Integer.parseInt(resp.get("accion").toString());
		    	return validarOpcion(req, res, model, resp, opcion, deviceTokenFSO, deviceTokenCookie);
		    	
    		}//fin de respuesta != null
    		
	    }//fin de bandera RSA
	    req.getSession().setAttribute("vContrasena", BANDERA);
		return pantallaChq(null, model);
	    
    }

    /**
     * @param request : HttpServletRequest
     * @param modelo : ModelAndView
     * @return String
     */
    @RequestMapping("/chqUsrLogout.do")
    public String logout(HttpServletRequest request, ModelAndView modelo) {
    	final UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
    	RSADTO  rsaBean = new RSADTO();
    	rsaBean = utilidadesRSA.generaBean(request, "", "");
    	try{

    		bitacoraTamSam.registrarOperacion(rsaBean.getIpOrigen(), rsaBean.getHostName(), 
    				rsaBean.getUserName(), "1", rsaBean.getAplicacion().concat("005"),
    				rsaBean.getIdSesion(), ConstantesRSA.REVIEW, rsaBean.getHostName());
    	}catch(BusinessException e){
    		LOGGER.error(e.getMessage(), e);
    	}
		request.getSession().setAttribute(USUARIO, null);
		request.getSession().invalidate();
	    return String.format("redirect:%s", EnlaceConfig.URL_LOGOUT_CHQDIG);
		
    }
    
    /**
     * @param req : HttpServletRequest
     * @param res : HttpServletResponse
     * @param model : ModelMap
     * @throws BusinessException : exception
     */
    @RequestMapping("/chqGeneraImagen.do")
    public void  generaImagen(HttpServletRequest req,
            HttpServletResponse res, ModelMap model) throws BusinessException {
    	
    	final String valImagen  = req.getSession().getAttribute(IMAGESVAL).toString() == null 
    	? "" : req.getSession().getAttribute(IMAGESVAL).toString();
    	final byte[] imageBytes = Base64.decodeBase64(valImagen.getBytes());
    	
    	res.setHeader("Cache-Control", "no-store");
        res.setHeader("Pragma", "no-cache");
        res.setDateHeader("Expires", 0);
    	res.setContentType("image/jpeg");
    	res.setContentLength(imageBytes.length);

    	try {
			res.getOutputStream().write(imageBytes);
			res.getOutputStream().close();	
		} catch (IOException e) {
			LOGGER.debug(e.getStackTrace());
		}
    	
    }
    
    /**
     * @param req : HttpServletRequest
     * @param res : HttpServletResponse
     * @param model : ModelMap
     * @param resp : Map<String, Object>
     * @param opcion : int
     * @param deviceTokenFSO : String
     * @param deviceTokenCookie : String
     * @return : ModelAndView
     * @throws BusinessException : exception
     */
    public ModelAndView validarOpcion(HttpServletRequest req,
            HttpServletResponse res, ModelMap model, Map<String, Object> resp, int opcion,
            String deviceTokenFSO, String deviceTokenCookie) throws BusinessException {
    	
    	switch(opcion){
    	
    	case 1:
    		modeloView = new ModelAndView("chqLoginContrasena");
			modeloView.addAllObjects(model);
			final String create = resp.get("CreateUser").toString();
			if(BANDERA.equals(create)){
				req.getSession().setAttribute("CreateUser", BANDERA);
			}
			req.getSession().setAttribute("vContrasena", BANDERA);
			return modeloView;
    	case 2: 
    		
    		modeloView = new ModelAndView("chqValidaImagen");
    		
        	deviceTokenFSO = resp.get("deviceTokenFSO").toString();
    	    deviceTokenCookie = resp.get("deviceTokenCookie").toString();
			
		    RSAServices.crearCookie(res, deviceTokenCookie);
			req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
			
		    req.getSession().setAttribute(IMAGESVAL, resp.get(IMAGESVAL).toString());
		    final String update = resp.get("UpdateUser") != null ? resp.get("UpdateUser").toString() : "false";
			if(BANDERA.equals(update)){
				req.getSession().setAttribute("UpdateUser", BANDERA);
			}
		    model.addAttribute("imagen", (ImagenDTO) resp.get("imagen"));
		    req.getSession().setAttribute("vImagen",BANDERA);
		    
			modeloView.addAllObjects(model);
			
	    	return modeloView;
	    	
    	case 3:
    		
    		modeloView = new ModelAndView("chqChallenge");
			
    		deviceTokenFSO = resp.get("deviceTokenFSO").toString();
    	    deviceTokenCookie = resp.get("deviceTokenCookie").toString();
    	    
    		RSAServices.crearCookie(res, deviceTokenCookie);
			req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
			
    		model.addAttribute("pregunta", (ChallengeQuestion)resp.get("pregunta"));
			model.addAttribute("Sesion", resp.get("sessionId").toString());
			model.addAttribute("Transaccion", resp.get("trasaccionId") != null ? resp.get("trasaccionId").toString() : "");
			req.getSession().setAttribute("challenge",BANDERA);
			modeloView.addAllObjects(model); 
			return modeloView;
			
    	case 4:
    		
    		modeloView = new ModelAndView(PAG_LOGIN);
    		req.getSession().setAttribute(USUARIO, null);
    		modeloView.addObject(MSJ, resp.get(MSJ).toString());
    		return modeloView;
    		
    	case 5:
    		
    		modeloView = new ModelAndView(PAG_LOGIN);
    		modeloView.addObject(MSJ, resp.get(MSJ).toString());
    		return modeloView;
    		
		}//fin switch
    	req.getSession().setAttribute("vContrasena", BANDERA);
    	return pantallaChq(null, model);
	}

	/**
	 * consumoRSA
	 * @return consumoRSA origen
	 */
	public RSAServiceEJB getConsumoRSA() {
		return consumoRSA;
	}

	/**
	 * consumoRSA
	 * @param consumoRSA origen
	 */
	public void setConsumoRSA(RSAServiceEJB consumoRSA) {
		this.consumoRSA = consumoRSA;
	}
	
	/**
	 * setBitacoraTamSam
	 * @param bitacoraTamSam origen
	 */
	public void setBitacoraTamSam(BOLRegistraBitacoraTamSam bitacoraTamSam) {
		this.bitacoraTamSam = bitacoraTamSam;
	}

	/**
	 * getBitacoraTamSam
	 * @return getBitacoraTamSam origen
	 */
	public BOLRegistraBitacoraTamSam getBitacoraTamSam() {
		return bitacoraTamSam;
	}
	
	/**
	 * getVersionRSA
	 * @return versionRSA origen
	 */
    public BOVersionRSA getVersionRSA() {
		return versionRSA;
	}

    /**
	 * setVersionRSA
	 * @param versionRSA origen
	 */
	public void setVersionRSA(BOVersionRSA versionRSA) {
		this.versionRSA = versionRSA;
	}
}
