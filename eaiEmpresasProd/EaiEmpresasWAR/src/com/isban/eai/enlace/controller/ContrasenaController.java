package com.isban.eai.enlace.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.isban.eai.enlace.controller.comun.GenericController;
import com.isban.eai.enlace.dto.RSADTO;
import com.isban.eai.enlace.dto.RequestAuthSAMDTO;
import com.isban.eai.enlace.dto.RespuestaAuthDTO;
import com.isban.eai.enlace.dto.VersionRSADTO;
import com.isban.eai.enlace.service.RSAServices;
import com.isban.eai.enlace.service.sam.BORServicioSAMEJB;
import com.isban.eai.enlace.servicio.BOLRegistraBitacoraTamSam;
import com.isban.eai.enlace.servicio.BOVersionRSA;
import com.isban.eai.enlace.util.ConstantesRSA;
import com.isban.eai.enlace.util.EnlaceConfig;
import com.isban.eai.enlace.util.UtilidadesRSA;
import com.isban.ebe.commons.exception.BusinessException;
import com.passmarksecurity.PassMarkDeviceSupportLite;

@Controller
public class ContrasenaController extends GenericController {

    /**
     * LOGGER
     */
    private static final Logger LOG = Logger.getLogger(ContrasenaController.class);

    /**
     * Constante para mensajes
     */
    private static final String MSJ = "msj";

    /**
     * Constante para el token
     */
    private static final String TOKEN = "token";

    /**usrAut**/
    private static final String USRAUT = "usrAut";

    /**
     * Constante para direccionar a pagina contrase�a
     */
    private static final String PAG_CONTRASENA = "pagLoginContrasena";

    /**bandera true**/
    private static final String BANDERA = "true";

    /**deviceTokenCookie**/
    private static final String DEVICETOKENCOOKIE = "deviceTokenCookie";

    /**deviceTokenFSO**/
    private static final String DEVICETOKENFSO = "deviceTokenFSO";

    /**OPERACI�N**/
    private static final String OPERACION = "E002";

    /**
     * bitacoraTamSam
     */
    private BOLRegistraBitacoraTamSam bitacoraTamSam;

    /**
     * consumoSAM
     */
    private BORServicioSAMEJB consumoSAM;

    /**
     * versionRSA
     */
    private BOVersionRSA versionRSA;
    
    /**
     * mostrarPaginaChallenge
     * @param req : req
     * @param res : res
     * @return mostrarPaginaContrasena mostrar pagina
     */
    @RequestMapping("/inicioContrasena.do")
    public ModelAndView mostrarPaginaContrasena(HttpServletRequest req,
            HttpServletResponse res) {
    	final UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
    	RSADTO  rsaBean = new RSADTO();
    	rsaBean = utilidadesRSA.generaBean(req, "", "");

    	generaBitacora(rsaBean.getIpOrigen(), rsaBean.getHostName(),
				rsaBean.getUserName(), "1", rsaBean.getAplicacion().concat("004"),
				rsaBean.getIdSesion(), ConstantesRSA.ALLOW, rsaBean.getHostName());

    	final ModelAndView modeloView = new ModelAndView(PAG_CONTRASENA);  
    	
        return modeloView;
    }

    /**
     * validaContrasena
     * @param req : req
     * @param res : res
     * @param model : model
     * @return validaContrasena valida
     */
    @RequestMapping("/validarContrasena.do")
    public ModelAndView validaContrasena(HttpServletRequest req,
            HttpServletResponse res, ModelMap model){

    	final boolean admonToken = req.getSession().getAttribute("admonToken") == null ? 
    			false : true, vContrasena = req.getSession().getAttribute("vContrasena") == null ? 
    					false : true;
    	if(!admonToken && !vContrasena){
    		try {
				res.sendRedirect("usrLogout.do");
				return null;
			} catch (IOException e) {
				LOG.error(e.getMessage(), e);
			}
    	}

    	final String token = req.getParameter(TOKEN) == null 
    	? "" : req.getParameter(TOKEN).toString(), usuario = BANDERA.equals(token) ? 
    			req.getParameter("username").toString() : req.getSession().getAttribute("userName").toString();
    	final String usrAut = BANDERA.equals(token) ? req.getParameter(USRAUT).toString() : req.getSession().getAttribute(USRAUT).toString();
    	String password = req.getParameter("txtPassword").toString();

    	if(BANDERA.equals(token)){
    		model.addAttribute(TOKEN, BANDERA);
    	}
    	try{
	    	password = usrAut.concat(password);

	    	final RequestAuthSAMDTO reqAuth = new RequestAuthSAMDTO();
	    	reqAuth.setPassword(password);
	    	reqAuth.setUser(usuario);

			final String ipOrigen = req.getHeader("iv-remote-address");

			LOG.info("La ip origen del usuario(iv-remote-address): "+ipOrigen);

			final boolean banderaCreate = req.getSession().getAttribute("CreateUser") != null ? true : false;
			final boolean banderaUpdate = req.getSession().getAttribute("UpdateUser") != null ? true : false;
			LOG.info("************************banderaCreate: "+ banderaCreate + "************************banderaUpdate: "+banderaUpdate);
			int accion = 0;
			RSADTO  rsaBean = new RSADTO();
			//Consulta bandera version RSA
			VersionRSADTO versionDto = new VersionRSADTO();
			versionDto = versionRSA.consulta();
			req.getSession().setAttribute("versionRSA", versionDto.getValor().toString());

			if((banderaCreate || banderaUpdate) && BANDERA.equals(EnlaceConfig.BANDERA_RSA)){

				accion = banderaCreate ? 1 : accion;
				accion = banderaUpdate ? 2 : accion;
				final UtilidadesRSA utilidadesRSA = new UtilidadesRSA();
			    rsaBean = utilidadesRSA.generaBean(req, "", "");
			}

			final Map<String,Object> datos = consumoSAM.validarPassword(reqAuth, accion, rsaBean);
			final RespuestaAuthDTO respuesta = (RespuestaAuthDTO) datos.get("respAuth");

			if((banderaCreate || banderaUpdate) && BANDERA.equals(EnlaceConfig.BANDERA_RSA)){
				final String deviceTokenFSO = datos.get(DEVICETOKENFSO) != null ? datos.get(DEVICETOKENFSO).toString() : null;
				final String deviceTokenCookie = datos.get(DEVICETOKENCOOKIE) != null ? datos.get(DEVICETOKENCOOKIE).toString() : null;
			    if(deviceTokenFSO != null && deviceTokenCookie != null){
			    	RSAServices.crearCookie(res, deviceTokenCookie);
			    	req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
			    }
			}

			return evaluarRespuesta(respuesta, req, res, model, ipOrigen, usuario);

		}catch(BusinessException e){
			LOG.info("try " + e.getMessage()+"************************************"+e.getCause());
		}catch(StringIndexOutOfBoundsException e){
			LOG.info("try " + e.getMessage()+"************************************"+e.getCause());
		}

		final String mensaje = "CERR003";
		final ModelAndView modeloView = new ModelAndView(PAG_CONTRASENA);
		model.addAttribute(MSJ, mensaje);
		modeloView.addAllObjects(model);
		return modeloView;
    }

    /**
     * @param respuesta : respuesta
     * @param req : reuqest
     * @param res : response
     * @param model : modelo
     * @param ipOrigen : ip origen
     * @param usuario : usuario
     * @return ModelAndView
     */
    public ModelAndView evaluarRespuesta(RespuestaAuthDTO respuesta, HttpServletRequest req,
            HttpServletResponse res, ModelMap model, String ipOrigen, String usuario){

    	if(respuesta.isAutenticado()){

			LOG.info("Usuario valido");
			req.getSession().removeAttribute("imagesVal");
			req.getSession().removeAttribute("admonToken");
			req.getSession().removeAttribute("vContrasena");

			generaBitacora(ipOrigen, req.getLocalName(),usuario, "1", OPERACION,req.getSession().getId(), ConstantesRSA.ALLOW, req.getLocalName());

			req.getSession().setAttribute("renovarC",BANDERA);
			LOG.info("Seteando headers: \n [am-eai-user-id] \n [am-eai-xattrs] \n [eai-orig-user] \n [am-eai-auth-level]");
			res.setHeader("am-eai-user-id", usuario);
			res.setHeader("am-eai-xattrs", "eai-orig-user");
			res.setHeader("eai-orig-user", usuario);
			res.setHeader("am-eai-auth-level", "1");

			return new ModelAndView("seleccionContrato");
		}else{
			if(respuesta.isExpirado()){

				final ModelAndView modeloView = new ModelAndView("pagRenovarContrasena");
				model.addAttribute(MSJ, respuesta.getErrorAcceso());
				modeloView.addAllObjects(model);
				req.getSession().removeAttribute("imagesVal");
				req.getSession().removeAttribute("vContrasena");
				req.getSession().setAttribute("renovarC",BANDERA);
				generaBitacora(ipOrigen, req.getLocalName(),usuario, "1", OPERACION,req.getSession().getId(), ConstantesRSA.ALLOW, req.getLocalName());
				return modeloView;

			}else{
				if(respuesta.getErrorAcceso()!= null){
					ModelAndView modeloView = null;
					final String token = req.getParameter(TOKEN) == null 
					? "" : req.getParameter(TOKEN).toString();
					modeloView = BANDERA.equals(token) ? new ModelAndView("pagLoginCliente") : new ModelAndView(PAG_CONTRASENA);
					generaBitacora(ipOrigen, req.getLocalName(),usuario, "0", OPERACION,req.getSession().getId(), ConstantesRSA.ALLOW, req.getLocalName());
					if("USUARIO O CONTRASE�A INCORRECTOS".equals(respuesta.getErrorAcceso())){
						final String mensaje = "CERR001";
						model.addAttribute(MSJ, mensaje);
						modeloView.addAllObjects(model);
						return modeloView;
					}else{
						if("USUARIO BLOQUEADO".equals(respuesta.getErrorAcceso())){
							final String mensaje = "CERR002";
							model.addAttribute(MSJ, mensaje);
							modeloView.addAllObjects(model);
							return modeloView;
						}else{
							final String mensaje = "CERR001";
							model.addAttribute(MSJ, mensaje);
							modeloView.addAllObjects(model);
							return modeloView;
						}
					}
				}
			}
		}
		return null;

    }

    /**
     * Genera la bitacora de login
     * @param ipOrigen ip de origen
     * @param hostWeb hostWeb
     * @param usuario identificador de usuario
     * @param exitoOperacion resultado de la operacion
     * @param tipoOperacion tipo de operacion
     * @param digIntegridad Digito de integridad
     * @param rsaEstatus Estatus devuelto por RSA
     * @param idInstanciaWeb Instancia web
     */
    public void generaBitacora(String ipOrigen, String hostWeb, String usuario, String exitoOperacion, String tipoOperacion,
    		String digIntegridad, String rsaEstatus, String idInstanciaWeb){
    	try{
    		bitacoraTamSam.registrarOperacion(ipOrigen, hostWeb, usuario, exitoOperacion,
    				tipoOperacion, digIntegridad, rsaEstatus, idInstanciaWeb);
    	}catch(BusinessException e){
    		LOG.info("Error al genrar bitacora.");
    		LOG.error(e,e);
    	}
    }

			/**
		 * Obtiene la ip del cliente.
		 * @param req Peticion
		 * @return Ip del cliente
		 */
		public static String obtenerIPCliente(HttpServletRequest req) {
				String ipActual = "";
				String headerEnlace= "iv-remote-address";
				String headerAkamai = "True-Client-IP";

				LOG.info("Obteniendo la IP del cliente");
				if (req.getHeader(headerAkamai)!= null && !"".equals(req.getHeader(headerAkamai).trim())) {
					LOG.info("Obteniendo la IP del cliente desde AKAMAI" );
					ipActual = req.getHeader(headerAkamai);
				}else if (req.getHeader(headerEnlace)!= null && !"".equals(req.getHeader(headerEnlace).trim())) {
					LOG.info("Obteniendo la IP del cliente desde los equipos de ENLACE" );
					ipActual = req.getHeader(headerEnlace);
				} else {
					LOG.info("Obteniendo la IP desde la peticion" );
					ipActual = req.getRemoteAddr();
				}
				LOG.info("la IP del cliente obtenida: "+ipActual );

				return ipActual;
			}
	/**
	 * setBitacoraTamSam
	 * @param bitacoraTamSam origen
	 */
	public void setBitacoraTamSam(BOLRegistraBitacoraTamSam bitacoraTamSam) {
		this.bitacoraTamSam = bitacoraTamSam;
	}

	/**
	 * getBitacoraTamSam
	 * @return getBitacoraTamSam origen
	 */
	public BOLRegistraBitacoraTamSam getBitacoraTamSam() {
		return bitacoraTamSam;
	}

	/**
	 * @return consumoSAM BORServicioSAMEJB
	 */
	public BORServicioSAMEJB getConsumoSAM() {
		return consumoSAM;
	}

	/**
	 * @param consumoSAM : consumoSAM
	 */
	public void setConsumoSAM(BORServicioSAMEJB consumoSAM) {
		this.consumoSAM = consumoSAM;
	}

	/**
	 * getVersionRSA
	 * @return versionRSA origen
	 */
    public BOVersionRSA getVersionRSA() {
		return versionRSA;
	}

    /**
	 * setVersionRSA
	 * @param versionRSA origen
	 */
	public void setVersionRSA(BOVersionRSA versionRSA) {
		this.versionRSA = versionRSA;
}}