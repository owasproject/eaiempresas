package com.isban.eai.enlace.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.isban.rsa.aa.ws.ChallengeQuestion;
import mx.isban.CmpRSA.service.ServiciosRSA;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.isban.eai.enlace.controller.comun.GenericController;
import com.isban.eai.enlace.dto.ImagenDTO;
import com.isban.eai.enlace.dto.RSADTO;
import com.isban.eai.enlace.dto.VersionRSADTO;
import com.isban.eai.enlace.service.RSAServices;
import com.isban.eai.enlace.servicio.RSAServiceEJB;
import com.isban.eai.enlace.servicio.BOVersionRSA;
import com.isban.eai.enlace.util.EnlaceConfig;
import com.isban.eai.enlace.util.UtilidadesRSA;
import com.isban.ebe.commons.exception.BusinessException;
import com.passmarksecurity.PassMarkDeviceSupportLite;

@Controller
public class ChallengeChqController extends GenericController {

    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(ChallengeChqController.class);
    
    /**msj**/
    private static final String MSJ = "msj";
    
    /**Sesion**/
    private static final String SESION = "Sesion";
    
    /**Transaccion**/
    private static final String TRANSACCION = "Transaccion";
        
    /**tipoDisp**/
    private static final String TIPODISP = "tipoDisp";
    
    /**deviceTokenFSO**/
    private static final String DEVICETOKENFSO = "deviceTokenFSO";
    
    /**deviceTokenCookie**/
    private static final String DEVICETOKENCOOKIE = "deviceTokenCookie";
    
    /**
     * ServiciosRSA
     */
    private transient ServiciosRSA servicioRSA;
    
    /**
     * RSADTO
     */
    private RSADTO rsaBean;
    
    /**
     * "consumoRSA"
     */
    private RSAServiceEJB consumoRSA;
	
	/**
     * versionRSA
     */
    private BOVersionRSA versionRSA;
    
   /**
    * @param req : peticion
 	* @param res : respuesta
 	* @param model : modelo
 	* @return ModelAndView : validacion cliente
 	* @throws BusinessException manejo de excepcion
 	*/ 
    @RequestMapping("/chqvalidarRespuesta.do")
    public ModelAndView validarCliente(HttpServletRequest req,
            HttpServletResponse res, ModelMap model) throws BusinessException {
        	
    		if("true".equals(EnlaceConfig.BANDERA_RSA)){
    			
				//Consulta bandera version RSA
				VersionRSADTO versionDto = new VersionRSADTO();
				versionDto = versionRSA.consulta();
				req.getSession().setAttribute("versionRSA", versionDto.getValor().toString());
				
    			final Map<String, String> entrada = new HashMap<String, String>();
		    	String deviceTokenFSO = "";
			    String deviceTokenCookie = "";
			    		    	
			    final UtilidadesRSA utils = new UtilidadesRSA();
				
				final String idPregunta = req.getParameter("idPregunta") != null ? req.getParameter("idPregunta") : "";
				final String respuesta = req.getParameter("respuesta") != null ? req.getParameter("respuesta") : "";
				final String idSesion = req.getParameter(SESION) != null ? req.getParameter(SESION) : "";
				final String idTransaccion = req.getParameter(TRANSACCION) != null ? req.getParameter(TRANSACCION) : "";
				final String txtPregunta = req.getParameter("txtPregunta") != null ? req.getParameter("txtPregunta") : "";
				final String tipoDisp = req.getParameter(TIPODISP) != null ? req.getParameter(TIPODISP) : "";
				final String usuario = req.getSession().getAttribute("userName").toString();
				
				entrada.put("idPregunta", idPregunta);
				entrada.put("respuesta", respuesta);
				entrada.put("txtPregunta", txtPregunta);
				entrada.put(TIPODISP, tipoDisp);
				entrada.put("userName", usuario);
				
				rsaBean =  utils.generaBean(req, idSesion, idTransaccion);
				
				Map<String, Object> resp = new HashMap<String, Object>();
				resp =  consumoRSA.validarRespuestaCliente(rsaBean, entrada);
				
		    	deviceTokenFSO = resp.get(DEVICETOKENFSO).toString();
			    deviceTokenCookie = resp.get(DEVICETOKENCOOKIE).toString();
			    
			    RSAServices.crearCookie(res, deviceTokenCookie);
				req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
				
				return evaluarRespuesta(req, res, model, deviceTokenFSO, deviceTokenCookie, resp);
				
    		}
			
    		LOG.info(">>> El web service de RSA no esta activo");
			return pantalla(null, model);
    		
    }
    
    /**
     * @param req : request
     * @param res : response
     * @param model : modelo
     * @param deviceTokenFSO : deviceTokenFSO
     * @param deviceTokenCookie : deviceTokenCookie
     * @param resp : resp
     * @return ModelAndView
     */
    public ModelAndView evaluarRespuesta (HttpServletRequest req,
            HttpServletResponse res, ModelMap model, String deviceTokenFSO,
		    String deviceTokenCookie, Map<String, Object> resp){
    	
    	final int opcion = Integer.parseInt(resp.get("accion").toString());
    	switch(opcion){
    	
    	case 1:
    		final ModelAndView modeloView = new ModelAndView("chqChallenge");
    		req.setAttribute("pregunta", (ChallengeQuestion)resp.get("pregunta"));
			req.setAttribute(SESION, resp.get(SESION).toString());
			req.setAttribute(TRANSACCION, resp.get(TRANSACCION).toString());
			req.setAttribute(TIPODISP, resp.get(TIPODISP).toString());
			req.setAttribute(MSJ, resp.get(MSJ).toString());
			
			deviceTokenFSO = resp.get(DEVICETOKENFSO).toString();
    	    deviceTokenCookie = resp.get(DEVICETOKENCOOKIE).toString();
    	    
    		RSAServices.crearCookie(res, deviceTokenCookie);
			req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
			
			return modeloView;
    	case 2:
    		final ModelAndView modeloView1 = new ModelAndView("pagLoginCliente");
    		req.setAttribute(MSJ, resp.get(MSJ).toString());
    		req.getSession().setAttribute("userName", null);
    		req.getSession().invalidate();
						
			return modeloView1;
    	case 3:
    		
    		final ModelAndView modeloView2 = new ModelAndView("chqValidaImagen");
    		
			req.getSession().removeAttribute("challenge");
        	
        	ImagenDTO dto = new ImagenDTO();
    		
        	deviceTokenFSO = resp.get(DEVICETOKENFSO).toString();
    	    deviceTokenCookie = resp.get(DEVICETOKENCOOKIE).toString();
			
    	    final String imagesVal = resp.get("imagesVal").toString();
			
		    dto = (ImagenDTO) resp.get("imagen");
		    
		    req.getSession().setAttribute("imagesVal", imagesVal);
		    model.addAttribute("imagen", dto);
		    req.getSession().setAttribute("vImagen","true");
		    
		    deviceTokenFSO = resp.get(DEVICETOKENFSO).toString();
    	    deviceTokenCookie = resp.get(DEVICETOKENCOOKIE).toString();
    	    
    		RSAServices.crearCookie(res, deviceTokenCookie);
			req.getSession().setAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO, deviceTokenFSO);
			
			LOG.info("Imagen   " + dto.getIdImagen());
			modeloView2.addAllObjects(model);
	    	return modeloView2;
    	case 6:
    		return pantalla(null, model);
    	}
		return null;
    	
    }
   
   /**
    * @param e Exception que se genero
    * @param model modelo que tiene datos de la pantalla
    * @return ModelAndView al que direccionara la aplicación
    */
   public ModelAndView pantalla(Exception e, ModelMap model){
   	
	   if(e != null){
		   LOG.info(e.getMessage());
	   }
	   LOG.debug(">>> El web service de RSA no respondio");
	   model.addAttribute("ValidarRSA", false);
	   final ModelAndView modeloView = new ModelAndView("chqLoginContrasena");
	   modeloView.addAllObjects(model);
	   return modeloView;
	   
   }

	/**
	 * @param servicioRSA : servicioRSA
	 */
	public void setServicioRSA(ServiciosRSA servicioRSA) {
		this.servicioRSA = servicioRSA;
	}
	
	/**
	 * @return servicioRSA
	 */
	public ServiciosRSA getServicioRSA() {
		return servicioRSA;
	}
	
	/**
	 * @param rsaBean : rsaBean
	 */
	public void setRsaBean(RSADTO rsaBean) {
		this.rsaBean = rsaBean;
	}
	
	/**
	 * @return RSABean
	 */
	public RSADTO getRsaBean() {
		return rsaBean;
	}
	
	/**
	 * consumoRSA
	 * @return consumoRSA origen
	 */
	public RSAServiceEJB getConsumoRSA() {
		return consumoRSA;
	}

	/**
	 * consumoRSA
	 * @param consumoRSA origen
	 */
	public void setConsumoRSA(RSAServiceEJB consumoRSA) {
		this.consumoRSA = consumoRSA;
	}
	
	/**
	 * getVersionRSA
	 * @return versionRSA origen
	 */
    public BOVersionRSA getVersionRSA() {
		return versionRSA;
	}

    /**
	 * setVersionRSA
	 * @param versionRSA origen
	 */
	public void setVersionRSA(BOVersionRSA versionRSA) {
		this.versionRSA = versionRSA;
	}
	
}
