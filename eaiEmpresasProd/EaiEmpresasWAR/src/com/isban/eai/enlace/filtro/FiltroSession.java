/**
 * STFQRO - 2011
 */
package com.isban.eai.enlace.filtro;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.sesion.Sesion;


public class FiltroSession implements Filter {

    /**
     * LOGGER : El objeto de escritura en log.
     */
	private static final Logger LOG = Logger.getLogger(FiltroSession.class);
	
    /**
     * URL de Error para mostrar al usuario
     */
	private static final String REDIR_ERROR_APP = "/jsp/private/error500.jsp";
	
	 /**
     * URL de cierre de sesi�n
     */
	private static final String URL_LOGOUT = "usrLogout.do";
	
	/**
     * URL de cierre de sesi�n cheque Digital
     */
	private static final String URL_LOGOUT_CHQ = "chqUsrLogout.do";
	
	/**
     * Aplicacion 
     */
	private static final String APLICACION = "aplicacion";
	
	/**
     * indica que esta en Cheque Digital
     */
	private static final String ID_C = "C";

	/**
	 * La sesion del usuario.
	 **/
	private Sesion sesion;

	/** {@inheritDoc} */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {

        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse resp = (HttpServletResponse) response;

        //Solo se validan los elementos necesarios
        if (!(req.getRequestURI().endsWith(".do")
                || req.getRequestURI().endsWith(".jsp"))) {
            chain.doFilter(request, response);
            return;
        }
        
        if(!req.getServletPath().contains("inicio.do") &&
        		!req.getServletPath().contains("srLogout.do") &&
        		!req.getServletPath().contains("urlToken.do") && 
        		!req.getServletPath().contains("/rsa/") &&
    			!req.getServletPath().contains("LoginCliente.jsp")) {
        	
        	final boolean admonToken = req.getSession().getAttribute("admonToken") == null ? false : true;
        	if(admonToken && req.getServletPath().contains("validarContrasena.do")){
        		chain.doFilter(request, response);
			}
        	
        	final boolean plantilla = req.getParameter("plantilla") == null ? false : true;
    		if(plantilla && !admonToken){
    			LOG.info("******Operacion no permitida.");
	    		resp.sendRedirect(URL_LOGOUT);
        		return;
    		}
        	
        	try{
	        	final boolean sesionValida = req.getSession().getAttribute("userName") == null ? false : true;
	        	String aplicacion = req.getSession().getAttribute(APLICACION) == null 
    			? "" : req.getSession().getAttribute(APLICACION).toString();
	        	if(sesionValida){
	        		
	        		boolean banderas = evaluaBanderas(request, response, req, resp);
        			if(banderas){
        				return;
        			}
        			
	        		chain.doFilter(request, response);
	        		return;
	        	}else{
	        		LOG.info("******Sesion invalida");
	        		if(ID_C.equals(aplicacion)){
	        			resp.sendRedirect(URL_LOGOUT_CHQ);
	        		}else{
	        			resp.sendRedirect(URL_LOGOUT);
	        		}
	            	return;
	        	}
        	}catch (IOException e) {
        		muestraError(request, response, REDIR_ERROR_APP);
			}catch (ServletException e){
				muestraError(request, response, REDIR_ERROR_APP);
			}
        }
        
        chain.doFilter(request, response);

    }
    
    /**
     * Muestra la pantalla con el mensaje de error.
     * @param request request de la petici�n
     * @param response response de la petici�n
     * @param urlPagError url de la p�gina de error 
     * @throws IOException excepci�n de entrada y salida
     * @throws ServletException excepci�n del servlet
     */
    private void muestraError(ServletRequest request, ServletResponse response,
            String urlPagError) throws IOException, ServletException {
    	LOG.error("Error de acceso");
    	LOG.error(String.format("REDIRECIONANDO A: %s", urlPagError));
        request.getRequestDispatcher(urlPagError).forward(request, response);
    }
    
    /**
     * @param request : ServletRequest
     * @param response : ServletResponse
     * @param req : HttpServletRequest
     * @param resp : HttpServletResponse
     * @return boolean
     * @throws IOException excepci�n de entrada y salida
     * @throws ServletException excepci�n del servlet
     */
    private boolean evaluaBanderas(ServletRequest request, ServletResponse response, HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException{
	    	
    	try{
    		String aplicacion = req.getSession().getAttribute(APLICACION) == null 
			? "" : req.getSession().getAttribute(APLICACION).toString();
    		final boolean challenge = req.getSession().getAttribute("challenge") == null ? false : true;
			
			//Debe contestar pregunta reto
			if(challenge && !req.getServletPath().contains("validarRespuesta.do") &&
	    			!req.getServletPath().contains("Challenge")){
				LOG.info("******Operacion no permitida");
				if(ID_C.equals(aplicacion)){
        			resp.sendRedirect(URL_LOGOUT_CHQ);
        		}else{
        			resp.sendRedirect(URL_LOGOUT);
        		}
	        	return true;
	    	}
			
			final boolean imagen = req.getSession().getAttribute("vImagen") == null ? false : true;
			final String pasoImg = req.getParameter("pasoImg") != null ? req.getParameter("pasoImg").toString() : "";
			if(imagen && (!req.getServletPath().contains("Imagen") && !req.getServletPath().contains("validarRespuesta.do") &&
	    			!(req.getServletPath().contains("inicioContrasena.do") && "true".equals(pasoImg)))){
				LOG.info("******Operacion no permitida");
				if(ID_C.equals(aplicacion)){
        			resp.sendRedirect(URL_LOGOUT_CHQ);
        		}else{
        			resp.sendRedirect(URL_LOGOUT);
        		}
	    		return true;
	    	}
			
			if("true".equals(pasoImg)){
				req.getSession().removeAttribute("vImagen");
				req.getSession().setAttribute("vContrasena", "true");
			}
			
			final boolean contrasena = req.getSession().getAttribute("vContrasena") == null ? false : true;
			if(contrasena && !req.getServletPath().contains("inicioContrasena.do") &&
	    			!req.getServletPath().contains("validarContrasena.do") && !req.getServletPath().contains("LoginContrasena")){
				LOG.info("---------------> ServletPath: "+req.getServletPath());
				LOG.info("*******Operacion no permitida");
				if(ID_C.equals(aplicacion)){
        			resp.sendRedirect(URL_LOGOUT_CHQ);
        		}else{
        			resp.sendRedirect(URL_LOGOUT);
        		}
	    		return true;
	    	}
			
			final boolean renovarC = req.getSession().getAttribute("renovarC") == null ? false : true;
			if(!renovarC && (req.getServletPath().contains("RenovarContrasena") ||
	    			req.getServletPath().contains("actualizarContrasena"))){
				LOG.info("******Operacion no permitida");
				if(ID_C.equals(aplicacion)){
        			resp.sendRedirect(URL_LOGOUT_CHQ);
        		}else{
        			resp.sendRedirect(URL_LOGOUT);
        		}
	    		return true;
	    	}
	    }catch (IOException e) {
			muestraError(request, response, REDIR_ERROR_APP);
	    }
	    return false;
    }

    /** {@inheritDoc} */
    public void init(FilterConfig filterConfig) throws ServletException {
        LOG.info("Se inicia el Filtro");
    }
    
    /** {@inheritDoc} */
	public void destroy() {
	}
	
    /**
     * Obtiene la sesion del usuario
     * @return Sesion : sesion del usuario
     */
    public Sesion getSesion() {
        return sesion;
    }

    /**
     * Asigna la sesion del usuario.
     * @param sesion : la sesion del usuario. 
     */
    public void setSesion(Sesion sesion) {
        this.sesion = sesion;
    }
    
}
