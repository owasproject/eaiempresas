/**
 * Isban Mexico
 *   Clase: HttpServletRequestWrapperHeader.java
 *   Descripci�n: Componente envolvedor de Headers para simulacion.
 *
 *   Control de Cambios:
 *   1.0 Mar 28, 2012 oacosta - Creacion
 */
package com.isban.eai.enlace.util;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * Componente envolvedor de Headers para simulacion.
 */
public class HttpServletRequestWrapperHeader extends HttpServletRequestWrapper {

	/**
	 * hashTable para Headers.
	 */
	private final transient Hashtable<String,Vector<String>> hashTable = new Hashtable<String,Vector<String>>(); 
    
    /**
     * @param request : request de la petici�n
     */
    @SuppressWarnings("unchecked")
    public HttpServletRequestWrapperHeader(HttpServletRequest request) {
        super(request);
        String name = null;
        String value = null;
        Vector<String> vectorValues = null;
        Enumeration<String> enumerationValues = null;
        final Enumeration<String> enumerationNames = request.getHeaderNames();
        while(enumerationNames.hasMoreElements()) {
            name = (String)enumerationNames.nextElement();
            enumerationValues = request.getHeaders(name);
            vectorValues = new Vector<String>();
            while(enumerationValues.hasMoreElements()) {
                value = enumerationValues.nextElement();
                vectorValues.addElement(value);
            }
            hashTable.put(name, vectorValues);
        }
    }
    
    /** {@inheritDoc} */
    public String getHeader(String name) {
        String elemento = null;
        final Vector<String> vector = hashTable.get(name);
        if(vector != null) {
            elemento = vector.firstElement();
        }
        return elemento;
    }
    
    /**
     * @param name : nombre de la propiedad
     * @param value : valor de la propiedad
     */
    public void addHeader(String name,String value) {
        Vector<String> vector = hashTable.get(name);
        if(vector == null) {
            vector = new Vector<String>();
        }
        vector.add(value);
        hashTable.put(name,vector);
    }
    
    /** {@inheritDoc} */
    public Enumeration<String> getHeaderNames(){
        return hashTable.keys();
    }   

    /** {@inheritDoc} */
    public Enumeration<String> getHeaders(String name) {
        Enumeration<String> enumeration = null;
        final Vector<String> vector = hashTable.get(name);
        if(vector != null) {
            enumeration = vector.elements();
        }
        return enumeration;
    }
}
