package com.isban.eai.enlace.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import mx.isban.rsa.bean.RSABean;
import mx.isban.rsa.bean.ServiciosSTUResponse;
import mx.isban.rsa.bean.ServiciosAAResponse;
import mx.isban.rsa.aa.ws.ChallengeQuestionGroup;
import mx.isban.rsa.aa.ws.ChallengeQuestionManagementResponsePayload;
import mx.isban.rsa.aa.ws.DeviceRequest;
import mx.isban.rsa.stu.ws.Image;
import mx.isban.rsa.conector.servicios.ServiciosSTU;
import mx.isban.rsa.conector.servicios.ServiciosAA;

import org.apache.log4j.Logger;

import com.isban.eai.enlace.controller.ContrasenaController;
import com.isban.eai.enlace.dto.ImagenDTO;
import com.isban.eai.enlace.dto.PreguntaDTO;
import com.isban.eai.enlace.dto.RSADTO;
import com.isban.eai.enlace.service.RSAServices;
import com.isban.ebe.commons.exception.BusinessException;
import com.passmarksecurity.PassMarkDeviceSupportLite;

public class UtilidadesRSA  {
	
	/**
	 * objeto para mostrar mensajes
	 * **/
    private static final Logger LOGGER = Logger.getLogger(UtilidadesRSA.class);
	
	/**
	 * String que contiene la versi�n de RSA
	 * **/
	private String versionRSA = "";
    /**
	 * Arrgle con las palabras prohibidas para el password
	 */
	//CSA RFC100001010145 - Validacion de politicas de contrase�a
	private static final String[] PALAB_PROHIBI = new String[] {"santander",
		"altec", "serfin", "banca", "electronica", "enlace", "banco",
		"supernet", "empresas"};
	
	/**
	 * Metodo para generar el bean que utilizadon los metodos de rsa
	 * @param request : request
	 * @param sessionID : sessionID
	 * @param transactionID : transactionID
	 * @return RSADTO
	 */
	public RSADTO generaBean (HttpServletRequest request, String sessionID, String transactionID) {
		
		LOGGER.info("M�todo generaBean");
		final RSADTO rsaBean = new RSADTO();
		
		// Datos para setear 
		versionRSA = request.getSession().getAttribute("versionRSA") != null ? 
				request.getSession().getAttribute("versionRSA").toString() : "1";
		LOGGER.info("Valor de version RSA: ".concat(versionRSA));
    	final String devicePrint = request.getSession().getAttribute("valorDeviceP") != null ?
    			request.getSession().getAttribute("valorDeviceP").toString() : "";
    	final String fsoCookie = request.getSession().getAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO) != null ?
    			request.getSession().getAttribute(PassMarkDeviceSupportLite.ATTR_SET_FLASH_SO).toString() : "";
    	final String cookie = RSAServices.getCookieValue(request);

    	final String ipAddress = ContrasenaController.obtenerIPCliente(request);
    	final String userCountry = request.getLocale().getCountry();
    	String httpAccept = request.getHeader("Accept");
    	final String httpAcceptChars = request.getHeader("Accept-Charset") != null ? request.getHeader("Accept-Charset") : "utf-8";
    	final String httpAcceptEncoding = request.getHeader("Accept-Encoding") != null ? request.getHeader("Accept-Encoding") : "gzip, deflate";
    	final String httpAcceptLanguage = request.getHeader("Accept-Language");			    
    	final String httpReferrer = request.getHeader("Referer");
    	final String userAgent = request.getHeader("user-agent");
		
		if(httpAccept == null || "".equals(httpAccept)) {
			httpAccept = request.getHeader("accept");
		}
		//device requestuest
    	final DeviceRequest deviceRequest = new DeviceRequest();
    	
    	deviceRequest.setDevicePrint(devicePrint);
    	deviceRequest.setDeviceTokenCookie(cookie);
    	deviceRequest.setDeviceTokenFSO(fsoCookie);
    	deviceRequest.setHttpAccept(httpAccept);
    	deviceRequest.setHttpAcceptChars(httpAcceptChars);
    	deviceRequest.setHttpAcceptEncoding(httpAcceptEncoding);
    	deviceRequest.setHttpAcceptLanguage(httpAcceptLanguage);
    	deviceRequest.setHttpReferrer(httpReferrer);
    	deviceRequest.setIpAddress(ipAddress);
    	deviceRequest.setUserAgent(userAgent);
    	
    	//IMPRESION PARA PRUEBAS 
    	LOGGER.info("DevicePrint    " + deviceRequest.getDevicePrint() +
    	" ***DeviceTokenCookie    " + deviceRequest.getDeviceTokenCookie() +
    	" ***DeviceTokenFSO    " + deviceRequest.getDeviceTokenFSO() +
    	" ***HttpAccept    " + deviceRequest.getHttpAccept() +
    	" ***HttpAcceptChars    " + deviceRequest.getHttpAcceptChars() +
    	" ***HttpAcceptEncoding    " + deviceRequest.getHttpAcceptEncoding() +
    	" ***HttpAcceptLanguage    " + deviceRequest.getHttpAcceptLanguage() +
    	" ***HttpReferrer    " + deviceRequest.getHttpReferrer() +
    	" ***IpAddress    " + deviceRequest.getIpAddress() +
    	" ***UserAgent    " + deviceRequest.getUserAgent());
    	//FIN IMPRESION PARA PRUEBAS
		
		rsaBean.setAuthSessionId(sessionID);
		rsaBean.setAuthTransactionId(transactionID);
    	rsaBean.setDeviceRequest(deviceRequest);
    	rsaBean.setIdSession(request.getSession().getId().toString());
    	rsaBean.setUserCountry(userCountry);
    	rsaBean.setUserLanguage("ES");
    	rsaBean.setIpOrigen(request.getHeader("iv-remote-address"));
    	rsaBean.setHostName(request.getLocalName());
    	rsaBean.setIdSesion(request.getSession().getId());
		//Version de RSA
		rsaBean.setVersionRSA("0".equals(versionRSA) ? "RSA6" : "RSA7");
    	
    	rsaBean.setUserName(request.getSession().getAttribute("userName") == null 
    			? "" : request.getSession().getAttribute("userName").toString());
    	
    	final String aplicacion = request.getSession().getAttribute("aplicacion") == null 
    			? "" : request.getSession().getAttribute("aplicacion").toString();
    	//Para realizar bitacorizaci�n por aplicaci�n
    	if(!"".equals(aplicacion)){
    		rsaBean.setAplicacion(aplicacion);
    	}
		
		rsaBean.setOrgName("C".equals(aplicacion) ? EnlaceConfig.APLICACION_C : EnlaceConfig.APLICACION_E);
		    	
    	//rsaBean.setUserName(request.getHeader("iv-user"));
    	
    	//IMPRESION PARA PRUEBAS 
    	LOGGER.info("***AuthSessionId    " + rsaBean.getAuthSessionId() +
    	" ***DeviceRequest    " + rsaBean.getDeviceRequest() +
    	" ***IdSession    " + rsaBean.getIdSession() +
    	" ***transactionID    " + rsaBean.getAuthTransactionId() +
    	" ***OrgName    " + rsaBean.getOrgName() +
    	" ***UserCountry    " + rsaBean.getUserCountry() +
    	" ***UserLanguage    " + rsaBean.getUserLanguage() +
    	" ***IpOrigen    " + rsaBean.getIpOrigen() +
    	" ***HostName    " + rsaBean.getHostName() +
    	" ***IdSesion    " + rsaBean.getIdSesion() +
    	" ***UserName    " + rsaBean.getUserName() +
    	" ***Aplicacion    " + rsaBean.getAplicacion() +
		" ***VersionRSA   " + rsaBean.getAplicacion());
    	//FIN IMPRESION PARA PRUEBAS
    	
    	
    	return rsaBean;
	}
	
	/**
	 * @param rsaBean : bean que contiene la información 
	 * necesaria para consultar las imagenes
	 * @return List<ImagenDTO> lista que contiene 10 imagenes aleatorias
	 * @throws BusinessException : exception
	 */
	public List<ImagenDTO> obtenerImagenes (RSABean rsaBean) throws BusinessException {
		
		ServiciosSTUResponse res = new ServiciosSTUResponse();
		final ServiciosSTU servicioSTU = new ServiciosSTU();
		
		res = servicioSTU.browseImages(rsaBean, "0".equals(versionRSA) ? "RSA6" : "RSA7");
		final Image [] imagenes = res.getSiteToUserBrowseResponse().getBrowsableImages();
		
		final List<ImagenDTO> lista = new ArrayList<ImagenDTO>();
		for(int i = 0 ; i < 10; i++) {
			final ImagenDTO dto = new ImagenDTO();
			dto.setImagen(imagenes[i].getData());
			dto.setIdImagen(imagenes[i].getPath());
			
			lista.add(dto);
		}
		
		return lista;
	}
	
	/**
	 * @param rsaBean : bean que contiene la información 
	 * necesaria para consultar las imagenes
	 * @return List<PreguntaDTO> lista que contiene 5 preguntas aleatorias
	 * @throws BusinessException : exception
	 */
	public List<PreguntaDTO> obtenerPreguntas (RSABean rsaBean) throws BusinessException {
		
		ServiciosAAResponse res = new ServiciosAAResponse();
		final ServiciosAA servicioAA = new ServiciosAA();
		
		res = servicioAA.browseQuestion(rsaBean, "0".equals(versionRSA) ? "RSA6" : "RSA7");
	
		ChallengeQuestionManagementResponsePayload  question = 
			new ChallengeQuestionManagementResponsePayload();
		question = res.getChallengeQuestionPayload();
		final ChallengeQuestionGroup[] questions = question.getBrowsableChallQuesGroupList();
		//ChallengeQuestion[] cq = questions[0].getChallengeQuestion();
		
		final List<PreguntaDTO> listaPreguntas = new ArrayList<PreguntaDTO>();
		//ChallengeQuestion[] cq = questions[generaNumero()].
			//getChallengeQuestion();
		
		for(int i = 0 ; i < 5; i++) {				
			final PreguntaDTO pregunta = new PreguntaDTO();
			//pregunta.setPreguntaTexto(cq[i].getQuestionText());
			//pregunta.setPreguntaID(cq[i].getQuestionId());
			
			pregunta.setPreguntaTexto(questions[generaNumero(questions.length)]
			                                    .getChallengeQuestion(generaNumero(10))
			                                    .getQuestionText());
			pregunta.setPreguntaID(questions[generaNumero(questions.length)]
			                                 .getChallengeQuestion(generaNumero(10))
			                                 .getQuestionId());
			
			listaPreguntas.add(pregunta);
			
		}
		
		return listaPreguntas;
	}
	
	/**
	 * Metodo que genera numero aleatorio
	 * @param limite : limite
	 * @return int : numero aleatorio
	 */
	private int generaNumero(int limite) {
		final Random rand = new Random();
	    final int num = rand.nextInt(limite);
	     
		return num;
	}

	/**
	 * @return versionRSA regresa valor de String
	 */
	public String getVersionRSA() {
		return versionRSA;
	}

	/**
	 * @param versionRSA : versionRSA
	 */
	public void setVersionRSA(String versionRSA) {
		this.versionRSA = versionRSA;
	}
	    /**
     * Metodo para validar que el pass no sea una palabra prohibida
     * @param pass password a validar
     * @return true/false si es o no una palabra prohibida
     */
	//CSA RFC100001010145 - Validacion de politicas de contrase�a
    public static boolean validarPalabras( String pass ) {
    	LOGGER.info("Entrando validarPalabras");
        boolean valida = false;
        String passwdMin = pass.toLowerCase();
        for(int i = 0; i < PALAB_PROHIBI.length ; i++ ) {
                if( passwdMin.indexOf(PALAB_PROHIBI[i]) != -1 ) {
                        valida = true;
                        i += PALAB_PROHIBI.length;
                }
        }
        LOGGER.info("Saliendo validarPalabras [" + valida + "]");
        return valida;
    }

    /**
     * Valida caracter a carecter.
     * @param pass cadena a revisar
     * @return true/false
     */
	//CSA RFC100001010145 - Validacion de politicas de contrase�a
	public static boolean reglasCNBV( String pass ) {
	    boolean valida = true;
	    LOGGER.info("Entrando reglasCNBV");

    	int longitud = pass.length();
    	for(int i = 0 ; i < longitud -2 ; i++ ){
    		String carac0 = pass.substring(i, i+1);
    		String carac1 = pass.substring(i+1, i+2);
    		String carac2 = pass.substring(i+2, i+3);
    		if(carac0 == carac1) {
    			if(carac0 == carac2) {
    				valida = false;
    				i += longitud;
    			}
    		}
    		int caraCode0 = pass.codePointAt(i);
    		int caraCode00 = pass.codePointAt(i);
    		int caraCode1 = pass.codePointAt(i+1);
    		int caraCode11 = pass.codePointAt(i+1);
    		int caraCode2 = pass.codePointAt(i+2);
    		int caraCode22 = pass.codePointAt(i+2);

    		if(++caraCode0  == caraCode1) {
    			if(++caraCode0 == caraCode2) {
    				valida = false;
    				i += longitud;
    			}
    		}
    		if(--caraCode00 == caraCode11) {
    			if(--caraCode00 == caraCode22) {
    				valida = false;
    				i += longitud;
    			}
    		}
    	}
    	if( !validaAlfa(pass) ) {
    		valida  = false;
    	}

    	LOGGER.info("Saliendo reglasCNBV [" + valida + "]");
	    return valida;
	}
	/**
	 * Valida que solo sean numeros y/o letras
	 * @param campo valor a validar
	 * @return true/false
	 */
	//CSA RFC100001010145 - Validacion de politicas de contrase�a
	private static boolean validaAlfa( String campo ) {
		Pattern p = Pattern.compile("[^A-Za-z0-9]+");
		Matcher m = p.matcher(campo);
		boolean resultado = m.find();
		boolean valido = true;
		while(resultado) {
			valido = false;
	        resultado = m.find();
	    }
		return valido;
	}
	

}